var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var BL = require('../public/javascripts/BL');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', function(req, res, next) {
  data = req.body;
  bcrypt.hash(data.pass, 10, function(err, hash){
    if(hash) {
      data.pass = hash;
      return BL.RegisterUser(data).then((result) => {
        console.log(result);
        res.send({message:""+result+""});
      }).catch((err) => {
        next(err);
      })
    }
})
});

router.post('/loginPassword', function(req, res, next) {
  data = req.body;
  return BL.LoginUserPassword(data).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})


router.post('/login', function(req, res, next) {
  data = req.body;
  return BL.LoginUser(data).then((result) => {
    res.send({message:""+result+""});
  }).catch((err) => {
    next(err);
  })
})

router.post('/resend', function(req, res, next) {
  return BL.ResendOTP(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/cafeLogin', function(req, res, next) {
  data = req.body;
  return BL.CafeLoginUser(data).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/email', function(req, res, next) {
  data = req.body.email;
  return BL.CheckEmail(data).then((result) => {
    //console.log(result);
    res.send({message:""+result+""});
    //console.log(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/name', function(req, res, next) {
  data = req.body.user;
  return BL.CheckName(data).then((result) => {
    res.send({message:""+result+""});
    //console.log(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/number', function(req, res, next) {
  data = Number(req.body.number);
  return BL.CheckNumber(data).then((result) => {
    res.send({message:""+result+""});
    //console.log(result);
  }).catch((err) => {
    next(err);
  })
})

router.get('/getCafes', function(req, res, next) {
  return BL.GetCafes().then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/getCafe', function(req, res, next) {
  return BL.GetCafe(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/getUser', function(req, res, next) {
  return BL.GetUser(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/cafeSystems', function(req, res, next) {
  return BL.CafeSystems(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/addAccount', function(req, res, next) {
  return BL.AddAccount(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/addContact', function(req, res, next) {
  return BL.AddContact(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/addAddress', function(req, res, next) {
  return BL.AddAddress(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/bookingPC', function(req, res, next) {
  return BL.BookingAvailabilityPC(req.body).then((result) => {
    console.log(result);
    res.send({'available' : result});
  }).catch((err) => {
    next(err);
  })
});

router.post('/bookingConsole', function(req, res, next) {
  return BL.BookingAvailabilityConsole(req.body).then((result) => {
    console.log(result);
    res.send({'available' : result});
  }).catch((err) => {
    next(err);
  })
});

router.post('/bookPC', function(req, res, next) {
  return BL.BookPC(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/bookConsole', function(req, res, next) {
  return BL.BookConsole(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/previous', function(req, res, next) {
  return BL.PreviousBooking(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/upcoming', function(req, res, next) {
  return BL.UpcomingBooking(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/bookedDates', function(req, res, next) {
  return BL.BookedDates(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/fblogin', function(req, res, next) {
  return BL.FBLogin(req.body).then((result) => {
    console.log(result);
    res.send({'message' : result+''});
  }).catch((err) => {
    next(err);
  })
})

router.post('/fbregister', function(req, res, next) {
  return BL.FBRegister(req.body).then((result) => {
    console.log(result);
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/getBookings', function(req, res, next) {
  return BL.GetBookings(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/getBookingsAdmin', function(req, res, next) {
  return BL.GetBookingsForAdmin(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/systemChange', function(req, res, next) {
  return BL.SystemChange(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/getBookingForSystem', function(req, res, next) {
  return BL.SystemBooking(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/changeStatus', function(req, res, next) {
  return BL.ChangeStatus(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/dataUpload', function(req, res, next) {
  return BL.AddCafe(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
});

router.post('/dataUpdate', function(req, res, next) {
  return BL.EditCafe(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
});

router.post('/editCafe', function(req, res, next) {
  return BL.EditCafeFromWeb(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
});

router.post('/gameUpload', function(req, res, next) {
  console.log(req.body);
  return BL.AddGame(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
});

router.post('/loginAdmin', function(req, res, next) {
  return BL.Login(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.get('/getGames', function(req, res, next) {
  return BL.GetGames().then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/removeCafe', function(req, res, next) {
  return BL.DeleteCafe(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/checkNumber', function(req, res, next) {
  return BL.RegisteredNumber(req.body).then((result) => {
    res.send({message:""+result+""});
  }).catch((err) => {
    next(err);
  })
})

router.post('/checkOTP', function(req, res, next) {
  return BL.CheckOTP(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/checkUserOTP', function(req, res, next) {
  return BL.CheckUserOTP(req.body).then((result) => {
    console.log(result);
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/storeUserToken', function(req, res, next) {
  return BL.StoreUserToken(req.body).then((result) => {
    res.send({message:""+result+""});
  }).catch((err) => {
    next(err);
  })
})

router.post('/addReview', function(req, res, next) {
  return BL.AddReview(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/cancelBooking', function(req, res, next) {
  return BL.CancelBooking(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/enableDate', function(req, res, next) {
  return BL.EnableDate(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

router.post('/disableDate', function(req, res, next) {
  return BL.DisableDate(req.body).then((result) => {
    res.send(result);
  }).catch((err) => {
    next(err);
  })
})

module.exports = router;
