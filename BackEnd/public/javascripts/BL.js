var DAL = require('./DAL');
var bcrypt = require('bcryptjs');
var GameOnBL = {}
var S3path =  "https://gameon-cafe-images.s3.ap-south-1.amazonaws.com/"

GameOnBL.RegisterUser = function(data) {
    return DAL.RegisterUser(data).then((result) => {
        if(result == null) {
            return "Account creation failed. Please try again";
        }
        return result;
    });
};

GameOnBL.LoginUserPassword = function(data) {
    return DAL.LoginUserPassword(data).then((result) => {
        if(result == null) {
            return {message : "error, no user found"};
        }
        else {
            if(bcrypt.compareSync(data.pass, result.Password)) {
                return result;
            }
            else{
                return {message : "error, wrong password"};
            }
        }   
    })
}


GameOnBL.LoginUser = function(data) {
    return DAL.LoginUser(data).then((result) => {
        if(result == null) {
            return 'Error';
        }
        return result;
    })
}

GameOnBL.ResendOTP = function(data) {
    return DAL.ResendOTP(data).then((result) => {
        if(result != null) {
            return {message: result};
        }
        else {
            return {message: "failed"};
        }
    })
}

GameOnBL.CafeLoginUser = function(data) {
    return DAL.CafeLoginUser(data).then((result) => {
        if(result == null) {
            return {message : "failed"};
        }
        else {
            if(bcrypt.compareSync(data.password, result.Password)) {
                return result;
            } else {
                return {message : "failed"};
            }
        }   
    })
}

GameOnBL.CheckEmail = function(data) {
    return DAL.CheckEmail(data).then((result) => {
        if(result == null) {
            return "success";
        }
        else {
            throw new Error('an account with this email address already exists! Please try with another email address')
        }
    })
}

GameOnBL.CheckName = function(data) {
    return DAL.CheckName(data).then((result) => {
        if(result == null) {
            return "success";
        }
        else {
            throw new Error('Username already exists')
        }
    })
}

GameOnBL.CheckNumber = function(data) {
    return DAL.CheckNumber(data).then((result) => {
        if(result == null) {
            return "success";
        }
        else {
            throw new Error('an account with this contact already exists! Please try with another contact number')
        }
    })
}

GameOnBL.GetCafes = function() {
    return DAL.GetCafes().then((result) => {
        return result;
    })
}

GameOnBL.GetCafe = function(data) {
    return DAL.GetCafe(data).then((result) => {
        if(result == null) {
            return {message : "failed"};
        }
        return result;
    })
}

GameOnBL.GetUser = function(data) {
    return DAL.GetUser(data).then((result) => {
        if(result == null) {
            return {message : "failed"};
        }
        return result;
    })
}

GameOnBL.CafeSystems = function(data) {
    return DAL.CafeSystems(data).then((result) => {
        if(result == null) {
            return {message : "failed"};
        }
        return result;
    })
}

GameOnBL.BookingAvailabilityPC = function(data) {
    return DAL.BookingAvailabilityPC(data).then((result) => {
        return result;
    })
}

GameOnBL.BookingAvailabilityConsole = function(data) {
    return DAL.BookingAvailabilityConsole(data).then((result) => {
        return result;
    })
}

GameOnBL.BookPC = function(data) {
    return DAL.BookPC(data).then((result) => {
        return result;
    })
}

GameOnBL.BookConsole = function(data) {
    return DAL.BookConsole(data).then((result) => {
        return result;
    })
}

GameOnBL.PreviousBooking = function(data) {
    return DAL.PreviousBooking(data).then((result) => {
        return result; 
    })
}

GameOnBL.UpcomingBooking = function(data) {
    return DAL.UpcomingBooking(data).then((result) => {
        return result; 
    })
}

GameOnBL.BookedDates = function(data) {
    return DAL.BookedDates(data).then((result) => {
        return result; 
    })
}

GameOnBL.FBLogin = function(data) {
    return DAL.FBLogin(data).then((result) => {
        return result; 
    })
}

GameOnBL.FBRegister = function(data) {
    return DAL.FBRegister(data).then((result) => {
        return result; 
    })
}

GameOnBL.GetBookings = function(data) {
    return DAL.GetBookings(data).then((result) => {
        return result; 
    })
}

GameOnBL.GetBookingsForAdmin = function(data) {
    return DAL.GetBookingsForAdmin(data).then((result) => {
        return result; 
    })
}

GameOnBL.SystemChange = function(data) {
    return DAL.SystemChange(data).then((result) => {
        return result; 
    })
}

GameOnBL.SystemBooking = function(data) {
    return DAL.SystemBooking(data).then((result) => {
        return result; 
    })
}

GameOnBL.ChangeStatus = function(data) {
    return DAL.ChangeStatus(data).then((result) => {
        return result; 
    })
}

GameOnBL.AddAccount = function(data) {
    //console.log('2');
    return DAL.AddAccount(data).then((result) => {
        return result;
    })
}

GameOnBL.AddContact = function(data) {
    return DAL.AddContact(data).then((result) => {
        return result;
    })
}

GameOnBL.AddAddress = function(data) {
    return DAL.AddAddress(data).then((result) => {
        return result;
    })
}

GameOnBL.Login = function(data) {
    return DAL.Login(data).then((result) => {
      if(result == null) {
        console.log('here');
        throw new Error('Error');
      }
      else {
        if(bcrypt.compareSync(data.password, result.Password)) {
            return result;
        }
        else{
            throw new Error('Error');
        }
      }
    })
  }

GameOnBL.GetGames = function() {
    return DAL.GetGames().then((result) => {
        return result;
    })
}

GameOnBL.AddCafe = function(data) {
    data.Main = S3path+data.Main;
    for(let i=0;i<data.Slides.length;i++) {
        data.Slides[i] = S3path+data.Slides[i];
    }
    return DAL.AddCafe(data).then((result) => {
        return result;
    })
}

GameOnBL.EditCafe = function(data) {
    data.Main = S3path+data.Main;
    for(let i=0;i<data.Slides.length;i++) {
        data.Slides[i] = S3path+data.Slides[i];
    }
    return DAL.EditCafe(data).then((result) => {
        return result;
    })
}

GameOnBL.EditCafeFromWeb = function(data) {
    return DAL.EditCafeFromWeb(data).then((result) => {
        return result;
    })
}


GameOnBL.AddGame = function(data) {
    return DAL.AddGame(data).then((result) => {
        return result;
    })
}

GameOnBL.DeleteCafe = function(data) {
    return DAL.DeleteCafe(data).then((result) => {
        return result;
    })
}

GameOnBL.RegisteredNumber = function(data) {
    return DAL.RegisteredNumber(data).then((result) => {
        if(result != null) {
            return "success";
        }
        else {
            return "failed";
        }
    })
}

GameOnBL.CheckOTP = function(data) {
    data.OTP = Number(data.OTP);
    return DAL.CheckOTP(data).then((result) => {
        if(result != null) {
            return result;
        }
        else {
            return {message: "failed"};
        }
    })
}

GameOnBL.CheckUserOTP = function(data) {
    data.OTP = Number(data.OTP);
    return DAL.CheckUserOTP(data).then((result) => {
        if(result != null) {
            return result;
        }
        else {
            return {message: "failed"};
        }
    })
}

GameOnBL.StoreUserToken = function(data) {
    return DAL.StoreUserToken(data).then((result) => {
        if(result != null) {
            return {message: "success"};
        }
        else {
            return {message: "failed"};
        }
    })
}

GameOnBL.AddReview = function(data) {
    return DAL.AddReview(data).then((result) => {
        return result;
    })
}

GameOnBL.CancelBooking = function(data) {
    return DAL.CancelBooking(data).then((result) => {
        return result;
    })
}

GameOnBL.EnableDate = function(data) {
    return DAL.EnableDate(data).then((result) => {
        return result;
    })
}

GameOnBL.DisableDate = function(data) {
    return DAL.DisableDate(data).then((result) => {
        return result;
    })
}


module.exports = GameOnBL