var MongoClient = require('mongodb');
// var ObjectId = MongoClient.ObjectID;
var GameOn = process.env.DB_LINK
var GameOnDAL = {};
var dbClient = MongoClient.connect(GameOn, { useNewUrlParser: true });
var nodemailer = require('nodemailer');
var bcrypt = require('bcryptjs');
var AWS = require('aws-sdk');
// var request = require('request-promise');

AWS.config.update({
    secretAccessKey: process.env.SECRECT_KEY,
    accessKeyId: process.env.ACCESS_KEY,
    region: 'ap-southeast-1'
  });

var transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

GameOnDAL.RegisterUser = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.findOne({'Email': data.email}).then((result) => {
            if(result == null) {
                connection = client.db().collection('Users');
                return connection.findOne({'Contact': data.phone}).then((result) => {
                    if(result == null) {
                        let date = new Date();
                        let id = "U"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
                        connection = client.db().collection('Users');
                        return connection.insertOne({'UserId':id, 'Username':data.name, 'Password':data.pass, 'Email':data.email, 'Contact':data.phone, 'DP':data.dp, 'GCredits':0}).then((result) =>{
                            if(result.insertedCount == 1){
                                let OTP = Math.floor(100000 + Math.random() * 900000);
                                connection = client.db().collection('OTP Logs');
                                return connection.findOne({'RMobile': data.phone}).then((result) => {
                                    if(result == null) {
                                        connection = client.db().collection('OTP Logs');
                                        return connection.insertOne({'RMobile': data.phone, 'OTP': OTP}).then((result) => {
                                            if(result.insertedCount == 1) {
                                                SendSMS(OTP, data.phone);
                                                return Number(OTP);
                                            } else {
                                                return null;
                                            }
                                        })
                                    } else {
                                        connection = client.db().collection('OTP Logs');
                                        return connection.deleteOne({'RMobile': data.phone}).then((result) => {
                                            if(result.deletedCount == 1) {
                                                connection = client.db().collection('OTP Logs');
                                                return connection.insertOne({'RMobile': data.phone, 'OTP': OTP}).then((result) => {
                                                    if(result.insertedCount == 1) {
                                                        SendSMS(OTP, data.phone);
                                                        return Number(OTP);
                                                    } else {
                                                        return null;
                                                    }
                                                })
                                            } else {
                                                return null;
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    } else {
                        return "An account with this phone number already exists";
                    }
                })
            } else {
                return "An account with this email address already exists";
            }
        })
    })
}

GameOnDAL.LoginUserPassword = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        if(data.user.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)){
            data.user = data.user.toLowerCase();
            return connection.findOne({'Email':data.user}).then((result) => {
				return result;
			})
        }
        else {
			return connection.findOne({'Username':data.user}).then((result) => {
				return result;
			})
        }
    })
}


GameOnDAL.LoginUser = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.findOne({'Contact': data.phone}).then((result) => {
            if(result != null) {
                let OTP = Math.floor(100000 + Math.random() * 900000);
                connection = client.db().collection('OTP Logs');
                return connection.findOne({'RMobile': data.phone}).then((result) => {
                    if(result == null) {
                        connection = client.db().collection('OTP Logs');
                        return connection.insertOne({'RMobile': data.phone, 'OTP': OTP}).then((result) => {
                            if(result.insertedCount == 1) {
                                SendSMS(OTP, data.phone);
                                return OTP;
                            } else {
                                return null;
                            }
                        })
                    } else {
                        connection = client.db().collection('OTP Logs');
                        return connection.deleteOne({'RMobile': data.phone}).then((result) => {
                            if(result.deletedCount == 1) {
                                connection = client.db().collection('OTP Logs');
                                return connection.insertOne({'RMobile': data.phone, 'OTP': OTP}).then((result) => {
                                    if(result.insertedCount == 1) {
                                        SendSMS(OTP, data.phone);
                                        return OTP;
                                    } else {
                                        return null;
                                    }
                                })
                            } else {
                                return null;
                            }
                        })
                    }
                })
            } else {
                return null;
            }
        })
    })
}

GameOnDAL.ResendOTP = function(data) {
    return dbClient.then((client) => {
        let OTP = Math.floor(100000 + Math.random() * 900000);
        connection = client.db().collection('OTP Logs');
        return connection.findOne({'RMobile': data.number}).then((result) => {
            if(result == null) {
                connection = client.db().collection('OTP Logs');
                return connection.insertOne({'RMobile': data.number, 'OTP': OTP}).then((result) => {
                    if(result.insertedCount == 1) {
                        SendSMS(OTP, data.number);
                        return OTP;
                    } else {
                        return null;
                    }
                })
            } else {
                connection = client.db().collection('OTP Logs');
                return connection.deleteOne({'RMobile': data.number}).then((result) => {
                    if(result.deletedCount == 1) {
                        connection = client.db().collection('OTP Logs');
                        return connection.insertOne({'RMobile': data.number, 'OTP': OTP}).then((result) => {
                            if(result.insertedCount == 1) {
                                SendSMS(OTP, data.number);
                                return OTP;
                            } else {
                                return null;
                            }
                        })
                    } else {
                        return null;
                    }
                })
            }
        })
    })
}

GameOnDAL.CheckUserOTP = function(data) {
    console.log(data);
    return dbClient.then((client) => {
        connection = client.db().collection('OTP Logs');
        return connection.findOne({$and :[{'RMobile': data.number}, {'OTP': data.OTP}]}).then((result) => {
            if(result != null) {
                connection = client.db().collection('OTP Logs');
                return connection.deleteOne({'RMobile': data.number}).then((result) => {
                    if(result.deletedCount == 1) {
                        connection = client.db().collection('Users');
                        return connection.findOne({'Contact': data.number}).then((result) => {
                            if(result != null) {
                                return result
                            }
                            return result;
                        })
                    } else {
                        return null;
                    }
                })
            } else {
                return result;
            }
        })
    })
}

GameOnDAL.CafeLoginUser = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafe Partners');
        if(data.username.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)){
            return connection.findOne({'Email':data.username}).then((result) => {
				return result;
			})
        } else {
            return connection.findOne({'Username':data.username}).then((result) => {
				return result;
			})
        }
    })
}

GameOnDAL.CheckEmail = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.findOne({'Email':data}).then((result) => {
            return result;
        })
    })
}

GameOnDAL.CheckName = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.findOne({'Username':data}).then((result) => {
            //console.log(result);
            return result;
        })
    })
}

GameOnDAL.CheckNumber = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.findOne({'Contact':data}).then((result) => {
            //console.log(result);
            return result;
        })
    })
}

GameOnDAL.GetCafes = function() {
	return dbClient.then((client) => {
		connection = client.db().collection('Cafes');
		return connection.find({}).toArray().then((result) => {
			return result;
		})
	})
}

GameOnDAL.GetCafe = function(data) {
	return dbClient.then((client) => {
		connection = client.db().collection('Cafes');
		return connection.findOne({'CafeId': data.CafeId}).then((result) => {
			return result;
		})
	})
}

GameOnDAL.GetUser = function(data) {
	return dbClient.then((client) => {
		connection = client.db().collection('Users');
		return connection.findOne({'UserId': data.UserId}).then((result) => {
			return result;
		})
	})
}

GameOnDAL.CafeSystems = function(data) {
    return dbClient.then((client) => {
		connection = client.db().collection('Cafes');
		if(data.Platform == 'PC') {
            return connection.findOneAndUpdate({'CafeId': data.CafeId}, {$set: {'PC_nos': data.PC_nos}},{returnNewDocument : true}).then((result) => {
                if(result.CafeId)
                    return result;
                else
                    return null;
            })
        } else {
            return connection.findOneAndUpdate({'CafeId': data.CafeId}, {$set: {'Console_nos': data.Console_nos}},{returnNewDocument : true}).then((result) => {
                if(result.CafeId)
                    return result;
                else
                    return null;
            })
        }
	})
}


GameOnDAL.BookingAvailabilityPC = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Bookings');
        return connection.find({$and : [{'Date': data.date}, {'Cafe.CafeId': data.CafeId}, {'Platform': 'PC'}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
            if(result.length == 0){
                connection = client.db().collection('Cafes');
                return connection.findOne({'CafeId': data.CafeId}).then((result) => {
                    if(result) {
                        return result.PC_nos.length;
                    }
                })
            }
            else{
                let arr = [];
                for(let i=0; i < result.length; i++) {
                    if(result[i].EndTime <= data.start || result[i].StartTime >= data.end) {
                        continue;
                    }
                    else if(result[i].StartTime <= data.start || result[i].EndTime >= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime >= data.start && result[i].EndTime <= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime >= data.start && result[i].EndTime >= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime <= data.start && result[i].EndTime <= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                }
                return data.available - arr.length;
            }
            
        })
    })
}

GameOnDAL.BookingAvailabilityConsole = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Bookings');
        return connection.find({$and : [{'Date': data.date}, {'Cafe.CafeId': data.CafeId}, {'Platform': 'Console'}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
            if(result.length == 0){
                connection = client.db().collection('Cafes');
                return connection.findOne({'CafeId': data.CafeId}).then((result) => {
                    if(result) {
                        return result.Console_nos.length;
                    }
                })
            }
            else{
                let arr = [];
                for(let i=0; i < result.length; i++) {
                    if(result[i].EndTime <= data.start || result[i].StartTime >= data.end) {
                        continue;
                    }
                    else if(result[i].StartTime <= data.start || result[i].EndTime >= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime >= data.start && result[i].EndTime <= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime >= data.start && result[i].EndTime >= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                    else if(result[i].StartTime <= data.start && result[i].EndTime <= data.end) {
                        arr = [...new Set([...arr, ...result[i].BookedSystems])];
                    }
                }
                return data.available - arr.length;
            }
            
        })
    })
}

GameOnDAL.BookPC = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.findOne({'CafeId': data.Cafe.CafeId}).then((resultCafe) => {
            if(resultCafe !== null) {
                let arrSys = resultCafe.PC_nos;
                connection = client.db().collection('Bookings');
                return connection.find({$and : [{'Date': data.Date}, {'Cafe.CafeId': data.Cafe.CafeId}, {'Platform': 'PC'}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
                    if(result.length == 0){
                        data.BookedSystems = arrSys.slice(0, data.TotalSystems);
                        data.Platform = 'PC'
                        let date = new Date();
                        let id = "B"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
                        data.BookingId = id;
                        connection = client.db().collection('Bookings');
                        return connection.insertOne(data).then((result) => {
                            if(result.insertedCount == 1){
                                console.log("1 PC1")
                                connection = client.db().collection('Users');
                                return connection.findOneAndUpdate({'UserId': data.User.UserId}, {$inc : {'GCredits': -data.GCreditsUsed}},{returnNewDocument : true}).then((resultNew) => {
                                    if(resultNew.value.UserId) {
                                        let earning = (resultCafe.CostPC * data.TotalHours) * data.TotalSystems;
                                        connection = client.db().collection('Cafes');
                                        return connection.findOneAndUpdate({'CafeId': data.Cafe.CafeId}, {$inc : {'TotalEarnings': earning}},{returnNewDocument : true}).then((resultent) => {
                                            if(resultent.lastErrorObject.updatedExisting) {
                                                return [data, resultNew.value];
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    } else{
                        let arr = [];
                        for(let i=0; i < result.length; i++) {
                            if(result[i].EndTime <= data.StartTime || result[i].StartTime >= data.EndTime) {
                                continue;
                            }
                            else if(result[i].StartTime <= data.StartTime || result[i].EndTime >= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime >= data.StartTime && result[i].EndTime <= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime >= data.StartTime && result[i].EndTime >= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime <= data.StartTime && result[i].EndTime <= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                        }
                        let arrRes = [], i = 0;
                        arrSys.forEach(function(key) {
                            if (arr.indexOf(key) === -1 && i < data.TotalSystems) {
                                arrRes.push(key);
                                i++;
                            }
                        });
                        data.BookedSystems = arrRes;
                        data.Platform = 'PC'
                        let date = new Date();
                        let id = "B"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
                        data.BookingId = id;
                        connection = client.db().collection('Bookings');
                        return connection.insertOne(data).then((result) => {
                            if(result.insertedCount == 1){
                                console.log("1 PC2")
                                connection = client.db().collection('Users');
                                return connection.findOneAndUpdate({'UserId': data.User.UserId}, {$inc : {'GCredits': -data.GCreditsUsed}},{returnNewDocument : true}).then((resultNew) => {
                                    if(resultNew.value.UserId) {
                                        let earning = (resultCafe.CostPC * data.TotalHours) * data.TotalSystems;
                                        connection = client.db().collection('Cafes');
                                        return connection.findOneAndUpdate({'CafeId': data.Cafe.CafeId}, {$inc : {'TotalEarnings': earning}},{returnNewDocument : true}).then((resultent) => {
                                            if(resultent.lastErrorObject.updatedExisting) {
                                                return [data, resultNew.value];
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })
}

GameOnDAL.BookConsole = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.findOne({'CafeId': data.Cafe.CafeId}).then((resultCafe) => {
            if(resultCafe !== null) {
                let arrSys = resultCafe.Console_nos;
                connection = client.db().collection('Bookings');
                return connection.find({$and : [{'Date': data.Date}, {'Cafe.CafeId': data.Cafe.CafeId}, {'Platform': 'Console'}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
                    if(result.length == 0){
                        data.BookedSystems = arrSys.slice(0, data.TotalSystems);
                        data.Platform = 'Console'
                        let date = new Date();
                        let id = "B"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
                        data.BookingId = id;
                        connection = client.db().collection('Bookings');
                        return connection.insertOne(data).then((result) => {
                            if(result.insertedCount == 1){
                                connection = client.db().collection('Users');
                                return connection.findOneAndUpdate({'UserId': data.User.UserId}, {$inc : {'GCredits': -data.GCreditsUsed}},{returnNewDocument : true}).then((resultNew) => {
                                    if(resultNew.value.UserId) {
                                        let earning = (resultCafe.CostConsole * data.TotalHours) * data.TotalSystems;
                                        connection = client.db().collection('Cafes');
                                        return connection.findOneAndUpdate({'CafeId': data.Cafe.CafeId}, {$inc : {'TotalEarnings': earning}},{returnNewDocument : true}).then((resultent) => {
                                            if(resultent.lastErrorObject.updatedExisting) {
                                                return [data, resultNew.value];
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    } else{
                        let arr = [];
                        for(let i=0; i < result.length; i++) {
                            if(result[i].EndTime <= data.StartTime || result[i].StartTime >= data.EndTime) {
                                continue;
                            }
                            else if(result[i].StartTime <= data.StartTime || result[i].EndTime >= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime >= data.StartTime && result[i].EndTime <= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime >= data.StartTime && result[i].EndTime >= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                            else if(result[i].StartTime <= data.StartTime && result[i].EndTime <= data.EndTime) {
                                arr = [...new Set([...arr, ...result[i].BookedSystems])];
                            }
                        }
                        let arrRes = [], i = 0;
                        arrSys.forEach(function(key) {
                            if (arr.indexOf(key) === -1 && i < data.TotalSystems) {
                                arrRes.push(key);
                                i++;
                            }
                        });
                        data.BookedSystems = arrRes;
                        data.Platform = 'Console'
                        let date = new Date();
                        let id = "B"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
                        data.BookingId = id;
                        connection = client.db().collection('Bookings');
                        return connection.insertOne(data).then((result) => {
                            if(result.insertedCount == 1){
                                connection = client.db().collection('Users');
                                return connection.findOneAndUpdate({'UserId': data.User.UserId}, {$inc : {'GCredits': -data.GCreditsUsed}},{returnNewDocument : true}).then((resultNew) => {
                                    if(resultNew.value.UserId) {
                                        let earning = (resultCafe.CostConsole * data.TotalHours) * data.TotalSystems;
                                        connection = client.db().collection('Cafes');
                                        return connection.findOneAndUpdate({'CafeId': data.Cafe.CafeId}, {$inc : {'TotalEarnings': earning}},{returnNewDocument : true}).then((resultent) => {
                                            if(resultent.lastErrorObject.updatedExisting) {
                                                return [data, resultNew.value];
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })
}


GameOnDAL.PreviousBooking = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Bookings');
        return connection.find({$or : [{$and : [{'User.UserId': data.UserId}, {'DateTime' : {$lt : data.Date}}]}, {$and : [{'User.UserId': data.UserId}, {'DateTime' : {$eq : data.Date}}, {'EndTime' : {$lt : data.Time}}]}]}).toArray().then((result) => {
            if(result.length != 0)
                return result;
            else
                return [];
        })
    })
}

GameOnDAL.UpcomingBooking = function(data) {
    return dbClient.then((client) => {
        console.log(data);
        connection = client.db().collection('Bookings');
        return connection.find({$or : [{$and : [{'User.UserId': data.UserId}, {'DateTime' : {$gt : data.Date}}, {'Status' : {$ne : "Cancelled"}}]}, {$and : [{'User.UserId': data.UserId}, {'DateTime' : {$eq : data.Date}}, {'StartTime' : {$gte : data.Time}}, {'Status' : {$ne : "Cancelled"}}]}]}).toArray().then((result) => {
            if(result.length != 0)
                return result.reverse();
            else
                return [];
        })
    })
}

GameOnDAL.BookedDates = function(data) {
    return dbClient.then((client) => {
		connection = client.db().collection('Bookings');
		if(data.choice == 'PC') {
            return connection.find({$and : [{'Date': data.date},{'Cafe.CafeId': data.CafeId},{'AvailablePCs' : 0}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
                if(result.length != 0)
                    return result;
                else
                    return [];
            })
        } else {
            return connection.find({$and : [{'Date': data.date},{'Cafe.CafeId': data.CafeId},{'AvailableConsoles' : 0}, {'Status' : {$ne : "Cancelled"}}]}).toArray().then((result) => {
                if(result.length != 0)
                    return result;
                else
                    return [];
            })
        }
	})
}

GameOnDAL.FBLogin = function(data) {
    return dbClient.then((client) => {
		connection = client.db().collection('Users');
		return connection.findOne({$and : [{'Username': data.Username},{'Email': data.Email}]}).then((result) => {
			if(result != null)
                return "login";
            else
                return "register";
		})
	})
}

GameOnDAL.FBRegister = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Users');
        return connection.insertOne(data).then((result) => {
            if(result.insertedCount == 1){
                return data;
            }
        })
    })
}

GameOnDAL.GetBookings = function(data) {
    return dbClient.then((client) => {
		connection = client.db().collection('Bookings');
		return connection.find({$and : [{'Date': data.date},{'Cafe.CafeId': data.cafeid}]}).toArray().then((result) => {
			if(result.length != 0)
                return result;
            else
                return [];
		})
	})
}

GameOnDAL.GetBookingsForAdmin = function(data) {
    return dbClient.then((client) => {
		connection = client.db().collection('Bookings');
		return connection.find({'Date': data.date}).toArray().then((result) => {
			if(result.length != 0)
                return result;
            else
                return [];
		})
	})
}

GameOnDAL.SystemBooking = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Bookings');
        return connection.find({$and : [{'Date': data.date}, {'Cafe.CafeId': data.CafeId}, {'Platform': data.Platform}]}).toArray().then((result) => {
            if(result.length != 0) {
                let arr = [];
                result.forEach((doc) => {
                    if(doc.BookedSystems.indexOf(data.system) > -1) {
                        arr.push(doc);
                    }
                })
                if(arr.length > 0)
                    return arr;
                else
                    return [];
            }
            else
                return [];
        })
    })
}

GameOnDAL.ChangeStatus = function(data) {
	return dbClient.then((client) => {
        connection = client.db().collection('Bookings');
        return connection.findOneAndUpdate({'BookingId': data.id}, {$set: {'Status': data.status}},{returnNewDocument : true}).then((result) => {
            return result;
        })
    })
}

GameOnDAL.SystemChange = function(data) {
	return dbClient.then((client) => {
        data.System = data.System.split(',');
        let arr = [];
        for(let i = 0; i < data.System.length; i++) {
            arr.push(Number(data.System[i]));
        }
        connection = client.db().collection('Bookings');
        return connection.findOneAndUpdate({'BookingId': data.BookingId}, {$set: {'BookedSystems': arr}},{returnNewDocument : true}).then((result) => {
            return result;
        })
    })
}

GameOnDAL.AddAccount = function(data) {
	return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
		return connection.findOneAndUpdate({'CafeId': data.CafeId}, {$set: {'BankDetails.Bank': data.bank, 'BankDetails.Account': data.account}},{returnOriginal: false}).then((result) => {
			if(result.lastErrorObject.updatedExisting){
                return result.value;
            }
            else {
                return null
            }
		})
	})
}

GameOnDAL.AddContact = function(data) {
	return dbClient.then((client) => {
		connection = client.db().collection('Cafes');
		return connection.findOne({'CafeId': data.CafeId}, {$set: {'Phone': data.number, 'Email': data.email}},{returnOriginal: false}).then((result) => {
			if(result.lastErrorObject.updatedExisting){
                return result.value;
            }
            else {
                return null
            }
		})
	})
}

GameOnDAL.AddAddress = function(data) {
	return dbClient.then((client) => {
		connection = client.db().collection('Cafes');
		return connection.findOne({'CafeId': data.CafeId}, {$set: {'Location': data.location, 'Address': data.Address}},{returnOriginal: false}).then((result) => {
			if(result.lastErrorObject.updatedExisting){
                return result.value;
            }
            else {
                return null
            }
		})
	})
}

GameOnDAL.Login = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Admin');
        return connection.findOne({'Admin':data.username}).then((result) => {
          return result;
      })
    })
  }

GameOnDAL.GetGames = function() {
    return dbClient.then((client) => {
        connection = client.db().collection('Games');
        return connection.find({},{_id:0}).toArray().then((result) => {
            return result;
        })
    })
}

GameOnDAL.AddCafe = function(data) {
    return dbClient.then((client) => {
        let date = new Date();
        let id = "G"+date.getDate()+''+(date.getMonth()+1)+''+date.getFullYear().toString().substr(-2)+''+date.getHours()+''+date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
        data.CafeId = id;
        connection = client.db().collection('Cafes');
        return connection.insertOne(data).then((result) => {
            if(result.insertedCount == 1){
                let password = randomString();
                bcrypt.hash(password, 10, function(err, hash){
                    if(hash) {
                        return dbClient.then((client) => {
                            connection = client.db().collection('Cafe Partners');
                            return connection.insertOne({'Email':data.Email, 'Username':data.Vendor, 'Password':hash, 'RMobile':data.Contact.Contact_1, 'CafeId':data.CafeId}).then((result) => {
                                if(result.insertedCount == 1){
                                    Mailer(data.Vendor, data.Email, data.Name, data.CafeId, password);
                                    return data;
                                }
                            })
                        })
                    }
                })
            }
        })
    })
}

GameOnDAL.EditCafe = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.replaceOne({'CafeId' : data.CafeId}, data).then((result) => {
            if(result.result.nModified) {
                connection = client.db().collection('Cafe Partners');
                return connection.findOneAndUpdate({'CafeId' : data.CafeId}, {$set: {'RMobile': data.Contact.Contact_1}},{returnNewDocument : true}).then((resultNew) => {
                    return result;
                })
            }
        })
    })
}

GameOnDAL.EditCafeFromWeb = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.replaceOne({'CafeId' : data.CafeId}, data).then((result) => {
            if(result.result.nModified) {
                connection = client.db().collection('Cafe Partners');
                return connection.findOneAndUpdate({'CafeId' : data.CafeId}, {$set: {'RMobile': data.Contact.Contact_1}},{returnNewDocument : true}).then((resultNew) => {
                    return result;
                })
            }
        })
    })
}

GameOnDAL.AddGame = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Games');
        return connection.insertOne(data).then((result) => {
            return result;
        })
    })
}

GameOnDAL.DeleteCafe = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.deleteOne({'CafeId':data.CafeId}).then((result) => {
            if(result.deletedCount == 1) {
                return dbClient.then((client) => {
                    connection = client.db().collection('Cafe Partners');
                    return connection.deleteOne({'CafeId':data.CafeId}).then((result) => {
                        if(result.deletedCount == 1) {
                            return result;
                        }
                    })
                })
            }
        })
    })
}

GameOnDAL.RegisteredNumber = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafe Partners');
        return connection.findOne({'RMobile':data.number}).then((result) => {
            if(result != null) {
                let OTP = Math.floor(100000 + Math.random() * 900000);
                connection = client.db().collection('OTP Logs');
                return connection.findOne({'RMobile': data.number}).then((result) => {
                    if(result == null) {
                        connection = client.db().collection('OTP Logs');
                        return connection.insertOne({'RMobile': data.number, 'OTP': OTP}).then((result) => {
                            if(result.insertedCount == 1) {
                                SendSMS(OTP, data.number);
                                return data;
                            } else {
                                return null;
                            }
                        })
                    } else {
                        connection = client.db().collection('OTP Logs');
                        return connection.deleteOne({'RMobile': data.number}).then((result) => {
                            if(result.deletedCount == 1) {
                                connection = client.db().collection('OTP Logs');
                                return connection.insertOne({'RMobile': data.number, 'OTP': OTP}).then((result) => {
                                    if(result.insertedCount == 1) {
                                        SendSMS(OTP, data.number);
                                        return data;
                                    } else {
                                        return null;
                                    }
                                })
                            } else {
                                return null;
                            }
                        })
                    }
                })
            } else {
                return result;
            }
        })
    })
}

GameOnDAL.CheckOTP = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('OTP Logs');
        return connection.findOne({$and :[{'RMobile': data.number}, {'OTP': data.OTP}]}).then((result) => {
            if(result != null) {
                connection = client.db().collection('OTP Logs');
                return connection.deleteOne({'RMobile': data.number}).then((result) => {
                    if(result.deletedCount == 1) {
                        connection = client.db().collection('Cafe Partners');
                        return connection.findOne({'RMobile': data.number}).then((result) => {
                            if(result != null) {
                                return {username : result.Username,
                                        cafeid : result.CafeId}
                            }
                            return result;
                        })
                    } else {
                        return null;
                    }
                })
            } else {
                return result;
            }
        })
    })
}

GameOnDAL.StoreUserToken = function(data) {
	return dbClient.then((client) => {
        connection = client.db().collection('Users');
		return connection.findOneAndUpdate({'Contact': data.phone}, {$set: {'DeviceToken': data.token}},{returnNewDocument : true}).then((result) => {
			if(result.DeviceToken){
                return result;
            }
            else {
                return null
            }
		})
	})
}

GameOnDAL.AddReview = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('CafeReviews');
        return connection.insertOne(data).then((result) => {
          if(result.insertedCount == 1) {
              connection = client.db().collection('Cafes');
              return connection.findOne({'CafeId' : data.CafeId}).then((result) => {
                if(result) {
                    let res = (result.Rating+data.Review.Rating)/2;
                    connection = client.db().collection('Cafes');
                    return connection.findOneAndUpdate({'CafeId':data.CafeId}, {$set: {'Rating': res}}, {returnNewDocument: true}).then((result2) => {
                        if(result2.value.CafeId) {
                            return result2.value;
                        }
                    })
                }
              })
          }
      })
    })
}

GameOnDAL.CancelBooking = function(data) {
    return dbClient.then((client) => {
        console.log(data);
        connection = client.db().collection('Bookings');
        return connection.findOneAndUpdate({'BookingId': data.BookingId}, {$set: {'Status': 'Cancelled', 'PaymentInfo.status': 'credited'}}, {returnNewDocument: true}).then((result) => {
            console.log(result.value);
            if(result.lastErrorObject.updatedExisting) {
                console.log("here");
                connection = client.db().collection('Cafes');
                return connection.findOne({'CafeId': result.value.Cafe.CafeId}).then(async (resultCafe) => {
                    if(resultCafe) {
                        let actualcost = 0;
                        if(result.value.Platform == "PC") {
                            actualcost = (resultCafe.CostPC * result.value.TotalHours) * result.value.TotalSystems;
                        } else {
                            actualcost = (resultCafe.CostConsole  * result.value.TotalHours) * result.value.TotalSystems;
                        }
                        if(result.value.GCreditsUsed > 0 && (result.value.GCreditsUsed*0.1 == result.value.TotalCost)) {
                            console.log("No credit");
                            connection = client.db().collection('Users');
                            return connection.findOneAndUpdate({'UserId': data.UserId}, {$inc: {'GCredits': result.value.GCreditsUsed}}, {returnNewDocument: true}).then((resultCredit) => {
                                if(resultCredit.lastErrorObject.updatedExisting) {
                                    connection = client.db().collection('Cafes');
                                    return connection.findOneAndUpdate({'CafeId': result.value.Cafe.CafeId}, {$inc: {'TotalEarnings': -actualcost}}, {returnNewDocument: true}).then((resultDeduction) => {
                                        if(resultDeduction.lastErrorObject.updatedExisting) {
                                            return {message : result.value.GCreditsUsed};
                                        } 
                                    })
                                }
                            })
                        } else {
                            let costcredit = 0; 
                            costcredit = (Number(result.value.TotalCost)+Number(result.value.GCreditsUsed)) - actualcost*0.1;
                            connection = client.db().collection('Users');
                            return connection.findOneAndUpdate({'UserId': data.UserId}, {$inc: {'GCredits': costcredit}}, {returnNewDocument: true}).then((resultCredit) => {
                                if(resultCredit.lastErrorObject.updatedExisting) {
                                    connection = client.db().collection('Cafes');
                                    return connection.findOneAndUpdate({'CafeId': result.value.Cafe.CafeId}, {$inc: {'TotalEarnings': -actualcost}}, {returnNewDocument: true}).then((resultDeduction) => {
                                        if(resultDeduction.lastErrorObject.updatedExisting) {
                                            return {message : costcredit};
                                        } 
                                    })
                                }
                            })
                        }
                    }
                })
                
            }
        })
    })
}

GameOnDAL.EnableDate = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.findOneAndUpdate({'CafeId': data.CafeId}, {$pull: {'DisabledDates': data.Date}}, {returnNewDocument: true}).then((result) => {
            if(result.lastErrorObject.updatedExisting){
                return {message : "success"};
            } else {
                return {message : "failed"};
            }
        })
    })
}

GameOnDAL.DisableDate = function(data) {
    return dbClient.then((client) => {
        connection = client.db().collection('Cafes');
        return connection.findOneAndUpdate({'CafeId': data.CafeId}, {$push: {'DisabledDates': data.Date}}, {returnNewDocument: true}).then((result) => {
            if(result.lastErrorObject.updatedExisting){
                return {message : "success"};
            } else {
                return {message : "failed"};
            }
        })
    })
}

function SendSMS(OTP, number) {
    let paramsMessage = {
        Message: `${OTP} is the OTP for your GameOn account. Please do not share the OTP with anyone. GameOn never calls to ask for the OTP`, /* required */
        PhoneNumber: `${number}`,
    };
    let paramsSMS = {
        attributes: { /* required */
            'DefaultSMSType': 'Transactional',
            'DefaultSenderID': 'GAMEON',
        }
    };

    let sns = new AWS.SNS();
    sns.setSMSAttributes(paramsSMS, (err, data) => {
        if(data) {
            console.log(data);
            sns.publish(paramsMessage, (err, data) => {
                if(data) {
                    console.log(data);
                    return true;
                } else {
                    return false;
                }
            })
        } else {
            return false;
        }
    })
}

function Mailer(Name, Email, Cafe, CafeId, Password) {
    var mailOptions = {
        from: `"GameOn Cafes" info@thegameon.co`, // sender address (who sends)
        to: `${Email}`, // list of receivers (who receives)
        subject: 'Cafe registration successful ', // Subject line
        html: `<body>
        <img src="https://s3.ap-south-1.amazonaws.com/gameon-cafe-images/NewGO.png" alt="GameOn" height="60px" style="margin-top: 20px;">
        <div>
          <p>Congratulations ${Name},</p>
          <p>Your Cafe,</p>
          <div style="margin-left: 30px;">
            <p>${Cafe}</p>
            <p>Id : <span style="color : #5491f7;">${CafeId}</span></p>
          </div>
          <p>has been successfully registered with GameOn and will be up for online bookings.</p>
          <p>Please login to <a href="https://cafe.thegameon.co" style="color : #5491f7;">GameOn Cafes</a> to manage your Cafe</p>
          <p>To interact and control your online bookings please refer to your user credentials provided below:</p><br>
          <div style="margin-left: 30px;">
            <p>Email : ${Email}</p>
            <p>Password : <span style="color : #5491f7;">${Password}</span></p>
          </div><br>
          <p>Please change your password after logging in for the first time for security purposes.</p>
          <p>We hope you will find our services pleasing for your business.</p>
          <p>For any queries regarding bookings and functionalities, please feel free to drop a mail to support@thegameon.co</p>
          <p>Thank you</p>
        </div>
      </body>` // html body
    };
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log("this here ",error);
        }
        else {
            console.log(info.response);
        }
    });
}

function randomString() {
    var result = '';
    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = 10; i > 0; --i) 
        result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

// function getPayment(payment_id){
//     return request(`https://${instance.key_id}:${instance.key_secret}@api.razorpay.com/v1/payments/${payment_id}`)
//     .then((response) => {
//         return response;
//     })
//     .catch((err) => {
//         return {payment_id : payment_id};
//     });
// }

// function initiateRefund(payment_id) {
//     return request({
//         method: 'POST',
//         url: `https://${instance.key_id}:${instance.key_secret}@api.razorpay.com/v1/payments/${payment_id}/refund`,
//       }).then((response) => {
//             return response;
//         })
//         .catch((err) => {
//             return {payment_id : payment_id};
//         });
// }


module.exports = GameOnDAL;
