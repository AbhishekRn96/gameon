var fs = require('fs');
var ReqLog = function(err, req, res, next) {
    var logmesssage = " "+new Date()+" "+req.method+" "+req.url+"\n";
    fs.appendFile('RequestLogger.txt', logmesssage, function(err) {
        if(err){
            next(err);
        }
    });
    next();
}

module.exports = ReqLog;
