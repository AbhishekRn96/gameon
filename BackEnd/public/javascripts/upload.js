const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

aws.config.update({
  secretAccessKey: 'qORzJL8Y55b2fTf5nxXXGXz27KudagIIOrJJcSp8',
  accessKeyId: 'AKIAIH5CQEDBZVUGWMFA',
  region: 'ap-south-1'
});

const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true)
  } else {
      cb(new Error('Invalid Mime Type, only JPEG and PNG'), false);
  }
}

const upload = multer({
  fileFilter,
  storage: multerS3({
    s3,
    bucket: 'gameon-cafe-images',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: 'TESTING_META_DATA!'});
    },
    key: function (req, file, cb) {
      let mime = file.mimetype.split('/'); 
      cb(null, Date.now().toString()+'-'+file.fieldname+'.'+mime[mime.length - 1]);
    }
  })
})

module.exports = upload;