import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';
import LinearGradient from 'react-native-linear-gradient';
import config from "../extra/config";
import Modal from 'react-native-modal';
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  FlatList,
  BackHandler,
  Animated,
  Easing} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class BookedDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cancel : false,
      cancelModal : false,
      reload : false,
      GameId : '',
      refresh : false,
      disable : false,
    }
    this.Cancel = this.Cancel.bind(this);
    this.Refresh = this.Refresh.bind(this);
  }

  componentWillMount() {
    let item = this.props.navigation.getParam('item');
    let date1 = new Date();
    let date2 = new Date(item.DateTime);
    date1.setHours(date1.getHours()+Number(item.End));
    date2.setHours(Number(item.Hours), Number(item.Minutes));
    date1 = new Date(date1);
    date2 = new Date(date2);
    if(date1 >= date2) {
      this.setState({reload : true, disable : true});
    }
  }

  Refresh() {
    this.setState({refresh : true});
    let item = this.props.navigation.getParam('item');
    fetch(`${config.userurl}getGameId`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
        TournamentId: item.TournamentId,
				}),
			})
			.then(response =>  response.json())
			.then((responseJson) => {
        if(responseJson.message === "success") {
          this.setState({GameId : responseJson.GameId, refresh : false});
        }
      }).catch (err => {
      console.log("verify ",err);
    })
  }

  async Cancel() {
    this.setState({
      cancel : true
    })
    let item = this.props.navigation.getParam('item');
    fetch(`${config.bookingurl}cancelTBooking`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
        TBookingId: item.TBookingId,
        UserId: item.User.UserId,
        TournamentId: item.TournamentId,
        TotalCost: item.TotalCost,
				}),
			})
			.then(response =>  response.json())
			.then(async (responseJson) => {
        if(responseJson.message === "success") {
          let credit = await AsyncStorage.getItem('gcredits');
          credit = credit === undefined ? 0 + responseJson.credit : Number(credit) + responseJson.credit;
          await AsyncStorage.setItem('gcredits', credit+'');
          this.props.navigation.navigate('BookingCancelled', {credit : responseJson.credit, item : item});
        }
      }).catch (err => {
      console.log("verify ",err);
    })
  }
  
  render() {
    const { goBack } = this.props.navigation;
    const {navigate} = this.props.navigation;
    const {navigation} = this.props;
    const item = navigation.getParam('item');
    const screen = navigation.getParam('screen');
    return(
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%'}}>
            <View>
              <TouchableNativeFeedback
                disabled={this.state.cancel}
								onPress={() => goBack()}>
                <View style={{padding: 10, paddingTop: 15}}>
                  <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
          <View></View>
        </View>
        <View style={{width: '100%', bottom: 0, position: 'absolute'}}> 
          <Modal
            swipeDirection='down'
            onSwipe = {() => this.setState({cancelModal : false})}
            onBackButtonPress = {() => this.setState({cancelModal : false})}
            onBackdropPress = {() => this.setState({cancelModal : false})}
            isVisible={this.state.cancelModal}
            backdropOpacity={0.9}
            backdropColor='white'
            style={[{alignItems: 'center'}, styles.bottomModal]}>
            <View style={{width: '100%', alignItems: 'center', backgroundColor: 'white',  borderTopRightRadius: 20, borderTopLeftRadius: 20}}>
              <View style={{width: '100%', alignItems: 'center', backgroundColor: '#cccccc',  borderTopRightRadius: 20, borderTopLeftRadius: 20}}>
                <Text style={{fontSize: 16}}>=</Text>
              </View>
              <View style={{width: '90%', alignItems: 'center', marginTop: '5%'}}>
                <Text style={styles.content}>Note : Upon cancellation, <Text style={{color : 'red'}}>10%</Text> of the Total Fee would be deducted as service changes and added to your GCredits</Text>
              </View>
              <View style={{width: '90%', marginTop: '5%'}}>
                <Text style={[styles.content, {color : 'red'}]}>Are you sure you want to Cancel?</Text>
              </View>
              <View style={{marginTop: '2%', width: '100%', marginBottom: '10%', alignItems: 'center'}}>
                <TouchableNativeFeedback
                  disabled={this.state.cancel}
                  onPress={() => {
                    this.setState({cancelModal : false}, () => this.Cancel())
                    }}>
                  <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF2F03', '#A63300']} style={styles.linearGradient}>
                    <View style={{ alignItems: 'center'}}>
                      <Text style={styles.book}>  Cancel  </Text>
                    </View>
                  </LinearGradient>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
        </View>
        <View style={{width: '100%', alignItems: 'center', alignSelf: 'center'}}>
          <View style={styles.shadow}>
            <Image
              source={{uri: item.Poster}}
              style={{height: hp('30%'), borderRadius: 3}}>
            </Image>
          </View>
        </View>
        <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
          <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>{item.Name}</Text>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Booking ID - <Text style={{color : '#1C8ADB'}}>{item.TBookingId}</Text></Text>
          </View>
          {item.Status == "Cancelled" ? (
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 18, color: '#5a5a5e'}}>Status: <Text style={{color: 'red', fontWeight: '400'}}>{item.Status}</Text></Text>
            </View>
          ) : null}
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Booked for <Text style={{color : '#1C8ADB', fontSize: 18}}>{item.Date}</Text></Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Timing : </Text>
            <Text style={{color : '#1C8ADB', fontSize: 18}}>{item.Hours}:{item.Minutes}</Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Paid : <Text style={{color: 'green'}}>&#8377;{item.TotalCost}</Text></Text>
          </View>
          <View style={{marginTop: 20}}>
            {this.state.reload ? (
              <View>
                <View style={{flexDirection: 'row', padding: 10}}>
                  <Text style={{fontSize: 18, color: '#1C8ADB'}}>Refresh to get Game Room Id</Text>
                  <View style={{marginLeft: '5%'}}>
                    {this.state.refresh ? (
                      <ActivityIndicator size={22} color="#1C8ADB" />
                    ) : (
                      <Icon4 name='reload' color={'#1C8ADB'} size={22} onPress={() => this.Refresh()}></Icon4>
                    )}
                  </View>
                </View>
                <Text style={{padding: 10, fontSize: 18, color: '#1C8ADB'}}>GameRoomId : {this.state.GameId}</Text>
              </View>
            ) : (
              <Text style={{fontSize: 18, color: '#5a5a5e'}}>The Game Room Id for this Tournament will appear here {item.End} Minutes before the Tournament</Text>
            )}
          </View>
        </View>
        <View style={{alignItems : 'center', bottom: 0, position: 'absolute', width: '100%'}}>
          {this.state.disable ? null : screen == "Previous" ? null : (
            item.Status == "Cancelled" ? null :
              this.state.cancel ? (
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF2F03', '#A63300']} style={styles.linearGradient}>
                  <View style={{ alignItems: 'center'}}>
                    <ActivityIndicator size={22} color="white" style={{padding: 15,}} />
                  </View>
                </LinearGradient>
              ) : (
                <TouchableNativeFeedback
                  disabled={this.state.cancel}
                  onPress={() => this.setState({cancelModal : true})}>
                  <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF2F03', '#A63300']} style={styles.linearGradient}>
                    <View style={{ alignItems: 'center'}}>
                      <Text style={styles.book}>  Cancel  </Text>
                    </View>
                  </LinearGradient>
                </TouchableNativeFeedback>
              )
          )}
        </View> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
	},
	text: {
		padding: 10,
		fontSize: 15,
  },
  CafeName: {
		fontSize: 20,
    fontWeight: '500',
  },
  content: {
    color: '#5a5c5e', 
    fontWeight: '300', 
    flexWrap: 'wrap',
    fontSize: 14
  },
	bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 2, 
    // borderBottomWidth: 2,
    // borderRightWidth: 2,
    // borderColor: '#c6c2c2',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%',
  },
})
