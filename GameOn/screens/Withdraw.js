import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { StackActions } from 'react-navigation';
import { showMessage, hideMessage } from "react-native-flash-message";
import config from "../extra/config";
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import {TextInput, 
				View,
				TouchableOpacity,
				TouchableNativeFeedback,
				Image,
				Text,
				PermissionsAndroid,
				Button,
				ScrollView,
				Keyboard,
				AsyncStorage,
				KeyboardAvoidingView,
				ActivityIndicator} from 'react-native';

export default class Withdraw extends Component {
	constructor(props) {
		super(props);
		this.state = {
      UserId : '',
      Contact : '',
      credit : '',
      UPI : '',
      hint : true,
      hint1 : true,
      amount : '',
      cancelModal : false,
    }
    this.VPA = this.VPA.bind(this);
    this.Withdraw = this.Withdraw.bind(this);
	}

	componentWillMount() {
		AsyncStorage.multiGet(['userid', 'gcredits', 'contact', 'vpa']).then((data) => {
      this.setState({
        UserId : data[0][1],
        Contact : data[2][1],
        credit : Number(data[1][1]),
        UPI : data[3][1] ? data[3][1] : '',
      })
    })
  }
  
  VPA() {
    this.setState({
			hint : false,
		})
		Keyboard.dismiss();
		fetch(`${config.userurl}addVPA`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				phone: this.state.Contact,
				vpa: this.state.UPI,
				}),
			})
			.then(response =>  response.json())
			.then(async (responseJson) => {
				if(responseJson === null){
					showMessage({
						message: "An error occurred",
						type: "danger",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
						UPI : '',
					});
					this.setState({hint : true})
				}
				else{
					await AsyncStorage.multiSet([
						['gcredits', responseJson.GCredits+''],
            ['vpa', responseJson.VPA]
          ]);
          showMessage({
						message: "UPI ID successfully saved",
						type: "success",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
					});
					this.setState({
						hint : true,
					})
				}
      }).catch (err => {
      console.log("verify ",err);
      this.setState({
        hint : true,
      })
    })
  }

  Withdraw() {
    this.setState({
			hint1 : false,
		})
		Keyboard.dismiss();
		fetch(`${config.userurl}addVPA`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				amount: this.state.amount,
        vpa: this.state.UPI,
        UserId : this.state.UserId,
        Contact : this.state.Contact
				}),
			})
			.then(response =>  response.json())
			.then(async (responseJson) => {
				if(responseJson === null){
					showMessage({
						message: "An error occurred",
						type: "danger",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
						UPI : '',
					});
					this.setState({hint1 : true})
				}
				else{
					await AsyncStorage.multiSet([
						['gcredits', responseJson.GCredits+''],
            ['vpa', responseJson.VPA]
          ]);
          showMessage({
						message: "Amount successfully withdrawn",
						type: "success",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
					});
					this.setState({
            hint1 : true,
            credit : responseJson.GCredits,
					})
				}
      }).catch (err => {
      console.log("verify ",err);
      this.setState({hint1 : true})
    })
  }

	render() {
		const { goBack } = this.props.navigation;
		return(
			<View style={styles.container}>
				<View style={[styles.headerStyle, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
					<View style={{marginLeft: '3%'}}>
						<View>
							<TouchableNativeFeedback
								onPress={() => goBack()}>
								<View style={{padding: 10}}>
									<Icon name='md-arrow-back' color={'black'} size={28}></Icon>
								</View>
							</TouchableNativeFeedback>
						</View>
					</View>
					<View></View>
				</View>
        <View style={{width: '100%', bottom: 0, position: 'absolute'}}> 
          <Modal
            swipeDirection='down'
            onSwipe = {() => this.setState({cancelModal : false})}
            onBackButtonPress = {() => this.setState({cancelModal : false})}
            onBackdropPress = {() => this.setState({cancelModal : false})}
            isVisible={this.state.cancelModal}
            backdropOpacity={0.9}
            backdropColor='white'
            style={[{alignItems: 'center'}, styles.bottomModal]}>
            <View style={{width: '100%', alignItems: 'center', backgroundColor: 'white',  borderTopRightRadius: 20, borderTopLeftRadius: 20}}>
              <View style={{width: '100%', alignItems: 'center', backgroundColor: '#cccccc',  borderTopRightRadius: 20, borderTopLeftRadius: 20}}>
                <Text style={{fontSize: 16}}>=</Text>
              </View>
              <View style={{width: '90%', alignItems: 'center', marginTop: '5%'}}>
                <Text style={styles.content}>Note : Upon Withdrawal, <Text style={{color : 'red'}}>10%</Text> of the Total Amount would be deducted as service changes and will be sent to your UPI ID</Text>
              </View>
              <View style={{width: '90%', marginTop: '5%'}}>
                <Text style={[styles.content, {color : '#1C8ADB'}]}>Are you sure you want to Cancel?</Text>
              </View>
              <View style={{marginTop: '2%', width: '100%', marginBottom: '10%', alignItems: 'center'}}>
                <TouchableNativeFeedback
                  disabled={this.state.cancel}
                  onPress={() => {
                    this.setState({cancelModal : false}, () => this.Withdraw())
                    }}>
                  <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
                    <View style={{ alignItems: 'center'}}>
                      <Text style={styles.book}>  Withdraw  </Text>
                    </View>
                  </LinearGradient>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
        </View>
        <ScrollView>
          <View style={{alignItems: 'flex-start',marginTop: '5%', marginLeft: '5%'}}>
            <Text style={styles.signup}>Save UPI ID</Text>
            <Text style={{width: '88%',	paddingLeft: 10,}}>Withdrawals will be linked to the UPI ID entered below</Text>
          </View>
          <View style={{alignItems: 'center', marginTop: '5%'}}>
            <Text style={styles.textcontainer}>UPI ID</Text>
          </View>
          <View style={styles.inputcontainer}>
            <TextInput 
              style={styles.input}
              underlineColorAndroid='transparent'
              //placeholder= "Email"
              onChangeText = {(text) => this.setState({UPI : text})}
              value = {this.state.UPI}
            >
            </TextInput>
          </View>
        <View style={{marginTop: '7%'}}>
          {this.state.hint ? (
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                disabled = {this.state.UPI.match(/^\w+@\w+$/) ? false : true}
                style = {this.state.UPI.match(/^\w+@\w+$/) ? styles.buttonContinue : styles.buttonContinueDisabled}
                onPress={() => this.VPA()}
              >
                <Text style={this.state.UPI.match(/^\w+@\w+$/) ? styles.text : styles.textDisabled}>  Save  </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
          )}
        </View>
          <View style={{alignItems: 'center', marginTop: '5%'}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: '3%', width: '90%', paddingLeft: 10, paddingRight: 10}}>
              <Text style={[styles.book2, {color: '#1C8ADB'}]}>Available GCredits</Text>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Image
                    source={require('../images/GCoins.png')}
                    style={{width: 28, height: 23}}>
                  </Image>
                </View>
                <View>
                  <Text style={{color: '#1C8ADB', fontSize: 18, fontWeight: '500'}}>{this.state.credit == 0 || this.state.credit == undefined ? "0" : this.state.credit}</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{alignItems: 'flex-start',marginTop: '5%', marginLeft: '5%'}}>
            <Text style={{width: '88%',	paddingLeft: 10,}}>Enter the amount to withdraw</Text>
          </View>
          <View style={{alignItems: 'center', marginTop: '5%'}}>
            <Text style={styles.textcontainer}>Amount</Text>
          </View>
          <View style={styles.inputcontainer}>
            <TextInput 
              style={styles.input}
              underlineColorAndroid='transparent'
              //placeholder= "Email"
              keyboardType='numeric'
              onChangeText = {(text) => this.setState({amount : text})}
              value = {this.state.amount}
            >
            </TextInput>
          </View>
          <View style={{marginTop: '7%'}}>
            {this.state.hint1 ? (
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  disabled = {this.state.amount > 100 && this.state.amount <= this.state.credit ? false : true}
                  style = {this.state.amount > 100 && this.state.amount <= this.state.credit ? styles.buttonContinue : styles.buttonContinueDisabled}
                  onPress={() => this.setState({cancelModal : true})}
                >
                  <Text style={this.state.amount > 100 && this.state.amount <= this.state.credit ? styles.text : styles.textDisabled}>  Withdraw  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
            )}
          </View>
        </ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 11,
		backgroundColor: 'white',
	},
	forgot: {
		color: '#525452',
		fontWeight: 'bold',
	},
	headerStyle: {
		backgroundColor: 'white',
	},
	textBoxBtnHolder:
	{
		position: 'relative',
		alignSelf: 'stretch',
		justifyContent: 'center',
		width: '95%',
		paddingTop: 1,
		paddingLeft: 30,
		marginTop: 3,
		//paddingRight: 10,
		marginBottom: 3,
	},
	bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
  content: {
    color: '#5a5c5e', 
    fontWeight: '300', 
    flexWrap: 'wrap',
    fontSize: 14
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 2,
  },
	visibilityBtn:
	{
		position: 'absolute',
		right: 3,
		height: 40,
		width: 35,
		padding: 5,
	},
	btnImage:
	{
		resizeMode: 'contain',
		height: '100%',
		width: '100%'
	},
	textDisabled: {
		color: '#1C8ADB',
		fontWeight: 'bold',
		fontSize: 16,
	},
	text: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 16,
	},
  book2: {
    color: 'grey',
    fontWeight: '400',
    fontSize: 18,
  },
	button: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#0d3f93',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 45,
	},
	back: {
		padding: 10,
		borderRadius: 100,
		backgroundColor: 'white',
		//marginTop: 5,
		width: '14%',
		alignItems: 'center',
		height: 45,
		borderWidth: 2,
	},
	buttonContinue: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#1C8ADB',
		backgroundColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
	},
	buttonContinueDisabled: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
		opacity: 0.7,
	},
	buttonContainer: {
		padding: 10,
		alignItems:'center',
	},
	password: {
		marginTop: 5,
		marginBottom: 25,
		width: '80%',
		padding: 10,
		height: 44,
		borderColor: 'grey',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderLeftWidth: 1,
		borderStyle: 'solid',
	},
	passwordcontainer: {
		paddingTop: 1,
		paddingBottom: 1  ,
		paddingLeft: 10,
		paddingRight: 10,
		flexDirection: 'row',
		alignItems: 'center',
	},
	toolbar: {
		height: 50,
		borderWidth: 2,
		borderColor: 'grey',
		alignItems: 'stretch',
		elevation: 1,
	},
	signup: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#0e4b77',
		fontSize: 20,
		paddingBottom: 12,
	},
	resend: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#0e4b77',
		fontSize: 15,
	},
	textcontainer: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#878a91',
		fontSize: 15,
	},
	textcontainer2: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#878a91',
	},
	textcontainerusername: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#525452',
		fontStyle: 'italic',
	},
	emailerror: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: '400',
		color: 'grey',
		fontStyle: 'italic'
	},
	inputcontainer: {
		//paddingTop: 1,
		//paddingBottom: 1  ,
		paddingLeft: 10,
		paddingRight: 10,
		alignSelf: 'center',
		flexDirection: 'row',
	},
	input: {
			borderColor: '#aeb1b7',
			borderBottomWidth: 1,
			borderStyle: 'solid',
			width: '88%',
			height: 50,
			fontSize: 20,
		},
		inputusername: {
			marginTop: 5,
			marginBottom: 3,
			borderColor: 'grey',
			borderWidth: 1,
			borderStyle: 'solid',
			width: '88%',
			padding: 10,
			height: 44,
			fontSize: 18,
			},
	});