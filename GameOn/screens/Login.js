import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import { Header } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';
import config from "../extra/config";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {TextInput,
        View, 
        AsyncStorage, 
        TouchableOpacity, 
        Text, 
        Image, 
        Keyboard, 
        KeyboardAvoidingView, 
        ScrollView, 
        StatusBar, 
        TouchableNativeFeedback,
        ActivityIndicator,
        ImageBackground} from 'react-native';

import renderIf from '../extra/renderIf';

export default class LogIn extends Component { 
  constructor(props) {
    super(props);
      this.state = {
        phone : '',
        show : false,
        showpassword : true,
        indicate : true
        };
        //this.onChangeUsername = this.onChangeUsername.bind(this);
        this.Login = this.Login.bind(this);
    }

    componentDidMount() {
      SplashScreen.hide();
    }

    Login = () => {
      this.setState({
        indicate : false,
      })
      Keyboard.dismiss();
      fetch(`${config.userurl}login`, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
    			'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					phone: '+91'+this.state.phone,
				}),
      })
      .then(response =>  response.json())
      .then(responseJson => {
        this.setState({
          indicate : true,
        })
        let route = this.props.navigation.getParam('route');
        if(responseJson.message == 'Error'){
          this.props.navigation.navigate('Signup', {number: this.state.phone, route: route, counter: 3});
        }
        else{
          let otp = Number(responseJson.message);
          this.props.navigation.navigate('Verify', {number: this.state.phone, route: route, otp: otp, counter: 2});
        }
      }).catch (err => {
      console.log(err);
    })
	}

    render() {
      const {navigate} = this.props.navigation;
      let route = this.props.navigation.getParam('route');
      return(
        <View style={{flex: 1, flexDirection: 'column'}}>
          <View style={{flex: 1, backgroundColor: 'white'}} 
            resizeMode='cover' 
            source={require('../assets/loginBg.png')}>
            <View style={{alignItems: 'flex-start', marginTop: '32%', marginLeft:'4%'}}>
              <Image 
                style={{height: hp('11%'), width: wp('65%')}}
                source={require('../images/NewGO.png')}
              />
            </View>
            <View style={styles.container}>
            <View style={{width: '94%', borderBottomWidth: 1, borderColor: 'grey', alignSelf: 'center', borderRadius: 5}}></View>
              <View style={{marginLeft: '4%'}}>
                <Text style={{fontSize: 20, fontWeight: '500', marginTop: '3%'}}>LOGIN</Text>
                <Text style={{marginTop: '5%'}}>Enter your phone number to receive an OTP</Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontSize: 18, marginTop: '5.5%', color: 'black'}}>+91 </Text>
                  <TextInput
                    allowFontScaling={false}
                    underlineColorAndroid='transparent'
                    style={styles.input}
                    placeholder = '10 digit number'
                    placeholderTextColor={'#878a91'}
                    onFocus = {() => this.setState({color1 : '#1C8ADB'})}
                    onBlur = {() => this.setState({color1 : 'grey'})}
                    onChangeText = {(text) => this.setState({phone : text})}
                    returnKeyType = {"next"}
                    keyboardType = 'numeric'
                    value = {this.state.phone}
                  />
                </View>
                <KeyboardAvoidingView style={{marginTop: '5%'}}>
                  {this.state.indicate ? (
                    <TouchableOpacity
                      style = {this.state.phone.length == 10 ?  styles.button : styles.buttonDisabled}
                      onPress={this.Login}
                      disabled={this.state.phone.length == 10 ? false : true}
                    >
                      <Text style={this.state.phone.length == 10 ? styles.text : styles.textDisabled}>  LOGIN  </Text>
                    </TouchableOpacity>
                  ) : (
                    <ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
                  )}
                </KeyboardAvoidingView>
              </View>
              <View style={{width: '94%', alignItems: 'center', alignSelf: 'center', marginTop: '5%'}}>
                <TouchableNativeFeedback
                  onPress={() => route ? navigate('PassLogin', {route: route, counter: 2}) : navigate('PassLogin')}>
                  <Text style={{fontSize: 16}}>Or <Text style={{color : '#1C8ADB', fontWeight: '400'}}>login using password</Text></Text>
                </TouchableNativeFeedback>
              </View>
              <View style={{width: '94%', borderBottomWidth: 1, borderColor: 'grey', alignSelf: 'center', marginTop: '5%', borderRadius: 5, borderStyle: 'dotted'}}></View>
              <View style={{marginLeft: '5%'}}>
                <Text style={{marginTop: '5%'}}>Don't have an account?</Text>
                <View style={{marginTop: '5%'}}>
                  <TouchableOpacity
                    style = {styles.button}
                    onPress={() => route ? navigate('Signup', {number: this.state.phone, route: route, counter: 3}) : navigate('Signup', {number: this.state.phone})}
                  >
                    <Text style={styles.text}>  SIGN UP  </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
            );
        }
    }

const styles = StyleSheet.create({
  container: {
    height: '60%',
    width : '95%',
    // backgroundColor: 'white',
    // borderRadius: 5,
    alignSelf: 'center',
    marginTop: '6%',
    // borderBottomWidth: 2,
    // borderLeftWidth: 2,
    // borderTopWidth: 1,
    // borderRightWidth: 1,
    // borderColor: '#ddd',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
  },
textBoxBtnHolder:
{
	position: 'relative',
	alignSelf: 'center',
	justifyContent: 'center',
	width: '100%',
	paddingTop: '2%',
	paddingLeft: '5%',
	//paddingRight: 10,
	marginBottom: '1.5%',
},
textBox:
{
	fontSize: 18,
	//alignSelf: 'stretch',
  height: 50,
  width: '88%',
	paddingRight: 45,
	paddingLeft: 8,
	paddingVertical: 0,
  marginLeft: '8%'
},
inputcontainer: {
  paddingLeft: 10,
  paddingRight: 10,
  paddingBottom: 10,
  paddingTop: 7,
  alignItems: 'center',
  flexDirection: 'row',
  width: '95%',
  justifyContent: 'space-around',
  //borderBottomWidth: 1,
  //borderColor: '#aeb1b7',
},
input: {
  marginTop: 5,
  marginBottom: 3,
  width: '60%',
  padding: 10,
  height: 50,
  fontSize: 18,
  borderRadius: 5,
  borderBottomWidth: 2,
  borderColor: '#4da3e3',
},
visibilityBtn:
{
	position: 'absolute',
	right: 3,
	height: 40,
	width: 35,
	padding: 5,
},
btnImage:
{
  marginTop: 3,
	resizeMode: 'contain',
	height: '100%',
	width: '100%'
},
  facebook: {
    backgroundColor: '#31439b',
    borderWidth: 1,
    borderColor: '#283187',
  },
  google: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    paddingLeft: 7,
    paddingRight: 10,
  },
  fgcontainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '92%'
  },
  warning: {
    fontWeight: 'bold',
    color: '#910700',
  },
  warningcontainer: {
    backgroundColor: '#ffbdba',
    borderWidth: 1,
    borderColor: '#910700',
    alignItems: 'center',
    padding: 10,
    width: '84%',
  },
  imagecontainer: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainer: {
    padding: 10,
    alignItems:'center',
    marginTop: '5%',
  },
  textContainer: {
    width: '92%',
    padding: 5,
    alignItems: 'flex-end',
    marginTop: '5%',
  },
  or: {
    fontWeight: 'bold',
    color: '#525452',
    fontSize: 16,
  },
  button: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    backgroundColor: '#1C8ADB',
    //marginTop: 5,
    width: '88%',
    alignItems: 'center',
    //opacity: 0.7,
    height: 50,
  },
  buttonDisabled: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    //marginTop: 5,
    width: '88%',
    height: 50,
    alignItems: 'center',
    opacity: 0.7,
  },
  textDisabled: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 16,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  forgot: {
    color: '#878a91',
    fontWeight: 'bold',
  }
  });
