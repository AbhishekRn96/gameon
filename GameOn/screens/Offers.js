import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback,
  TouchableHighlight, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  FlatList,
  BackHandler} from 'react-native';

export default class Offers extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
  }

  render() {
    const { goBack } = this.props.navigation;
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <TouchableNativeFeedback
                 onPress={() => goBack()}>
                <View style={{padding: 10, paddingTop: 15}}>
                  <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                </View>
              </TouchableNativeFeedback>
            </View>
            <View style={{padding: 10, marginTop: '3%'}}>
              <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Offers</Text>
            </View>
          </View>
          <View></View>
        </View>
        <View style={{alignItems: 'center', marginTop: '50%'}}>
          <Text style={{fontSize: 20, color: 'grey', fontWeight: '500'}}>Coming Soon...</Text>
        </View>
      </View>
    )
  }
}  

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
	},
	text: {
		padding: 10,
		fontSize: 15,
	},
})
