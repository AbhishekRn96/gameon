import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import config from "../extra/config";
import {TextInput, 
		AsyncStorage, 
		View, 
		TouchableOpacity, 
		ActivityIndicator, 
		TouchableNativeFeedback, 
		KeyboardAvoidingView, 
		Text, 
		Image, 
		ScrollView,  
		Keyboard,
		Alert} from 'react-native';

export default class SignUp extends Component {
    constructor(props) {
        super(props);
          this.state = {
						email : '',
						number : '',
						name : '',
						password : '',
						emailerror: '',
						showpassword : true,
						hint : true,
						show : false,
						};
						//this.Next = this.Next.bind(this);
						this.SignUp = this.SignUp.bind(this);
						//this.validateEmail = this.validateEmail.bind(this);
				}
			
			componentDidMount() {
				let number = this.props.navigation.getParam('number');
				if(number.length === 10) {
					this.setState({
						number : number
					})
				}
			}
				
			componentWillUnmount() {
				this.setState({showpassword : true})
				Keyboard.dismiss();
			}
	
			changepasswordVis() {
				this.setState({showpassword : !this.state.showpassword})
			}

		SignUp () {
			this.setState({
				hint : false,
				emailerror: ''
			})
			Keyboard.dismiss();
				fetch(`${config.userurl}register`, {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						email: this.state.email.toLowerCase(),
						phone: '+91'+this.state.number,
						name: this.state.name,
						pass: this.state.password,
						dp : 'https://thegameonn.herokuapp.com/dp/avatar.png'
					}),
				})
				.then(response =>  response.json())
				.then(responseJson => {
					this.setState({
						hint : true,
					})
					let route = this.props.navigation.getParam('route');
					if(responseJson.message.length == 6){
						let otp = Number(responseJson.message);
						this.props.navigation.navigate('Verify', {number: this.state.number, route: route, otp: otp, counter : 3});
					}
					else {
						this.setState({
							hint : true,
							show : true
						})
						Alert.alert(
							'',
							`${responseJson.message}`,
							[
								{text: 'OK', onPress: () => console.log('OK Pressed')},
							],
							{cancelable: true},
						)
					}
				}).catch (err => {
				console.log(err);
			})
		}

    render() {
			const {navigate} = this.props.navigation;
			const { goBack } = this.props.navigation;
		return(
			<View style={{flex: 1, flexDirection: 'column'}}>
				<ScrollView>
					<View style={[styles.headerStyle, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
						<View style={{marginLeft: '3%'}}>
							<View>
								<TouchableNativeFeedback
									onPress={() => goBack()}>
									<View style={{padding: 10}}>
										<Icon name='md-arrow-back' color={'black'} size={28}></Icon>
									</View>
								</TouchableNativeFeedback>
							</View>
						</View>
						<View></View>
					</View>
					<View style={styles.container}>
						<View style={{alignItems: 'center'}}>
							<Text style={styles.signup}>SIGN UP</Text>
							<Text style={{width: '88%',	paddingLeft: 10,}}>Let's create an account for you with your phone number</Text>
						</View>
						<View style={{marginTop: '5%'}}>
							<View style={{alignItems: 'center'}}>
								<Text style={styles.textcontainer}>PHONE NUMBER</Text>
							</View>
							<KeyboardAvoidingView behavior='padding'>
								<View style={styles.inputcontainer}>
									<TextInput 
										style={styles.input}
										underlineColorAndroid='transparent'
										//placeholder= "Email"
										keyboardType='numeric'
										onChangeText = {(text) => this.setState({number : text})}
										value = {this.state.number}
									>
									</TextInput>
									{this.state.number.match(/\d{10}/) ? (
										<Icon name='md-checkmark' color={'green'} size={18} style={{paddingTop: '5%'}}></Icon>
									) : (
										<Icon name='md-checkmark' color={'white'} size={18} style={{paddingTop: '5%'}}></Icon>
									)}
								</View>
							</KeyboardAvoidingView>
						</View>
						<View style={{marginTop: '5%'}}>
							<View style={{alignItems: 'center'}}>
								<Text style={styles.textcontainer}>EMAIL</Text>
							</View>
							<KeyboardAvoidingView behavior='padding'>
								<View style={styles.inputcontainer}>
									<TextInput 
										style={styles.input}
										underlineColorAndroid='transparent'
										//placeholder= "Email"
										onChangeText = {(text) => this.setState({email : text})}
										value = {this.state.email}
										keyboardType='email-address'
									>
									</TextInput>
									{this.state.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) ? (
										<Icon name='md-checkmark' color={'green'} size={18} style={{paddingTop: '5%'}}></Icon>
									) : (
										<Icon name='md-checkmark' color={'white'} size={18} style={{paddingTop: '5%'}}></Icon>
									)}
								</View>
							</KeyboardAvoidingView>
						</View>
						<View style={{marginTop: '5%'}}>
							<View style={{alignItems: 'center'}}>
								<Text style={styles.textcontainer}>NAME</Text>
							</View>
							<KeyboardAvoidingView behavior='padding'>
								<View style={styles.inputcontainer}>
									<TextInput 
										style={styles.input}
										underlineColorAndroid='transparent'
										//placeholder= "Email"
										onChangeText = {(text) => this.setState({name : text})}
										value = {this.state.name}
									>
									</TextInput>
									<Icon name='md-checkmark' color={'white'} size={18} style={{paddingTop: '5%'}}></Icon>
								</View>
							</KeyboardAvoidingView>
						</View>
						<View style={{marginTop: '5%'}}>
							<View style={{alignItems: 'center'}}>
								<Text style={styles.textcontainer}>PASSWORD</Text>
							</View>
							<KeyboardAvoidingView behavior='padding'>
								<View style={{flexDirection: 'row'}}>
									<View style = { styles.textBoxBtnHolder }>
										<TextInput underlineColorAndroid = "transparent" placeholder='More then 6 characters' onChangeText = {(text) => this.setState({password : text})} secureTextEntry = { this.state.showpassword } style = { styles.input }/>
										<TouchableOpacity activeOpacity = { 1 } style = { styles.visibilityBtn } onPress = { this.changepasswordVis.bind(this) }>
											<Image source = { ( this.state.showpassword ) ? require('../images/hide.png') : require('../images/view.png') } style = { styles.btnImage } />
										</TouchableOpacity>
									</View>
									<Icon name='md-checkmark' color={'white'} size={18} style={{paddingTop: '5%'}}></Icon>
								</View>
							</KeyboardAvoidingView>
						</View>
						<View style={{alignItems: 'center', marginTop: '5%', marginBottom: '5%'}}>
							<Text style={{ justifyContent: 'center', width: '88%',color: '#525452',paddingLeft: 10, flexWrap: 'wrap',fontSize:11}}>
								By signing up, you agree to GameOn's <Text style={{fontWeight :'bold', color : '#1C8ADB'}}>Terms and Conditions</Text>.
							</Text>
						</View>
						{this.state.hint ? (
							<View style={styles.buttonContainer}>
								<TouchableOpacity
									disabled = {this.state.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) && this.state.password.length >= 6 && this.state.name !== '' && this.state.number.match(/\d{10}/) ? false : true}
									style = {this.state.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) && this.state.password.length >= 6 && this.state.name !== '' && this.state.number.match(/\d{10}/) ? styles.buttonContinue : styles.buttonContinueDisabled}
									onPress={this.SignUp}
								>
									<Text style={this.state.email.match(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/) && this.state.password.length >= 6 && this.state.name !== '' && this.state.number.match(/\d{10}/) ? styles.text : styles.textDisabled}>  SIGN UP  </Text>
								</TouchableOpacity>
							</View>
							) : (
								<ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
							)}
					</View>
				</ScrollView>	
			</View>
        );
    }
}

const styles = StyleSheet.create({
container: {
	flex: 11,
	backgroundColor: 'white',
},
forgot: {
	color: '#525452',
	fontWeight: 'bold',
},
headerStyle: {
	backgroundColor: 'white',
},
textBoxBtnHolder:
{
	position: 'relative',
	alignSelf: 'stretch',
	justifyContent: 'center',
	width: '95%',
	paddingTop: 1,
	paddingLeft: 30,
	marginTop: 3,
	//paddingRight: 10,
	marginBottom: 3,
},
visibilityBtn:
{
	position: 'absolute',
	right: 3,
	height: 40,
	width: 35,
	padding: 5,
},
btnImage:
{
	resizeMode: 'contain',
	height: '100%',
	width: '100%'
},
textDisabled: {
	color: '#1C8ADB',
	fontWeight: 'bold',
	fontSize: 16,
},
text: {
	color: 'white',
	fontWeight: 'bold',
	fontSize: 16,
},
button: {
	padding: 10,
	borderWidth: 2,
  borderColor: '#0d3f93',
	//marginTop: 5,
	width: '88%',
	alignItems: 'center',
	height: 45,
},
back: {
	padding: 10,
	borderRadius: 100,
	backgroundColor: 'white',
	//marginTop: 5,
	width: '14%',
	alignItems: 'center',
	height: 45,
	borderWidth: 2,
},
buttonContinue: {
	padding: 10,
	borderWidth: 2,
	borderColor: '#1C8ADB',
	backgroundColor: '#1C8ADB',
	//marginTop: 5,
	width: '88%',
	alignItems: 'center',
	height: 50,
},
buttonContinueDisabled: {
	padding: 10,
	borderWidth: 2,
  borderColor: '#1C8ADB',
	//marginTop: 5,
	width: '88%',
	alignItems: 'center',
	height: 50,
	opacity: 0.7,
},
buttonContainer: {
	padding: 10,
	alignItems:'center',
},
password: {
	marginTop: 5,
	marginBottom: 25,
	width: '80%',
	padding: 10,
	height: 44,
	borderColor: 'grey',
	borderTopWidth: 1,
	borderBottomWidth: 1,
	borderLeftWidth: 1,
	borderStyle: 'solid',
},
passwordcontainer: {
	paddingTop: 1,
	paddingBottom: 1  ,
	paddingLeft: 10,
	paddingRight: 10,
	flexDirection: 'row',
	alignItems: 'center',
},
toolbar: {
	height: 50,
	borderWidth: 2,
	borderColor: 'grey',
	alignItems: 'stretch',
	elevation: 1,
},
signup: {
	width: '88%',
	paddingLeft: 10,
	fontWeight: 'bold',
	color: '#0e4b77',
	fontSize: 20,
	paddingBottom: 12,
},
textcontainer: {
	width: '88%',
	paddingLeft: 10,
	fontWeight: 'bold',
	color: '#878a91',
	fontSize: 12,
},
textcontainer2: {
	width: '88%',
	paddingLeft: 10,
	fontWeight: 'bold',
	color: '#878a91',
},
textcontainerusername: {
	width: '88%',
	fontSize: 12,
	paddingLeft: 10,
	fontWeight: 'bold',
	color: '#525452',
	fontStyle: 'italic',
},
emailerror: {
	width: '88%',
	fontSize: 12,
	paddingLeft: 10,
	fontWeight: '400',
	color: 'grey',
	fontStyle: 'italic'
},
inputcontainer: {
	//paddingTop: 1,
	//paddingBottom: 1  ,
	paddingLeft: 10,
	paddingRight: 10,
	alignSelf: 'center',
	flexDirection: 'row',
},
input: {
		marginTop: 5,
		marginBottom: 3,
		borderColor: '#aeb1b7',
		borderBottomWidth: 1,
		borderStyle: 'solid',
		width: '85%',
		padding: 10,
		height: 40,
		fontSize: 18,
	},
	inputusername: {
		marginTop: 5,
		marginBottom: 3,
		borderColor: 'grey',
		borderWidth: 1,
		borderStyle: 'solid',
		width: '88%',
		padding: 10,
		height: 44,
		fontSize: 18,
		},
});