import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {configure, localNotif, scheduleNotifDay} from './Notifications';
import {TextInput,
        AsyncStorage,
        View,
        TouchableOpacity, 
        ActivityIndicator, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button, 
        ScrollView, 
        Keyboard,
        FlatList,
        BackHandler} from 'react-native';

export default class BookConfirm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        result : null
      }
			this.ok = this.ok.bind(this);
			this.handleBackButton = this.handleBackButton.bind(this);
		}
		
    componentDidMount() {
			BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      localNotif();
      let item = this.props.navigation.getParam('item');
      let usedata = this.props.navigation.getParam('userDate');
      AsyncStorage.getItem('userid').then((result) => {
        scheduleNotifDay(item.Date, result, item);
        this.setState({result : result});
      })
		}

		componentWillUnmount() {
			BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
		}

		handleBackButton() {
			return true;
		}
		
		ok() {
      let login = this.props.navigation.getParam('login');
      if(login) {
        this.props.navigation.navigate('App');
      } else {
        this.props.navigation.navigate('Dashboard');
      }
      return true;
		}

    render() {
			const {navigation} = this.props;
			const item = navigation.getParam('item');
			return (
				<View style={{backgroundColor: 'white', flex: 1}}>
          <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback
								  onPress={this.ok}>
                  <View style={{padding: 10, paddingTop: 15}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10, flexDirection: 'row', justifyContent: 'space-between', marginTop: '2%'}}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Booking Successful</Text>
                <Icon name='md-checkmark-circle-outline' color={'green'} size={28} style={{marginLeft: '10%'}}></Icon>
              </View>
            </View>
            <View></View>
          </View>
					<View style={{width: '90%', alignItems: 'center', alignSelf: 'center'}}>
            <View style={styles.shadow}>
              <Image
                source={{uri: item.Poster}}
                style={styles.image}>
              </Image>
            </View>
					</View>
					<View style={{marginLeft: '3%', marginTop: '10%'}}>
            <Text style={styles.textInImage}>&nbsp;&nbsp;&nbsp;&nbsp;You have successfully registered for <Text style={styles.font}>{item.Name}</Text> Tournament on <Text style={styles.font}>{item.DateString}</Text>, <Text style={styles.font}>{item.Hours}:{item.Minutes}</Text></Text>
					</View>
          <View style={{marginLeft: '3%', marginTop: '3%'}}>
            <Text style={styles.textInImage}>&nbsp;&nbsp;&nbsp;&nbsp;Your Game Room Id will be shared with you, <Text style={styles.font}>{item.End} Minutes</Text> before the Tournament</Text>
					</View>
          <View style={{marginLeft: '3%', marginTop: '3%'}}>
            <Text style={styles.textInImage}>&nbsp;&nbsp;&nbsp;&nbsp;Check your Booking details before <Text style={styles.font}>{item.End} Minutes</Text> of he Tournament. You will also recieve a text message on your Registered Phone Number, <Text style={styles.font}>{item.End} Minutes</Text> prior to the Tournament with your Game Room Details</Text>
					</View>
					<View style={{alignItems : 'center', bottom: 0, position: 'absolute', width: '100%'}}>
						<TouchableNativeFeedback
							onPress={this.ok}>
							<LinearGradient colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
								<View style={{ alignItems: 'center'}}>
									<Text style={styles.ok}>  Done  </Text>
								</View>
							</LinearGradient>
						</TouchableNativeFeedback>
					</View> 
				</View>
			)
	}
}

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: 'white',
  },
  image: {
    height: hp('27%'), 
    borderRadius: 3 
  },
  textInImage: {
    fontSize: 17, 
    fontWeight: '400', 
    color: 'grey'
  },
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
  edit: {
		backgroundColor: '#1C8ADB',
		padding: 10,
		width: '{item.End}%',
		height: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 2,
  },
  label: {
    fontSize: 20,
    color: '#1C8ADB',
    fontWeight: '500'
  },
  font: {
    color : '#1C8ADB',
  },
  booking: {
		backgroundColor: '#1C8ADB',
		height: 60,
    alignItems: 'center',
  },
  scaleButtons: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: '#858687'
  },
  scaleButtonsSelected: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: 'white'
  },
  editDate: { 
    backgroundColor: 'white',
		padding: 10,
		width: '60%',
		height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderWidth: 2,
    borderColor: '#1C8ADB',
  },
  picker: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#1C8ADB',
		padding: 10,
		width: '70%',
		height: 50,
    alignItems: 'center',
  },
  pickerContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  pcContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 25,
    padding: 10,
  },
  ok: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%'
  },
  number: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 25,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
  },
  book1: {
    color: '#1C8ADB',
    fontWeight: '400',
    fontSize: 18,
  },
  book2: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 22,
  },
  bookDate: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 10
  },
  bookDay: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 23,
  },
})