import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {TextInput,
				AsyncStorage,
				View,
				TouchableOpacity, 
				ActivityIndicator, 
				TouchableNativeFeedback, 
				KeyboardAvoidingView, 
				Text, 
				Image,
				Button, 
				ScrollView, 
				Keyboard,
				FlatList,
				BackHandler,
				Animated,
				Easing} from 'react-native';
				
export default class Settings extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show : false
		}
		this.logout = this.logout.bind(this);
	}

	componentDidMount() {
	}

	async logout() {
		this.setState({show : false})
		await AsyncStorage.clear()
		this.props.navigation.navigate('Auth');
	}

	render() {
		const {navigate} = this.props.navigation;
		const { goBack } = this.props.navigation;
		const {navigation} = this.props;
		return (
			<View style={{backgroundColor: 'white', flex: 1}}>
				<View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
					<View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
						<View>
							<TouchableNativeFeedback>
								<View style={{padding: 10, paddingTop: 15}}>
									<Icon name='md-arrow-back' color={'black'} size={28} onPress={() => goBack()}></Icon>
								</View>
							</TouchableNativeFeedback>
						</View>
						<View style={{padding: 10}}>
							<Text style={{fontSize: 20, fontWeight: '400', color: 'black'}}>Settings</Text>
						</View>
					</View>
					<View></View>
				</View>
					<View style={{alignItems: 'center'}}> 
						<Modal
							animationIn = 'fadeIn'
							animationOut = 'fadeOut'
							isVisible={this.state.show}
							onBackdropPress={() => this.setState({show : false})}
					    onBackButtonPress={() => this.setState({show : false})}
							style={{alignItems: 'center'}}>
							<View style={{width: '80%', alignItems: 'center', backgroundColor: 'white',  borderTopRightRadius: 10, borderTopLeftRadius: 10}}>
								<Text allowFontScaling={false} style={{padding: 10, fontSize: 16}}>Are you sure you want to logout ?</Text>
							</View>
							<View style={{flexDirection: 'row', width: '80%'}}>
								<TouchableNativeFeedback
									onPress={this.logout}>
									<View style={{width: '50%', alignItems: 'center', backgroundColor: 'white',  borderBottomLeftRadius: 10, borderRightWidth: 1, borderColor: '#939496', borderTopWidth: 1}}>
										<Text allowFontScaling={false} style={{padding: 10, fontSize: 16}}>Yes</Text>
									</View>
								</TouchableNativeFeedback>
								<TouchableNativeFeedback
									onPress={() => this.setState({show : false})}>
									<View style={{width: '50%', alignItems: 'center', backgroundColor: 'white', borderBottomRightRadius: 10, borderTopWidth: 1, borderColor: '#939496',}}>
										<Text allowFontScaling={false} style={{padding: 10, fontSize: 16}}>No</Text>
									</View>
								</TouchableNativeFeedback>
							</View>
						</Modal>
					</View>
					<View style={{marginLeft: '5%'}}>
						<TouchableNativeFeedback
							onPress={() => this.setState({show : true})}>
							<View style={styles.settingItemsLast}>
								<Text style={styles.text}>Logout</Text>
							</View>
						</TouchableNativeFeedback>
					</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
	},
	settingItemsLast: { 
    width: '100%', 
    marginTop: 5, 
    marginBottom: 5
  },
	text: {
    fontSize: 18,
    color: '#4d4f54',
    padding: 10
  },
	image: {
    width: wp('35%'),
    height: hp('20%'),
    borderRadius: 100,
		backgroundColor: 'white',
  }
})