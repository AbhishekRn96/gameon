import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import config from "../extra/config";
import {TextInput,
				AsyncStorage, 
				View, 
				TouchableOpacity, 
				ActivityIndicator, 
				TouchableNativeFeedback, 
				KeyboardAvoidingView, 
				Text, 
				Image, 
				Button, 
				ScrollView, 
				Keyboard,
				BackHandler} from 'react-native';

export default class Details extends Component {
    constructor(props) {
				super(props);
				this.state = {
					user : '',
					loading : true,
				}
    }

		componentDidMount() {
			let state = this.props.navigation.getParam('user');
			fetch(`${config.userurl}getUser`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
					UserId : state,
				}),
			})
			.then(response =>  response.json())
			.then(async (responseJson) => {
				console.log(responseJson)
			}).catch (err => {
				console.log("verify ",err);
			});
			this.setState({
				user : state,
			})
		}

    render() {
			const {navigation} = this.props;
			const { goBack } = this.props.navigation;
        return (
				<View style={styles.container}>
					{this.state.loading ? null : (
          <View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback
									onPress={() => goBack()}>
                  <View style={{padding: 10, paddingTop: 15}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10}}>
                <Text style={{fontSize: 20, fontWeight: '400', color: 'black'}}></Text>
              </View>
            </View>
						<View></View>
          </View>
					)}
					{this.state.loading ? (
						<View style={{flex: 1, backgroundColor: 'white'}}>
							<View style={{alignItems: 'center', marginTop: '50%', justifyContent: 'center',}}>
								<ActivityIndicator size={50} color="#1C8ADB" style={{marginTop: '20%'}} />
							</View>
						</View>
					) : (
						<ScrollView>
							<View style={styles.picContainer}>
								<View style={{alignItems: 'center', marginTop: 13}}>
									<Image
										source={{uri: data.dp}}
										style={styles.dp}>
									</Image>
								</View>
								<View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>
										{<Text style={styles.Name}>{data.name}</Text>}
								</View>
							</View>
							<View style={{width: '90%', alignSelf: 'center'}}>

							</View>
						</ScrollView>
					)}
				</View>
        )
    }
}

const styles = StyleSheet.create({
	container : {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'white',
	},
	picContainer: {
		flex: 3,
		backgroundColor: '#1C8ADB',
	},
	dp: {
		borderRadius: 100,
		borderWidth: 3,
		borderColor: '#ffffff',
		height: 116,
		width: 116,
	},
	Name: {
		fontSize: 23,
		fontWeight: 'bold',
		color: 'white',
	},
	headerStyle: {
		backgroundColor: '#1C8ADB',
	},
	edit: {
		backgroundColor: '#1C8ADB',
		borderRadius: 100,
		paddingTop: 20,
		width: '18%',
		alignItems: 'center',
		height: '11.8%',
		marginBottom: 15,
		marginLeft: '75%',
	}
})