import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import config from "../extra/config";
import RazorpayCheckout from 'react-native-razorpay';
import moment from 'moment';
import {TextInput,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        InteractionManager, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button,
        Dimensions,
        ScrollView, 
        Keyboard,
        StatusBar,
        BackHandler} from 'react-native';

export default class TBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
          didFinishInitialAnimation: false,
          error : false,
          loading : false,
          GameId : '',
          gameiderr : false,
          totalCost : 0,
          credit : 0,
          Username : '',
          DP : '',
          Email : '',
          UserId : '',
          Contact : '',
          usedCredit : 0,
          disable : false,
          complete : false,
          berror : false,
          bcancel : false,
        }
        this.navigateBack = this.navigateBack.bind(this);
        this.RazorPay = this.RazorPay.bind(this);
        this.join = this.join.bind(this);
        this.initiate = this.initiate.bind(this);
    }

    componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
        this.setState({
          didFinishInitialAnimation: true,
        });
      });
      let item = this.props.navigation.getParam('item');
      let fee = Number(item.Fee);
      AsyncStorage.multiGet(['userid', 'dp', 'email', 'user', 'gcredits', 'contact']).then((data) => {
        this.setState({
          Username : data[0][1],
          DP : data[1][1],
          Email : data[2][1],
          UserId: data[3][1],
          Contact: data[5][1],
          credit : Number(data[4][1]),
        })
        if(Number(data[4][1]) == 0) {
          this.setState({
            totalCost : fee,
            usedCredit : 0,
          })
        } else if(Number(data[4][1]) > fee) {
          this.setState({
            totalCost : 0,
            usedCredit : fee,
          })
        } else if(Number(data[4][1]) == fee) {
          this.setState({
            totalCost : 0,
            usedCredit : fee,
          })
        } else if(Number(data[4][1]) < fee) {
          this.setState({
            totalCost : fee - Number(data[4][1]),
            usedCredit : Number(data[4][1]),
          })
        }
      })
    }

    checkAvailability() {
      this.setState({loading : true});
      let item = this.props.navigation.getParam('item');
      fetch(`${config.bookingurl}TBookingAvailability`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          TournamentId : item.TournamentId,
        }),
      })
      .then(response =>  response.json())
      .then((resData) => {
        if(resData.message === "noSlots") {
          this.setState({error : true, loading: false});
        } else if(resData.message === "available"){
          this.setState({loading : false});
          this.RazorPay();
        }
      })
      .catch(err => {
        console.log("fail ",err);
        this.setState({loading : false});
      });
    }

    navigateBack() {
      this.setState({loading: false, error: false});
    }

    RazorPay() {
      this.setState({disable : true})
      let item = this.props.navigation.getParam('item');
      var options = {
        description: item.Name,
        image: 'https://s3.ap-south-1.amazonaws.com/gameon-cafe-images/icon.png',
        currency: 'INR',
        key: 'rzp_test_t5wTJXwdktpc56',
        amount: Number(this.state.totalCost)*100,
        name: 'GameOn',
        prefill: {
          email: this.state.Email,
        },
        theme: {color: '#1C8ADB'}
      }
      RazorpayCheckout.open(options).then((data) => {
        // handle success
        data.status = "paid";
        this.join(data);
      }).catch((error) => {
        console.log(error);
        this.setState({disable : false});
        if(error.description === 'Payment Cancelled') {
          this.setState({bcancel : true});
        } else {
          this.setState({berror : true});
        }
        // handle failure
        //alert(`Error: ${error.code} | ${error.description}`);
      });
    }

    async join(payment) {
      this.setState({complete : true});
      let item = this.props.navigation.getParam('item');
      let login = this.props.navigation.getParam('login');
      try {
        let response = await fetch(`${config.bookingurl}bookingTournament`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Date : item.DateString,
              DateTime : item.Date,
              Name : item.Name,
              DateOfBooking : moment(new Date()).format('MMMM Do YYYY'),
              User : {UserId : this.state.UserId, Username : this.state.Username, Email : this.state.Email, Contact: this.state.Contact, DP : this.state.DP},
              GameId : this.state.GameId,
              TotalCost : Number(item.Fee),
              Payed : Number(this.state.totalCost),
              TournamentId : item.TournamentId,
              Slots : item.Slots,
              Poster : item.Poster,
              Hours : item.Hours,
              Minutes : item.Minutes,
              End : item.End,
              StartTime : Number(item.Hours+'.'+item.Minutes),
              GCreditsUsed : this.state.usedCredit,
              PaymentInfo : payment
            }),
          })
          let responseJson = await response.json();
          console.log(responseJson);
          if(responseJson.message === "success"){
            let credit = this.state.credit - this.state.usedCredit;
            await AsyncStorage.setItem('gcredits', credit+'');
            this.setState({complete : false});
            this.props.navigation.navigate('BookConfirm', {item : item, userDate: responseJson.id, login : login});
          } else {
            this.setState({complete : false});
          }
      } catch (err) { 
        console.log(err.message);
        this.setState({complete : false});
      }
    }

    initiate() {
      if(this.state.GameId === '') {
        this.setState({gameiderr : true});
      } else {
        if(this.state.totalCost == 0) {
          this.join({payment : 'Payed by GCredits'});
        } else {
          this.checkAvailability();
        }
      }
    }

    render() {
      const {navigate} = this.props.navigation;
      const { goBack } = this.props.navigation;
      const {navigation} = this.props;
      const item = navigation.getParam('item');
      return(
        this.state.didFinishInitialAnimation == false ? (
          <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={styles.container}>
              <ActivityIndicator />
              <StatusBar
                backgroundColor="white"
                barStyle="dark-content"
              />
            </View>
          </View>
        ) : (
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', top: 0}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback
								  onPress={() => goBack()}>
                  <View style={{padding: 10, paddingTop: 10, paddingRight: 10}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10}}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Booking</Text>
              </View>
            </View>
            <View style={{marginRight: '4%'}}>
            </View>
          </View>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.error}
            backdropOpacity={0.9}
            backdropColor='white'
            onBackdropPress={() => this.setState({error : false})}
            onBackButtonPress={() => this.setState({error : false})}
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{alignItems: 'center'}}>
                <View style={{padding: 10}}>
                  <Text style={{fontSize: 16, color: 'black'}}>Sorry, there are no Slots available for this Tournament</Text>
                </View>
                <TouchableNativeFeedback
                  onPress={() => this.navigateBack()}>
                  <View style={{marginTop: '2%', padding: 10}}>
                    <Text style={{fontSize: 16, color: '#1C8ADB', fontWeight: '500'}}>OK</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.loading}
            backdropOpacity={0.9}
            backdropColor='white'
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{flexDirection: 'row', padding: 10}}>
                <ActivityIndicator size={22} color="#1C8ADB" />
                <Text style={{marginLeft: '3%', fontSize: 16, color: 'black'}}>Checking for Slot Availability</Text>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.complete}
            backdropOpacity={0.9}
            backdropColor='white'
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{flexDirection: 'row', padding: 10}}>
                <ActivityIndicator size={22} color="#1C8ADB" />
                <Text style={{marginLeft: '3%', fontSize: 16, color: 'black'}}>Completing Transaction</Text>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.berror}
            backdropOpacity={0.9}
            backdropColor='white'
            onBackdropPress={() => this.setState({berror : false})}
            onBackButtonPress={() => this.setState({berror : false})}
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{alignItems: 'center'}}>
                <View style={{flexDirection: 'row', padding: 10}}>
                  <Icon2 name='error-outline' color={'red'} size={22}></Icon2>
                  <Text style={{marginLeft: '3%', fontSize: 16, color: 'black'}}>Booking Failed</Text>
                </View>
                <TouchableNativeFeedback
                  onPress={() => this.setState({berror : false})}>
                  <View style={{marginTop: '2%', padding: 10}}>
                    <Text style={{fontSize: 16, color: '#1C8ADB', fontWeight: '500'}}>OK</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.bcancel}
            backdropOpacity={0.9}
            backdropColor='white'
            onBackdropPress={() => this.setState({bcancel : false})}
            onBackButtonPress={() => this.setState({bcancel : false})}
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{alignItems: 'center'}}>
                <View style={{flexDirection: 'row', padding: 10}}>
                  <Icon2 name='error-outline' color={'red'} size={22}></Icon2>
                  <Text style={{marginLeft: '3%', fontSize: 16, color: 'black'}}>Booking Cancelled</Text>
                </View>
                <TouchableNativeFeedback
                  onPress={() => this.setState({bcancel : false})}>
                  <View style={{marginTop: '2%', padding: 10}}>
                    <Text style={{fontSize: 16, color: '#1C8ADB', fontWeight: '500'}}>OK</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
          <View style={{marginTop: '5%'}}>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.textcontainer}>Enter your Gaming Id</Text>
            </View>
            <View style={styles.inputcontainer}>
              <TextInput
                style={styles.input}
                underlineColorAndroid='transparent'
                value = {this.state.value}
                onChangeText = {(text) => this.setState({GameId : text, gameiderr : false})}
              >
              </TextInput>
            </View>
            <View style={{alignItems: 'center', marginTop: '2%'}}>
              {this.state.gameiderr ? (
                <Text style={styles.errcontainer}>Please enter your Gaming Id</Text>
              ) : (
                <Text style={styles.textcontainer}> </Text>
              )}
            </View>
            <View style={{alignItems: 'center'}}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: '3%', width: '90%', paddingLeft: 10, paddingRight: 10}}>
                <Text style={[styles.book2, {color: '#1C8ADB'}]}>Available GCredits</Text>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image
                      source={require('../images/GCoins.png')}
                      style={{width: 28, height: 23}}>
                    </Image>
                  </View>
                  <View>
                    <Text style={{color: '#1C8ADB', fontSize: 18, fontWeight: '500'}}>{this.state.credit == 0 || this.state.credit == undefined ? "0" : this.state.credit}</Text>
                  </View>
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: '3%', width: '90%', paddingLeft: 10, paddingRight: 10}}>
                <Text style={styles.book2}>Entry Fee</Text>
                <Text style={[styles.book2, {color: '#1C8ADB', fontWeight: '500'}]}>&#8377;{item.Fee}</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: '3%', width: '90%', paddingLeft: 10, paddingRight: 10}}>
                <Text style={styles.book2}>Total Cost</Text>
                <Text style={[styles.book2, {color: '#1C8ADB', fontWeight: '500'}]}>&#8377;{this.state.totalCost}</Text>
              </View>
            </View>
            <View style={{alignItems : 'center', marginTop: '5%'}}>
              <TouchableNativeFeedback
                disabled={this.state.disable}
                onPress={() => this.initiate()}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
                  <View style={{ alignItems: 'center'}}>
                    <Text style={styles.book}>  JOIN  </Text>
                  </View>
                </LinearGradient>
              </TouchableNativeFeedback>
            </View>
            <View style={{marginTop: '5%'}}>
              <View style={{alignItems: 'center'}}>
                <Text style={{fontSize: 16, color: 'grey', width: '90%', paddingLeft: 10}}>Note - Steps to find your Gaming Id</Text>
              </View>
              <View style={{alignItems: 'center', marginTop: '2%'}}>
                <Image
                  source={{uri: 'https://gameon-tournaments.s3.ap-south-1.amazonaws.com/step1.PNG'}}
                  style={styles.image}>
                </Image>
              </View>
              <View style={{alignItems: 'center', marginTop: '2%'}}>
                <Image
                  source={{uri: 'https://gameon-tournaments.s3.ap-south-1.amazonaws.com/step2.PNG'}}
                  style={styles.image}>
                </Image>
              </View>
            </View>
          </View>
        </View>
        )
      )
    }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
	modal: {
		width: '90%',
		borderWidth: 1,
		borderRadius: 5,
		backgroundColor: 'white',
		alignItems: 'center',
	},
  book2: {
    color: 'grey',
    fontWeight: '400',
    fontSize: 18,
  },
  image: {
		height: hp('8%'),
		width: '90%', 
  },
	textcontainer: {
		width: '90%',
    paddingLeft: 10,
    fontSize: 18,
		color: '#525452',
  },
	errcontainer: {
		width: '90%',
    paddingLeft: 10,
    fontSize: 14,
		color: 'red',
	},
	inputcontainer: {
		marginTop: '4%',
		paddingLeft: 10,
		paddingRight: 10,
		alignItems: 'center',
	},
	input: {
		marginTop: '2%',
		borderColor: 'grey',
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 2,
		width: '90%',
		padding: 10,
		fontSize: 18,
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%'
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 2,
  },
})