import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import {localNotifCancel} from './Notifications';
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  FlatList,
  BackHandler,
  Animated,
  Easing} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class BookingCancelled extends Component {
  constructor(props) {
    super(props);
    this.ok = this.ok.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentWillMount() {
    localNotifCancel();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  ok() {
    this.props.navigation.navigate('Account');
    return true;
  }
  
  render() {
    const {navigation} = this.props;
    const item = navigation.getParam('item');
    const credit = navigation.getParam('credit');
    return(
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <TouchableNativeFeedback
								onPress={this.ok}>
                <View style={{padding: 10, paddingTop: 15}}>
                  <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                </View>
              </TouchableNativeFeedback>
            </View>
            <View style={{padding: 10, flexDirection: 'row', justifyContent: 'space-between', marginTop: '2%'}}>
              <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Booking Cancelled</Text>
              <Icon name='md-checkmark-circle-outline' color={'#e02c10'} size={28} style={{marginLeft: '10%'}}></Icon>
            </View>
          </View>
          <View></View>
        </View>
        <View style={{width: '90%', alignItems: 'center', alignSelf: 'center'}}>
          <View style={styles.shadow}>
            <Image
              source={{uri: item.Poster}}
              style={{height: hp('30%'), borderRadius: 3}}>
            </Image>
          </View>
        </View>
        <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
          <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>{item.Name}</Text>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Booking ID - <Text style={{color : '#1C8ADB'}}>{item.TBookingId}</Text></Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Status: <Text style={{color: 'red', fontWeight: '400'}}>Cancelled</Text></Text>
          </View>
          <View style={{marginTop: 10, flexDirection: 'row'}}>
            <Text style={{fontSize: 18, color: '#1C8ADB'}}>GCredits earned : </Text>
            <View style={{flexDirection: 'row'}}>
              <View>
                <Image
                  source={require('../images/GCoins.png')}
                  style={{width: 28, height: 23}}>
                </Image>
              </View>
              <View>
                <Text style={{color: '#1C8ADB', fontSize: 18, fontWeight: '500'}}>{credit == 0 || credit == undefined ? "0" : credit}</Text>
              </View>
            </View>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Booked for <Text style={{color : '#1C8ADB', fontSize: 18}}>{item.Date}</Text></Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Timing : </Text>
            <Text style={{color : '#1C8ADB', fontSize: 18}}>{item.Hours}:{item.Minutes}</Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 18, color: '#5a5a5e'}}>Paid : <Text style={{color: 'green'}}>&#8377;{item.TotalCost}</Text></Text>
          </View>
        </View>
        <View style={{alignItems : 'center', bottom: 0, position: 'absolute', width: '100%'}}>
          <TouchableNativeFeedback
            onPress={this.ok}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF2F03', '#A63300']} style={styles.linearGradient}>
              <View style={{ alignItems: 'center'}}>
                <Text style={styles.ok}>  Done  </Text>
              </View>
            </LinearGradient>
          </TouchableNativeFeedback>
        </View> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
	},
	text: {
		padding: 10,
		fontSize: 15,
  },
  ok: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%'
  },
  CafeName: {
		fontSize: 20,
    fontWeight: '500',
  },
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 2, 
    // borderBottomWidth: 2,
    // borderRightWidth: 2,
    // borderColor: '#c6c2c2',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: 10,
  },
})
