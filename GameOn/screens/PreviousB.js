import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import config from "../extra/config";
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  FlatList,
  BackHandler,
  Animated,
  Easing} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class Previous extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookings : [],
      loading : true,
      user : null,
      //rotateAnim: new Animated.Value(0.8),
    }
    this.getBookings = this.getBookings.bind(this);
  }

  async componentDidMount() {
    //this.animation();
    let state = this.props.navigation.getParam('user');
    await this.setState({
      user : state
    })
    this.getBookings();
  }

  async getBookings() {
    let date = new Date();
    this.setState({
      loading : true
    })
    try {
      let response = await fetch(`${config.bookingurl}previousT`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        UserId: this.state.user,
        Date: date.getFullYear()+'-'+(date.getMonth()+1 == 13 ? '01' : (date.getMonth()+1 < 10 ? '0'+date.getMonth()+1 : date.getMonth()+1))+'-'+(date.getDate() < 10 ? '0'+date.getDate() : date.getDate()),
      }),
    })
      let responseJson = await response.json();
      if(responseJson.length != 0){
        console.log(responseJson);
        this.setState({
          bookings : responseJson,
          loading : false, 
        })
      }
      else {
        this.setState({loading : false})
      }
    } catch (err) { 
      console.log(err.message);
    }
  }

  // animation() {
	// 	this.state.rotateAnim.setValue(0.8)
  //   Animated.spring(
  //     this.state.rotateAnim,
  //     {
  //       toValue: 1,
  //       friction : 0.9,
	// 			easing: Easing.ease,
  //     }
  //   ).start(() => {
  //     this.animation()
  //   })
	// }

  render() {
    const { goBack } = this.props.navigation;
    const {navigate} = this.props.navigation;
    return (
      this.state.loading ? (
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <View style={{alignItems: 'center', marginTop: '50%', justifyContent: 'center',}}>
            <ActivityIndicator size={50} color="#1C8ADB" style={{marginTop: '20%'}} />
          </View>
        </View>
      ) : (
        <View style={{flex: 1}}>
          <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback>
                  <View style={{padding: 10, paddingTop: 15}}>
                    <Icon name='md-arrow-back' color={'black'} size={28} onPress={() => goBack()}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10, marginTop: '2%'}}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>My Bookings</Text>
              </View>
            </View>
						<View></View>
          </View>
        <ScrollView>
          {this.state.bookings.length >= 1 ? (
            <FlatList
            data={this.state.bookings}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => 
              <TouchableNativeFeedback
                onPress={() => navigate('BookedDetails', {item : item, screen : "Previous"})}>
                <View>
                  <View style={styles.container}>
                    <View>
                      <View style={{alignItems: 'center'}}>
                        <View style={styles.shadow}>
                          <Image
                            source={{uri: item.Poster}}
                            style={{height: hp('27%'), borderRadius: 3}}>
                          </Image>
                        </View>
                      </View>
                      <View style={{marginLeft: '6%', padding: 5}}>
                        <Text style={styles.CafeName}>{item.Name}</Text>
                        {item.Status == "Cancelled" ? (
                          <View style={{marginTop: 7, marginLeft: 18}}>
                            <Text style={{color: '#575859', marginLeft: 5}}>Status: <Text style={{color: 'red', fontWeight: '400'}}>{item.Status}</Text></Text>
                          </View>
                        ) : null}
                        <View style={{flexDirection: 'row', marginTop: 7}}>
                          <Icon2 name='calendar-o' color={'#28B463'} size={18}></Icon2>
                          <Text style={{color: '#575859', marginLeft: 5}}>{item.Date}</Text>
                        </View>
                        <View style={{flexDirection: 'row', marginTop: 7}}>
                          <Icon2 name='clock-o' color={'#799e9e'} size={19}></Icon2>
                          <Text style={{color: '#575859', marginLeft: 5}}>{item.Hours}:{item.Minutes}</Text>
                        </View>
                      </View>
                      {/* <View style={{borderBottomWidth: 1, borderBottomColor: '#bec4ce', width: '84%', alignSelf: 'center', marginTop: 5}}></View>
                        <View style={{alignItems: 'center'}}>
                          <TouchableNativeFeedback
                            onPress={() => this.ToRate(item.Cafe)}>
                            <View>
                              <Text style={{color: '#fc9700', fontSize: 18, fontWeight: '400', padding: 10}}>Rate this Cafe</Text>
                            </View>
                          </TouchableNativeFeedback>
                        </View> */}
                    </View>
                  </View>
                </View>
              </TouchableNativeFeedback>
              }
              keyExtractor={item => item._id}
              refreshing={this.state.refreshing}
              style={{width : '96.5%', alignSelf: 'center', marginBottom: 3}}>
            </FlatList>
          ) : (
            <View style={{alignItems: 'center', marginTop: '50%'}}>
              <Text style={{padding: 10, color: 'black'}}>There are no past bookings to show</Text>
            </View>
          )}
        </ScrollView>
      </View>
      )
    )
  }
}  

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
	},
	text: {
		padding: 10,
		fontSize: 15,
  },
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
  image: {
    width: wp('35%'),
    height: hp('20%'),
    borderRadius: 100,
		backgroundColor: 'white',
  },
  container: {
		borderBottomWidth: 1,
		borderRadius: 3,
		padding: 7,
		borderColor: '#bec4ce',
    marginTop: 5,
    marginBottom : 2,
		backgroundColor: 'white',
  },
  CafeName: {
    color: 'black',
		fontSize: 20,
	},
})
