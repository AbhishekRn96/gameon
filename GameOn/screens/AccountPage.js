import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/SimpleLineIcons';
import Modal from "react-native-modal";
import config from "../extra/config";
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  Alert} from 'react-native';

export default class AccountPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dp : null,
      name : null,
      show : false,
      credit : 0,
      user : null,
      refresh : false,
    }
    this.Refresh = this.Refresh.bind(this);
    this.logout = this.logout.bind(this);
  }

  async componentDidMount() {
    //BackHandler.addEventListener('hardwareBackPress', () => this.props.navigation.navigate('Dashboard'));
    await AsyncStorage.multiGet(['userid', 'dp', 'gcredits', 'user']).then((data) => {
      this.setState({
        name : data[0][1],
        dp : data[1][1],
        credit : data[2][1],
        user : data[3][1],
      })
    })
  }

  async Refresh() {
    this.setState({refresh : true});
    fetch(`${config.userurl}getUser`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        UserId : this.state.user,
      }),
    })
    .then(response =>  response.json())
    .then(async (resData) => {
      if(resData.message === "failed") {
        this.setState({
          refresh: false,
        })
      } else {
        this.setState({name : resData.Username, credit : resData.GCredits, refresh : false});
        await AsyncStorage.multiSet([
          ['userid', resData.Username],
          ['email', resData.Email],
          ['dp', resData.DP],
          ['contact', resData.Contact],
          ['gcredits', resData.GCredits+''],
          ['user', resData.UserId]
        ])
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  async logout() {
		this.setState({show : false})
    await AsyncStorage.clear()
    this.props.screenFocus();
		this.props.navigation.navigate('Auth');
	}

  render() {
    const {navigate} = this.props.navigation;
    return(
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={{width : '97%', alignSelf: 'center', marginTop: 10}}>
            <View style={styles.container}>
              <TouchableNativeFeedback
                disabled={true}
                onPress={() => navigate('Details', {user: this.state.user})}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image
                      source={{uri: this.state.dp}}
                      style={{width: 80, height: 80, borderRadius: 100}}>
                    </Image>
                  </View>
                  <View style={{marginLeft: 10, marginTop: 10}}>
                    <View>
                      <Text style={{fontSize: 22, color: '#4d4f54'}}>{this.state.name}</Text>
                      <View style={{flexDirection: 'row', marginTop: 7, justifyContent: 'space-between', width: '62%'}}>
                        <View style={{flexDirection: 'row'}}>
                          <View>
                            <Image
                              source={require('../images/GCoins.png')}
                              style={{width: 28, height: 23}}>
                            </Image>
                          </View>
                          <View>
                            <Text style={{color: '#1C8ADB', fontSize: 18, fontWeight: '500'}}>{this.state.credit == 0 || this.state.credit == undefined ? "0" : this.state.credit}</Text>
                          </View>
                        </View>
                        <View>
                          {this.state.refresh ? (
                            <ActivityIndicator size={25} color="#1C8ADB" />
                          ) : (
                            <Icon3 name='reload' color={'#1C8ADB'} size={24} onPress={() => this.Refresh()}></Icon3>
                          )}
                        </View>
                      </View>
                    </View>
                    {/* <View style={{flexDirection: 'row', marginTop: 10, marginLeft: 5}}>
                      <View>
                        <Image
                          source={require('../images/GCoins.png')}
                          style={{width: 30, height: 25}}>
                        </Image>
                      </View>
                      <View style={{marginLeft: 7}}>
                        <Text style={{color: '#1C8ADB', fontSize: 20, fontWeight: '500'}}>{this.state.credit == 0 || this.state.credit == undefined ? "0" : this.state.credit}</Text>
                      </View>
                    </View> */}
                  </View>
                </View>
              </TouchableNativeFeedback>
            </View>
          <View style={styles.container}>
              <TouchableNativeFeedback
                onPress={() => navigate('Withdraw')}>
                <View style={styles.settingItems}>
                  <Text style={styles.text}>Withdraw</Text>
                  <View>
                    <Icon name='angle-right' color={'#d6d8db'} size={28} style={{padding: 10}}></Icon>
                  </View>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback
                onPress={() => navigate('Offers')}>
                <View style={styles.settingItems}>
                  <Text style={styles.text}>My Offers</Text>
                  <View>
                    <Icon name='angle-right' color={'#d6d8db'} size={28} style={{padding: 10}}></Icon>
                  </View>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback
                onPress={() => navigate('Upcoming', {user : this.state.user})}>
                <View style={styles.settingItems}>
                  <Text style={styles.text}>Upcoming Tournaments</Text>
                  <View>
                    <Icon name='angle-right' color={'#d6d8db'} size={28} style={{padding: 10}}></Icon>
                  </View>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback
                onPress={() => navigate('Previous', {user : this.state.user})}>
                <View style={styles.settingItems}>
                  <Text style={styles.text}>My Tournaments</Text>
                  <View>
                    <Icon name='angle-right' color={'#d6d8db'} size={28} style={{padding: 10}}></Icon>
                  </View>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback
							  onPress={() => Alert.alert(
                  '',
                  'Are you sure you want to logout?',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {text: 'OK', onPress: () => this.logout()},
                  ],
                  {cancelable: true},
                )}>
                <View style={styles.settingItemsLast}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon2 name='md-log-out' color={'#e02c10'} size={28} style={{padding: 10}}></Icon2>
                    <Text style={styles.text}>Log Out</Text>
                  </View>
                  <View>
                    <Icon name='angle-right' color={'#d6d8db'} size={28} style={{padding: 10}}></Icon>
                  </View>
                </View>
              </TouchableNativeFeedback>
          </View>
        </View>
      </View> 
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
  },
  container: {
		borderWidth: 1,
		borderRadius: 3,
		padding: 7,
		borderColor: '#bec4ce',
		marginTop: 5,
		backgroundColor: 'white',
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
  },
  text: {
    fontSize: 18,
    color: '#4d4f54',
    padding: 10
  },
  settingItems: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    borderBottomWidth: 1, 
    borderBottomColor: '#d6d8db', 
    width: '100%', 
    marginTop: 5, 
    marginBottom: 5
  },
  settingItemsLast: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    width: '100%', 
    marginTop: 5, 
    marginBottom: 5
  },
})
