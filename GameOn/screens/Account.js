import React, {Component} from 'react';
import {AsyncStorage, View, ActivityIndicator, StyleSheet, StatusBar} from 'react-native';
import AccountPage from './AccountPage';
import LogIn from './Login';

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user : null,
      loading : true,
    }
    this.onScreenFocus = this.onScreenFocus.bind(this);
    this.navig = this.props.navigation.addListener('willFocus', this.onScreenFocus);
  }

  // componentWillMount() {
  //   this.navig = this.props.navigation.addListener('willFocus', this.onScreenFocus);
  // }

  onScreenFocus() {
    this.setState({loading : true, user : null});
    AsyncStorage.getItem('user').then((result) => {
      if(result && this.state.user !== result) {
        this.setState({user : result, loading : false});
      } else if(result === null) {
        this.setState({user : null, loading : false});
      }
    })
    // this.navig.remove();
  }

  render() {
    return(
      <View style={{flex: 1, backgroundColor: 'white'}}>
        {this.state.loading ? (
          <View style={styles.container}>
            <StatusBar
              backgroundColor="white"
              barStyle="dark-content"
            />
            <ActivityIndicator />
            <StatusBar barStyle="default" />
          </View>
        ) : this.state.user ? (
          <AccountPage navigation={this.props.navigation} screenFocus={this.onScreenFocus} />
        ) : (
          <LogIn navigation={this.props.navigation} />
        )}
      </View> 
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
