import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { StackActions } from 'react-navigation';
import { showMessage, hideMessage } from "react-native-flash-message";
import {configure} from './Notifications';
import config from "../extra/config";
import {TextInput, 
				View,
				TouchableOpacity,
				TouchableNativeFeedback,
				BackHandler,
				Text,
				PermissionsAndroid,
				Button,
				ScrollView,
				Keyboard,
				AsyncStorage,
				KeyboardAvoidingView,
				ActivityIndicator} from 'react-native';

export default class Verify extends Component {
	constructor(props) {
		super(props);
		this.state = {
			OTP : '',
			verifyOTP: '',
			number : '',
			hint : true,
			resend : true,
		}
		this.Verify = this.Verify.bind(this);
		this.Resend = this.Resend.bind(this);
	}

	componentDidMount() {
		let number = this.props.navigation.getParam('number');
		let otp = this.props.navigation.getParam('otp');
		this.setState({
			number : number,
			verifyOTP : otp,
		})
	}

	Verify() {
		this.setState({
			hint : false,
		})
		Keyboard.dismiss();
		fetch(`${config.userurl}checkUserOTP`, {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				number: '+91'+this.state.number,
				OTP: this.state.OTP,
				}),
			})
			.then(response =>  response.json())
			.then(async (responseJson) => {
				console.log(responseJson);
				if(responseJson.message == 'failed'){
					showMessage({
						message: "Invalid OTP",
						type: "danger",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
						OTP : '',
					});
					this.setState({hint : true})
				}
				else{
					await AsyncStorage.multiSet([
						['userid', responseJson.Username],
						['email', responseJson.Email],
						['dp', responseJson.DP],
						['contact', responseJson.Contact],
						['gcredits', responseJson.GCredits+''],
						['user', responseJson.UserId],
						['vpa', responseJson.VPA ? responseJson.VPA : '']
					])
					console.log(await AsyncStorage.getItem('userid'));
					configure(responseJson.Contact);
					let route = this.props.navigation.getParam('route');
					let counter = this.props.navigation.getParam('counter');
					if(route) {
						this.props.navigation.dispatch(StackActions.pop({
							n: counter,
						}));
					} else {
						this.props.navigation.navigate('App')
					}
					this.setState({
						hint : true,
					})
				}
      }).catch (err => {
      console.log("verify ",err);
    })
	}

	Resend() {
		this.setState({resend : false});
		fetch(`${config.userurl}resend`, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
    			'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					number: '+91'+this.state.number,
				}),
      })
      .then(response =>  response.json())
      .then(responseJson => {
        if(responseJson.message == 'failed'){
					showMessage({
						message: "Failed to resend OTP",
						type: "danger",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
					});
					this.setState({resend : true});
        }
        else{
					this.setState({verifyOTP : Number(responseJson.message)});
					showMessage({
						message: "OTP sent",
						type: "success",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
					});
					this.setState({resend : true});
        }
      }).catch (err => {
      console.log(err);
    })
	}

	render() {
		const { goBack } = this.props.navigation;
		return(
			<View style={styles.container}>
				<View style={[styles.headerStyle, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
					<View style={{marginLeft: '3%'}}>
						<View>
							<TouchableNativeFeedback
								onPress={() => goBack()}>
								<View style={{padding: 10}}>
									<Icon name='md-arrow-back' color={'black'} size={28}></Icon>
								</View>
							</TouchableNativeFeedback>
						</View>
					</View>
					<View></View>
				</View>
				<View style={{alignItems: 'flex-start',marginTop: '5%', marginLeft: '5%'}}>
					<Text style={styles.signup}>VERIFY CONTACT NUMBER</Text>
					<Text style={{width: '88%',	paddingLeft: 10,}}>OTP sent to {this.state.number}</Text>
				</View>
				<View style={{marginTop: '5%'}}>
					<View style={{alignItems: 'center'}}>
						<Text style={styles.textcontainer}>Enter OTP</Text>
					</View>
					<KeyboardAvoidingView behavior='position'>
						<View style={styles.inputcontainer}>
							<TextInput 
								style={styles.input}
								underlineColorAndroid='transparent'
								//placeholder= "Email"
								keyboardType='numeric'
								onChangeText = { (text) => this.setState({OTP : text})}
								value = {this.state.OTP}
							>
							</TextInput>
						</View>
					</KeyboardAvoidingView>
				</View>
				{this.state.resend ? (
					<View style={{alignItems: 'flex-end',marginTop: '9%', marginLeft: '60%',}}>
						<Text style={styles.resend} onPress={this.Resend}>Resend OTP</Text>
					</View>
				) : (
					<View style={{alignItems: 'flex-end',marginTop: '9%', marginRight: '20%'}}>
						<ActivityIndicator allowFontScaling={false} size={25} color="#1C8ADB" />
					</View>
				)}
				<View style={{marginTop: '7%'}}>
					{this.state.hint ? (
						<View style={styles.buttonContainer}>
							<TouchableOpacity
								disabled = {this.state.OTP.length === 6 ? false : true}
								style = {this.state.OTP.length === 6 ? styles.buttonContinue : styles.buttonContinueDisabled}
								onPress={this.Verify}
							>
								<Text style={this.state.OTP.length === 6 ? styles.text : styles.textDisabled}>  VERIFY  </Text>
							</TouchableOpacity>
						</View>
					) : (
						<ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
					)}
				</View>
				{/* <View style={styles.buttonContainer}>
          <Text style={styles.or}>OR</Text>
        </View> */}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 11,
		backgroundColor: 'white',
	},
	forgot: {
		color: '#525452',
		fontWeight: 'bold',
	},
	headerStyle: {
		backgroundColor: 'white',
	},
	textBoxBtnHolder:
	{
		position: 'relative',
		alignSelf: 'stretch',
		justifyContent: 'center',
		width: '95%',
		paddingTop: 1,
		paddingLeft: 30,
		marginTop: 3,
		//paddingRight: 10,
		marginBottom: 3,
	},
	visibilityBtn:
	{
		position: 'absolute',
		right: 3,
		height: 40,
		width: 35,
		padding: 5,
	},
	btnImage:
	{
		resizeMode: 'contain',
		height: '100%',
		width: '100%'
	},
	textDisabled: {
		color: '#1C8ADB',
		fontWeight: 'bold',
		fontSize: 16,
	},
	text: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 16,
	},
	button: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#0d3f93',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 45,
	},
	back: {
		padding: 10,
		borderRadius: 100,
		backgroundColor: 'white',
		//marginTop: 5,
		width: '14%',
		alignItems: 'center',
		height: 45,
		borderWidth: 2,
	},
	buttonContinue: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#1C8ADB',
		backgroundColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
	},
	buttonContinueDisabled: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
		opacity: 0.7,
	},
	buttonContainer: {
		padding: 10,
		alignItems:'center',
	},
	password: {
		marginTop: 5,
		marginBottom: 25,
		width: '80%',
		padding: 10,
		height: 44,
		borderColor: 'grey',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderLeftWidth: 1,
		borderStyle: 'solid',
	},
	passwordcontainer: {
		paddingTop: 1,
		paddingBottom: 1  ,
		paddingLeft: 10,
		paddingRight: 10,
		flexDirection: 'row',
		alignItems: 'center',
	},
	toolbar: {
		height: 50,
		borderWidth: 2,
		borderColor: 'grey',
		alignItems: 'stretch',
		elevation: 1,
	},
	signup: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#0e4b77',
		fontSize: 20,
		paddingBottom: 12,
	},
	resend: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#0e4b77',
		fontSize: 15,
	},
	textcontainer: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#878a91',
		fontSize: 15,
	},
	textcontainer2: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#878a91',
	},
	textcontainerusername: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#525452',
		fontStyle: 'italic',
	},
	emailerror: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: '400',
		color: 'grey',
		fontStyle: 'italic'
	},
	inputcontainer: {
		//paddingTop: 1,
		//paddingBottom: 1  ,
		paddingLeft: 10,
		paddingRight: 10,
		alignSelf: 'center',
		flexDirection: 'row',
	},
	input: {
			marginTop: 5,
			marginBottom: 3,
			borderColor: '#aeb1b7',
			borderBottomWidth: 1,
			borderStyle: 'solid',
			width: '88%',
			padding: 10,
			height: 80,
			fontSize: 25,
			letterSpacing: 30
		},
		inputusername: {
			marginTop: 5,
			marginBottom: 3,
			borderColor: 'grey',
			borderWidth: 1,
			borderStyle: 'solid',
			width: '88%',
			padding: 10,
			height: 44,
			fontSize: 18,
			},
	});