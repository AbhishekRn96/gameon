import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';
import Modal from 'react-native-modal';
import config from "../extra/config";
import {TextInput,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        InteractionManager, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button,
        Dimensions,
        ScrollView, 
        Keyboard,
        StatusBar,
        BackHandler} from 'react-native';

export default class ViewTour extends Component {
    constructor(props) {
        super(props);
        this.state = {
          didFinishInitialAnimation: false,
          refresh : false,
          error : false,
          loading : false,
          login : false,
          count : null,
        }
        this.Refresh = this.Refresh.bind(this);
        this.checkAvailability = this.checkAvailability.bind(this);
        this.navigateBack = this.navigateBack.bind(this);
        this.ToBooking = this.ToBooking.bind(this);
    }

    componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
        this.setState({
          didFinishInitialAnimation: true,
        });
      });
    }

    checkAvailability() {
      this.setState({loading : true});
      let item = this.props.navigation.getParam('item');
      fetch(`${config.bookingurl}TBookingAvailability`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          TournamentId : item.TournamentId,
        }),
      })
      .then(response =>  response.json())
      .then((resData) => {
        if(resData.message === "noSlots") {
          this.setState({error : true, loading: false});
        } else if(resData.message === "available"){
          this.props.navigation.navigate('TBooking', {item : item, login : this.state.login});
          this.setState({loading : false});
        }
      })
      .catch(err => {
        console.log("fail ",err);
        this.setState({loading : false});
      });
    }

    Refresh() {
      this.setState({refresh : true});
      let item = this.props.navigation.getParam('item');
      fetch(`${config.userurl}checkAvailability`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          TournamentId : item.TournamentId,
        }),
      })
      .then(response =>  response.json())
      .then((resData) => {
        if(resData.count) {
          this.setState({count : resData.count, refresh : false});
        }
      })
      .catch(err => {
        console.log("fail ",err);
        this.setState({refresh : false});
      });
    }

    navigateBack() {
      this.setState({loading: false, error: false});
      this.props.navigation.goBack();
    }

    ToBooking() {
      AsyncStorage.getItem('userid').then((result) => {
        if(result) {
          this.checkAvailability();
        } else {
          this.setState({login : true});
          this.props.navigation.navigate('Login', {route: 'ViewTour'});
        }
      })
    }

    render() {
      const {navigate} = this.props.navigation;
      const { goBack } = this.props.navigation;
      const {navigation} = this.props;
      const item = navigation.getParam('item');
      return(
        this.state.didFinishInitialAnimation == false ? (
          <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={styles.container}>
              <ActivityIndicator />
              <StatusBar
                backgroundColor="white"
                barStyle="dark-content"
              />
            </View>
          </View>
        ) : (
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', top: 0}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback
								  onPress={() => goBack()}>
                  <View style={{padding: 10, paddingTop: 10, paddingRight: 10}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10}}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>About</Text>
              </View>
            </View>
            <View style={{marginRight: '4%'}}>
              <TouchableNativeFeedback
                onPress={() => this.ToBooking()}>
                <View style={styles.edit}>
                  <Text style={styles.book}>  JOIN  </Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.error}
            backdropOpacity={0.9}
            backdropColor='white'
            onBackdropPress={() => this.setState({error : false})}
            onBackButtonPress={() => this.setState({error : false})}
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{alignItems: 'center'}}>
                <View style={{padding: 10}}>
                  <Text style={{fontSize: 16, color: 'black'}}>Sorry, there are no Slots available for this Tournament</Text>
                </View>
                <TouchableNativeFeedback
                  onPress={() => this.navigateBack()}>
                  <View style={{marginTop: '2%', padding: 10}}>
                    <Text style={{fontSize: 16, color: '#1C8ADB', fontWeight: '500'}}>OK</Text>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
          </Modal>
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.loading}
            backdropOpacity={0.9}
            backdropColor='white'
            style={{alignItems: 'center'}}>
            <View style={styles.modal}>
              <View style={{flexDirection: 'row', padding: 10}}>
                <ActivityIndicator size={22} color="#1C8ADB" />
                <Text style={{marginLeft: '3%', fontSize: 16, color: 'black'}}>Checking for Slot Availability</Text>
              </View>
            </View>
          </Modal>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Image
                source={{uri: item.Poster}}
                style={styles.image}>
              </Image>
            </View>
            <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
              <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>{item.Name}</Text>
            </View>
            <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
              <Text style={{color: 'green', fontSize: 16}}>Entry Fee : {'\u20B9'}{item.Fee}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'flex-start', width: '90%', marginTop: 10, alignSelf: 'center', justifyContent: 'space-between'}}>
              <View style={{flexDirection: 'row'}}>
                <Icon3 name='calendar-o' color={'#28B463'} size={20}></Icon3>
                <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>{item.DateString}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Icon3 name='clock-o' color={'#799e9e'} size={20}></Icon3>
                <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>{item.Hours}:{item.Minutes}</Text>
              </View>
            </View>
            <View style={{alignItems: 'flex-start', width: '90%', marginTop: 10, alignSelf: 'center', flexDirection: 'row'}}>
              <View style={{flexDirection: 'row'}}>
                <Icon2 name='sword-cross' color={'#1C8ADB'} size={20}></Icon2>
                <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>Slots available : {this.state.count ? Number(item.Slots) - this.state.count : item.SlotsAvailable}/{item.Slots}</Text>
              </View>
              <View style={{marginLeft: '3%'}}>
                {this.state.refresh ? (
                  <ActivityIndicator size={22} color="#1C8ADB" />
                ) : (
                  <Icon4 name='reload' color={'#1C8ADB'} size={22} onPress={() => this.Refresh()}></Icon4>
                )}
              </View>
            </View>
            <View style={{padding: 10, borderBottomWidth: 1, borderColor: '#ccccc5', width: '90%',  alignSelf: 'center'}}></View>
            <View style={{marginTop: 10, marginBottom: '10%'}}>
              <View style={{width: '90%', alignSelf: 'center'}}>
                <Text style={styles.content}>{item.Desc}</Text>
              </View>
            </View>
          </ScrollView>
        </View>
        )
      )
    }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
	modal: {
		width: '90%',
		borderWidth: 1,
		borderRadius: 5,
		backgroundColor: 'white',
		alignItems: 'center',
	},
  carousal: {
  backgroundColor: 'white'
    //width: Dimensions.get("window").width,
  },
  map : {
    left: 0,
    right: 0,
    position: 'absolute'
  },
  item: {
    alignItems: 'center',
  },
  content: {
    color: '#5a5c5e', 
    fontWeight: '300', 
    flexWrap: 'wrap',
    fontSize: 14
  },
  button: {
    padding: 10,
    backgroundColor: '#0e8c0c',
    width: '90%',
    alignItems: 'center',
    height: 50,
    borderRadius: 3,
  },
  buttontext: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  image: {
    width: Dimensions.get("window").width, 
    height: 230,
  },
  image2: {
    width: 45, 
    height: 45,
  },
  edit: {
		backgroundColor: 'white',
    borderRadius: 10,
		alignItems: 'center',
    borderColor: 'white',
		//marginBottom: '5%',
		//marginRight: 5,
  },
  book: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 15,
    padding: '2%'
  },
  title: {
    marginLeft: 10,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#1C8ADB',
  },
  desc: {
    fontWeight: '400',
    color: '#5a5c5e',
    fontSize: 20,
  },
  address: {
    fontWeight: '600',
    color: '#1C8ADB',
    marginLeft: 5,
    marginTop: 10
  },
})