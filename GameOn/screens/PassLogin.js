import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Ionicons';
import { StackActions } from 'react-navigation';
import Modal from "react-native-modal";
import { Header } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';
import { showMessage, hideMessage } from "react-native-flash-message";
import config from "../extra/config";
import {configure} from './Notifications';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {TextInput,
        View, 
        AsyncStorage, 
        TouchableOpacity, 
        Text, 
        Image, 
        Keyboard, 
        KeyboardAvoidingView, 
        ScrollView, 
        StatusBar, 
        TouchableNativeFeedback,
        ActivityIndicator,
        ImageBackground} from 'react-native';

import renderIf from '../extra/renderIf';

export default class PassLogin extends Component { 
  constructor(props) {
    super(props);
      this.state = {
        showpassword : true,
        indicate : true,
        user : '',
        pass : '',
        color1 : 'grey',
        color2 : 'grey',
      };
      //this.onChangeUsername = this.onChangeUsername.bind(this);
      this.Login = this.Login.bind(this);
    }

    componentDidMount() {
      SplashScreen.hide();
    }

    Login = () => {
      this.setState({
        indicate : false,
      })
      Keyboard.dismiss();
      fetch(`${config.userurl}loginPassword`, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
    			'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					user: this.state.user,
					pass: this.state.pass,
				}),
      })
      .then(response =>  response.json())
      .then(async (responseJson) => {
        if(responseJson.message){
          this.setState({
            indicate : true,
          })
          showMessage({
						message: "Invalid Username or Password",
						type: "danger",
						hideOnPress: true,
						duration: 3000,
						autoHide: true,
					});
        }
        else{
          await AsyncStorage.multiSet([
            ['userid', responseJson.Username],
            ['email', responseJson.Email],
            ['dp', responseJson.DP],
            ['contact', responseJson.Contact],
            ['gcredits', responseJson.GCredits+''],
            ['user', responseJson.UserId]
          ])
          console.log(await AsyncStorage.getItem('userid'));
          configure(responseJson.Contact);
          let route = this.props.navigation.getParam('route');
          let counter = this.props.navigation.getParam('counter');
          this.setState({
            indicate : true,
          })
          if(route) {
            this.props.navigation.dispatch(StackActions.pop({
              n: counter,
            }));
          } else {
            this.props.navigation.navigate('App')
          }
        }
      }).catch (err => {
      console.log(err);
    })
	}

    render() {
      const {navigate} = this.props.navigation;
      let route = this.props.navigation.getParam('route');
      const { goBack } = this.props.navigation;
      return(
        <View style={{flex: 1, flexDirection: 'column'}}>
          <View style={[styles.headerStyle, {flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
            <View style={{marginLeft: '3%'}}>
              <View>
                <TouchableNativeFeedback
								  onPress={() => goBack()}>
                  <View style={{padding: 10}}>
                    <Icon2 name='md-arrow-back' color={'black'} size={28}></Icon2>
                  </View>
                </TouchableNativeFeedback>
              </View>
            </View>
            <View></View>
          </View>
          <View style={{marginTop: '5%', marginLeft: '10%'}}>
            <Text style={{fontSize: 20, fontWeight: '500', marginTop: '3%'}}>LOGIN</Text>
            <Text style={{marginTop: '5%'}}>Enter your Credentials below to Login</Text>
          </View>
          <KeyboardAvoidingView behavior='padding'>
            <View style={[styles.inputcontainer, {marginTop: '5.5%'}]}>
              <Icon allowFontScaling={false} name='user' color={this.state.color1} size={26} style={{marginLeft: '17%'}}></Icon>
              <View style = { [styles.textBoxBtnHolder, {marginLeft: '20%'}] }>
                <TextInput
                  allowFontScaling={false}
                  underlineColorAndroid='transparent'
                  style={styles.input}
                  placeholder = 'Username or Email'
                  placeholderTextColor={'#878a91'}
                  onFocus = {() => this.setState({color1 : '#1C8ADB'})}
                  onBlur = {() => this.setState({color1 : 'grey'})}
                  onChangeText = {(text) => this.setState({user : text})}
                  value = {this.state.user}
                />
              </View>
            </View>
            <View style={[styles.inputcontainer, {padding: 7}]}>
              <Icon allowFontScaling={false} name='lock' color={this.state.color2} size={26} style={{marginLeft: '11.5%'}}></Icon>
              <View style = { styles.textBoxBtnHolder }>
                <TextInput 
                  allowFontScaling={false}
                  underlineColorAndroid = "transparent"  
                  placeholder = 'Password' 
                  placeholderTextColor={'#878a91'}
                  onFocus = {() => this.setState({color2 : '#1C8ADB'})}
                  onBlur = {() => this.setState({color2 : 'grey'})}
                  onChangeText = {(text) => this.setState({pass : text})} 
                  secureTextEntry = { this.state.showpassword }
                  style = { styles.textBox }/>
                <TouchableOpacity 
                  activeOpacity = { 1 } 
                  style = { styles.visibilityBtn } 
                  onPress = { () => this.setState({showpassword : !this.state.showpassword}) }>
                  <Image source = { ( this.state.showpassword ) ? require('../images/hide.png') : require('../images/view.png') } style = { styles.btnImage } />
                </TouchableOpacity>
              </View>
              </View>
            </KeyboardAvoidingView>
            <View style={styles.buttonContainer}>
            {this.state.indicate ? (
              <TouchableOpacity
              style = {this.state.user.length < 6 || this.state.pass.length < 6 ? styles.buttonDisabled : styles.button}
              onPress={this.Login}
              disabled={this.state.user.length < 6 || this.state.pass.length < 6 ? true : false}
              >
                <Text style={this.state.user.length < 6 || this.state.pass.length < 6 ? styles.textDisabled : styles.text}>LOGIN</Text>
              </TouchableOpacity>
            ) : (
              <ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
            )}
            </View>
          </View>
            );
        }
    }

const styles = StyleSheet.create({
  container: {
    height: '60%',
    width : '95%',
    // backgroundColor: 'white',
    // borderRadius: 5,
    alignSelf: 'center',
    marginTop: '6%',
    // borderBottomWidth: 2,
    // borderLeftWidth: 2,
    // borderTopWidth: 1,
    // borderRightWidth: 1,
    // borderColor: '#ddd',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
  },
	headerStyle: {
		backgroundColor: 'white',
	},
textBoxBtnHolder:
{
	position: 'relative',
	alignSelf: 'center',
	justifyContent: 'center',
	width: '100%',
	paddingTop: '2%',
	paddingLeft: '5%',
	//paddingRight: 10,
	marginBottom: '1.5%',
},
textBox:
{
	fontSize: 18,
	//alignSelf: 'stretch',
  height: 50,
  width: '88%',
	paddingRight: 45,
	paddingLeft: 8,
	paddingVertical: 0,
  marginLeft: '8%'
},
inputcontainer: {
  paddingLeft: 10,
  paddingRight: 10,
  paddingBottom: 10,
  paddingTop: 7,
  alignItems: 'center',
  flexDirection: 'row',
  width: '95%',
  justifyContent: 'space-around',
  //borderBottomWidth: 1,
  //borderColor: '#aeb1b7',
},
input: {
  marginTop: 5,
  marginBottom: 3,
  width: '85%',
  padding: 10,
  height: 50,
  fontSize: 18,
},
visibilityBtn:
{
	position: 'absolute',
	right: 3,
	height: 40,
	width: 35,
	padding: 5,
},
btnImage:
{
  marginTop: 3,
	resizeMode: 'contain',
	height: '100%',
	width: '100%'
},
  facebook: {
    backgroundColor: '#31439b',
    borderWidth: 1,
    borderColor: '#283187',
  },
  google: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    paddingLeft: 7,
    paddingRight: 10,
  },
  fgcontainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '92%'
  },
  warning: {
    fontWeight: 'bold',
    color: '#910700',
  },
  warningcontainer: {
    backgroundColor: '#ffbdba',
    borderWidth: 1,
    borderColor: '#910700',
    alignItems: 'center',
    padding: 10,
    width: '84%',
  },
  imagecontainer: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonContainer: {
    padding: 10,
    alignItems:'center',
    marginTop: '5%',
  },
  textContainer: {
    width: '92%',
    padding: 5,
    alignItems: 'flex-end',
    marginTop: '5%',
  },
  or: {
    fontWeight: 'bold',
    color: '#525452',
    fontSize: 16,
  },
  button: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    backgroundColor: '#1C8ADB',
    //marginTop: 5,
    width: '88%',
    alignItems: 'center',
    //opacity: 0.7,
    height: 50,
  },
  buttonDisabled: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    //marginTop: 5,
    width: '88%',
    height: 50,
    alignItems: 'center',
    opacity: 0.7,
  },
  textDisabled: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 16,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  forgot: {
    color: '#878a91',
    fontWeight: 'bold',
  }
  });
