import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import config from "../extra/config";
import {TextInput,
  AsyncStorage,
  View,
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableNativeFeedback, 
  KeyboardAvoidingView, 
  Text, 
  Image, 
  Button, 
  ScrollView, 
  Keyboard,
  FlatList,
  BackHandler,
  Animated,
  Easing} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class RateCafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating : 0,
      systems : false,
      clean : false,
      mbehaviour : false,
      bprocess : false,
      comment : "",
      userid : "",
    }
    this.addReview = this.addReview.bind(this);
  }

  componentWillMount() {
    AsyncStorage.getItem('user').then((data) => {
      this.setState({
        userid: data
      })
    })
  }

  addReview() {
    const cafe = this.props.navigation.getParam('Cafe');
    fetch(`${config.userurl}addReview`, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
    			'Content-Type': 'application/json',
				},
				body: JSON.stringify({
          CafeId: cafe.CafeId,
          Review: {
            UserId : this.state.userid,
            System : this.state.systems,
            Behavior : this.state.mbehaviour,
            Cleanliness : this.state.clean,
            BookingProcess : this.state.bprocess,
            Rating : this.state.rating,
            Comment : this.state.comment,
          }
				}),
      })
      .then(response =>  response.json())
      .then(responseJson => {
        if(responseJson.CafeId) {
          this.props.navigation.navigate('Account');
        }
      }).catch (err => {
      console.log(err);
    })
  }

  render() {
    const cafe = this.props.navigation.getParam('Cafe');
    const {navigate} = this.props.navigation;
    const { goBack } = this.props.navigation;
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%', flexDirection: 'row'}}>
            <TouchableNativeFeedback>
              <View style={{paddingLeft: 8, paddingRight: 5, paddingTop: 5}}>
                <Icon name='md-arrow-back' color={'black'} size={28} onPress={() => goBack()}></Icon>
              </View>
            </TouchableNativeFeedback>
            <Text style={{marginLeft: '10%', fontSize: 20, padding: 4, color: 'black'}}>Rate this Cafe</Text>
          </View>
          <View style={{marginRight: '4%'}}>
          </View>
        </View>
        <View style={{width: '90%', alignSelf: 'center'}}>
          <Text style={{marginTop: '1%', fontSize: 20, color: 'black'}}>{cafe.Cafename}</Text>
          <View>
            <View style={{flexDirection: 'row', marginTop: '6%', justifyContent: 'space-between'}}>
              {this.state.rating == 1 ? (
                <Icon name='md-sad' size={50} style={{marginTop: '0.5%', color: '#d32e2e'}}></Icon>
              ) : (
                <Icon name='md-sad' size={50} style={{marginTop: '0.5%', color: '#9b9b9b'}} onPress={() => this.setState({rating : 1})}></Icon>
              )}
              {this.state.rating == 2 ? (
                <Icon2 name='emoticon-sad-outline' size={50} style={{color: '#e05726'}}></Icon2>
              ) : (
                <Icon2 name='emoticon-sad-outline' size={50} style={{color: '#9b9b9b'}} onPress={() => this.setState({rating : 2})}></Icon2>
              )}
              {this.state.rating == 3 ? (
                <Icon2 name='emoticon-neutral-outline' size={50} style={{color: '#e28d24'}}></Icon2>
              ) : (
                <Icon2 name='emoticon-neutral-outline' size={50} style={{color: '#9b9b9b'}} onPress={() => this.setState({rating : 3})}></Icon2>
              )}
              {this.state.rating == 4 ? (
                <Icon2 name='emoticon-happy-outline' size={50} style={{color: '#95c41d'}}></Icon2>
              ) : (
                <Icon2 name='emoticon-happy-outline' size={50} style={{color: '#9b9b9b'}} onPress={() => this.setState({rating : 4})}></Icon2>
              )}
              {this.state.rating == 5 ? (
                <Icon2 name='emoticon-outline' size={50} style={{color: '#28a808'}}></Icon2>
              ) : (
                <Icon2 name='emoticon-outline' size={50} style={{color: '#9b9b9b'}} onPress={() => this.setState({rating : 5})}></Icon2>
              )}
            </View>
          </View>
          <View style={{marginTop: '10%'}}>
            <Text style={{fontSize: 15}}>What did you feel about the cafe?</Text>
          </View>
          <View>
          <View style={{marginTop: '10%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15}}>
              Systems
            </Text>
            <View>
              {this.state.systems ? (
                <TouchableNativeFeedback
                  onPress={() => this.setState({systems : false})}>
                  <View style={{borderWidth: 2, borderColor: 'green', backgroundColor: 'green', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'white', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.setState({systems : true})}>
                  <View style={{borderWidth: 2, borderColor: 'grey', backgroundColor: 'white', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'grey', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              )}
            </View>
          </View>
          <View style={{marginTop: '10%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15}}>
              Cleanliness
            </Text>
            <View>
              {this.state.clean ? (
                <TouchableNativeFeedback
                  onPress={() => this.setState({clean : false})}>
                  <View style={{borderWidth: 2, borderColor: 'green', backgroundColor: 'green', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'white', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.setState({clean : true})}>
                  <View style={{borderWidth: 2, borderColor: 'grey', backgroundColor: 'white', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'grey', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              )}
            </View>
          </View>
          <View style={{marginTop: '10%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15}}>
              Behaviour
            </Text>
            <View>
              {this.state.mbehaviour ? (
                <TouchableNativeFeedback
                  onPress={() => this.setState({mbehaviour : false})}>
                  <View style={{borderWidth: 2, borderColor: 'green', backgroundColor: 'green', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'white', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.setState({mbehaviour : true})}>
                  <View style={{borderWidth: 2, borderColor: 'grey', backgroundColor: 'white', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'grey', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              )}
            </View>
          </View>
          <View style={{marginTop: '10%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{fontSize: 15}}>
              Booking Procedure
            </Text>
            <View>
              {this.state.bprocess ? (
                <TouchableNativeFeedback
                  onPress={() => this.setState({bprocess : false})}>
                  <View style={{borderWidth: 2, borderColor: 'green', backgroundColor: 'green', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'white', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.setState({bprocess : true})}>
                  <View style={{borderWidth: 2, borderColor: 'grey', backgroundColor: 'white', borderRadius: 5}}>
                    <Icon name='md-checkmark' size={15} style={{color: 'grey', fontWeight: '500', padding: 3}}></Icon>
                  </View>
                </TouchableNativeFeedback>
              )}
            </View>
          </View>
          </View>
          <View style={{marginTop: '10%'}}>
            <Text>Feel free to add a comment<Text style={{fontStyle: 'italic'}}>(optional)</Text></Text>
            <TextInput 
              allowFontScaling={false}
              underlineColorAndroid='transparent'
              style={styles.input}
              placeholderTextColor={'#878a91'}
              onChangeText = {(text) => this.setState({comment : text})}
              // returnKeyType = {"next"}
              // keyboardType = 'numeric'
              value = {this.state.comment}>
            </TextInput>
          </View>
        </View>
        <View style={{alignItems : 'center', bottom: 0, width: '100%', marginTop: 20}}>
          <TouchableNativeFeedback
            onPress={this.addReview}
            disabled={this.state.rating > 0 ? false : true}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
              <View style={{ alignItems: 'center'}}>
                <Text style={styles.book}>  Done  </Text>
              </View>
            </LinearGradient>
          </TouchableNativeFeedback>
        </View> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
    // borderBottomWidth: 2,
    // borderColor: '#ddd',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
  },
  input: {
    marginTop: 5,
    marginBottom: 3,
    width: '100%',
    height: '40%',
    padding: 10,
    height: 50,
    fontSize: 18,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#4da3e3',
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%',
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 100, 
    // borderBottomWidth: 2,
    // borderRightWidth: 2,
    // borderColor: '#c6c2c2',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
  },
})
