import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import renderIf from '../extra/renderIf';
import Modal from "react-native-modal";
import {TextInput, 
		AsyncStorage, 
		View, 
		Dimensions, 
		ActivityIndicator, 
		TouchableNativeFeedback, 
		KeyboardAvoidingView, 
		Text, 
		Image, 
		Button, 
		ScrollView, 
		Keyboard} from 'react-native';

export default class SysChoice extends Component {
    constructor(props) {
        super(props);
		}

    render() {
			const {navigate} = this.props.navigation;
			const { goBack } = this.props.navigation;
      let item = this.props.navigation.getParam('item');
		return(
			<View style={styles.container}>
				<View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
            <View style={{marginLeft: '3%'}}>
              <View style={{flexDirection: 'row'}}>
                <TouchableNativeFeedback
									onPress={() => goBack()}>
                  <View style={{paddingLeft: 8, paddingRight: 5, paddingTop: 5}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
								<View style={{marginLeft: '3%', padding: 8}}>
									<Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>PC or Console</Text>
								</View>
              </View>
            </View>
						<View></View>
          </View>
				<View>
					<TouchableNativeFeedback
						onPress={() => navigate('Book', {item : item, choice : "PC"})}>
						<View style={{marginLeft: 20}}>
							<Image
								source={require('../assets/gameonPC.png')}
								style={styles.image}>
							</Image>
						</View>
					</TouchableNativeFeedback>
					<TouchableNativeFeedback
						onPress={() => navigate('Book', {item : item, choice : "Console"})}>
						<View style={{marginRight: 20}}>
							<Image
								source={require('../assets/gameonConsole.png')}
								style={styles.image2}>
							</Image>
						</View>
					</TouchableNativeFeedback>
				</View>
			</View>
        );
    }
}

const styles = StyleSheet.create({
container: {
		flex: 11,
		backgroundColor: 'white',
},
headerStyle : {
		backgroundColor: 'white',
},
image: {
	alignSelf: 'center',
	width: '40%', 
	height: '40%',
	marginTop: '20%',
},
image2: {
	alignSelf: 'center',
	width: '40%', 
	height: '40%',
	marginTop: '10%',
	marginLeft: '15%'
},
});