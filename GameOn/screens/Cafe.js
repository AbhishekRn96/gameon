import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/Entypo';
import MapView, { Marker } from 'react-native-maps';
import moment from 'moment';
import getDirections from 'react-native-google-maps-directions';
import {TextInput,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        InteractionManager, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button,
        Dimensions,
        ScrollView, 
        Keyboard,
        StatusBar,
        BackHandler} from 'react-native';

export default class Cafe extends Component {
    constructor(props) {
        super(props);
        this.state = {
          didFinishInitialAnimation: false,
          readmore: false,
        }
        this.handleGetDirections = this.handleGetDirections.bind(this);
        this.Navigate = this.Navigate.bind(this);
        //this.Slides();
        this.Slides = this.Slides.bind(this);
    }

    componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
        this.setState({
          didFinishInitialAnimation: true,
        });
      });
      // f => {
      //   // ensure f get called, timeout at 500ms
      //   // @gre workaround https://github.com/facebook/react-native/issues/8624
      //   let called = false;
      //   const timeout = setTimeout(() => { called = true; f() }, 500);
      //   InteractionManager.runAfterInteractions(() => {
      //     if (called) return;
      //     clearTimeout(timeout);
      //     f();
      //   });
      // }
    }

    Navigate() {
      const item = this.props.navigation.getParam('item');
      if(item.PCs == null || item.PCs == 0) {
        this.props.navigation.navigate('Book', {item : item, choice : "Console"});
      } else if(item.Consoles == null || item.Consoles == 0) {
        this.props.navigation.navigate('Book', {item : item, choice : "PC"});
      } else {
        this.props.navigation.navigate('SysChoice', {item : item});
      }
    }

    Slides = () => {
      let item = this.props.navigation.getParam('item');
      let swipe = [];
      for(let i=0;i<item.Games.length;i++) {
        swipe.push(
          <View style={styles.item} key={i}>
            <Image
              source={{uri : item.Games[i].url}}
              style={styles.image2}>
            </Image>
          </View>
        )
      }
      return swipe;
    } 

    handleGetDirections() {
      const location = this.props.navigation.getParam('location');
      const item = this.props.navigation.getParam('item');
      let data;
      if(location) {
         data = {
           source: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude
          },
          destination: {
            latitude: this.state.latitude,
            longitude: this.state.longitude
          },
          params: [
            {
              key: "travelmode",
              value: "driving"        // may be "walking", "bicycling" or "transit" as well
            }
          ]
        }
      }
      else {
         data = {
          //  source: {
          //   latitude: 12.812129,
          //   longitude: 104.913913
          // },
          destination: {
            latitude: item.Latitude,
            longitude: item.Longitude
          },
          params: [
            {
              key: "travelmode",
              value: "driving"        // may be "walking", "bicycling" or "transit" as well
            }
          ]
        }
      }
      getDirections(data)
    }

    render() {
      const {navigate} = this.props.navigation;
      const { goBack } = this.props.navigation;
      const {navigation} = this.props;
      const item = navigation.getParam('item');
      // const name = navigation.getParam('name');
      // const location = navigation.getParam('location');
      // const desc = navigation.getParam('desc')
      let time = Number(moment(new Date()).format('HH'));
      return(
        this.state.didFinishInitialAnimation == false ? (
          <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={styles.container}>
              <ActivityIndicator />
              <StatusBar
                backgroundColor="white"
                barStyle="dark-content"
              />
            </View>
          </View>
        ) : (
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', top: 0}]}>
            <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <TouchableNativeFeedback
								  onPress={() => goBack()}>
                  <View style={{padding: 10, paddingTop: 10, paddingRight: 10}}>
                    <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                  </View>
                </TouchableNativeFeedback>
              </View>
              <View style={{padding: 10}}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>About</Text>
              </View>
            </View>
            <View style={{marginRight: '4%'}}>
              <TouchableNativeFeedback
                onPress={this.Navigate}>
                <View style={styles.edit}>
                  <Text style={styles.book}>  BOOK  </Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <ScrollView style={styles.carousal} horizontal={true} showsHorizontalScrollIndicator={false}>
                {item.Slides.map((data, key) => (
                  <View style={styles.item} key={key}>
                    <Image
                      source={{uri : data}}
                      style={styles.image}>
                    </Image>
                  </View>
                ))}
              </ScrollView>
            </View>
            <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
              <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>{item.Name}</Text>
            </View>
            <View style={{width: '90%', alignSelf: 'center', marginTop: 10, flexDirection: 'row', marginLeft: 10}}>
              {item.Rating == 5 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 5 && item.Rating >= 4.5 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 4.5 && item.Rating >= 4 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 4 && item.Rating >= 3.5 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 3.5 && item.Rating >= 3 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 3 && item.Rating >= 2.5 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 2.5 && item.Rating >= 2 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 2 && item.Rating >= 1.5 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : item.Rating < 1.5 && item.Rating >= 1 ? (
                  <View style={{flexDirection: 'row'}}>
                    <Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
                  </View>
              ) : null}
            </View>
            <View style={{width: '90%', alignSelf: 'center', marginTop: 10, flexDirection: 'row'}}>
              <Icon4 name='location-pin' color={'#f21f1f'} size={19}></Icon4>
              <Text style={[styles.content, {marginLeft: 3}]}>{item.Location}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'flex-start', width: '90%', marginTop: 5, alignSelf: 'center'}}>
              <Icon3 name='clock-o' color={'#799e9e'} size={19}></Icon3>
              <Text style={[styles.content, {marginLeft: 3}]}>
                {item.OpenTime <= time && time <= item.CloseTime ? (
                  time > (item.CloseTime-2) ? (
                    <Text style={{color: '#a3a31a'}}>Closes Soon</Text>
                  ) : (
                    <Text style={{color: 'green'}}>Open now</Text>
                  )
                ) : (
                  <Text style={{color: 'red'}}>Closed</Text>
                )}
              </Text>
              <Text style={styles.content}> - {item.OpenTime%1 == 0.5 ? (item.OpenTime-0.5+':30') : (item.OpenTime+':00')} - {item.CloseTime%1 == 0.5 ? (item.CloseTime-0.5+':30') : (item.CloseTime+':00')}</Text>
            </View>
            <View style={{padding: 10, borderBottomWidth: 1, borderColor: '#ccccc5', width: '90%',  alignSelf: 'center'}}></View>
            <View style={{marginTop: 10}}>
              <View style={{width: '90%', alignSelf: 'center'}}>
                {this.state.readmore ? (
                <Text style={styles.content}>{item.Desc}   <Text style={{color: '#1C8ADB', fontWeight: '400'}} onPress={() => this.setState({readmore: false})}>Read less</Text></Text>
                ) : (
                  <Text style={styles.content}>{item.Desc.substr(0, 200)}...<Text style={{color: '#1C8ADB', fontWeight: '400'}} onPress={() => this.setState({readmore: true})}>Read more</Text></Text>
                  )}
              </View>
            <View style={{padding: 10, borderBottomWidth: 1, borderColor: '#ccccc5', width: '90%',  alignSelf: 'center'}}></View>
            <View style={{width: '90%', alignSelf: 'center'}}>
              <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>Games</Text>
              <View style={{marginTop: 16}}>
                <ScrollView style={styles.carousal} horizontal={true} showsHorizontalScrollIndicator={false}>
                  {item.Games.map((data, key) => (
                    <View style={[styles.item, {marginLeft: 5}]} key={key}>
                      <Image
                        source={{uri : data.url}}
                        style={styles.image2}>
                      </Image>
                    </View>
                  ))}
                </ScrollView>
              </View>
            </View>
            <View style={{padding: 10, borderBottomWidth: 1, borderColor: '#ccccc5', width: '90%',  alignSelf: 'center'}}></View>
              <View style={{width: '90%', alignSelf: 'center', marginTop: 10}}>
                <Text style={styles.content}><Text style={{color: '#5a5a5e', fontWeight: '400'}}>Contact</Text> - {item.Contact.Contact_1}</Text>
              </View>
              {/* <View style={{width: '90%', marginLeft: 10, marginTop: 3}}>
                <Text style={styles.desc}>Games</Text>
              </View>
              <View style={{width: '90%', marginLeft: 10, marginTop: 3}}>
                  <View>
                  {item.Games.map((data, key) => (
                    <View style={styles.item} key={key}>
                      <Image
                        source={{uri : data.url}}
                        style={styles.image2}>
                      </Image>
                    </View>
                  ))}
                  </View>
              </View> */}
              <View style={{alignItems: 'flex-start', width: '90%', marginTop: 10, alignSelf: 'center'}}>
                <Text style={styles.content}><Text style={{color: '#5a5a5e', fontWeight: '400'}}>Price</Text> - {item.PCs == null || item.PCs == 0 ? (<Text>Console : {'\u20B9'}{item.CostConsole}</Text>) : (<Text>PC : {'\u20B9'}{item.CostPC}</Text>)} | {item.Consoles == null || item.Consoles == 0 ? (<Text>PC : {'\u20B9'}{item.CostPC}</Text>) : (<Text>Console : {'\u20B9'}{item.CostConsole}</Text>)}</Text>
              </View>
            </View>
            <View style={{padding: 10, borderBottomWidth: 1, borderColor: '#ccccc5', width: '90%',  alignSelf: 'center'}}></View>
            <View style={{marginTop: 10, marginBottom: 10, width: '90%', alignSelf: 'center' }}>
              <Text style={{fontSize: 20, fontWeight: '400', color: '#5a5a5e'}}>Location</Text>
              <MapView
                style={[{height: 250, marginTop: 16}]}
                initialRegion={{
                  latitude: item.Latitude,
                  longitude: item.Longitude,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}>
                  <Marker 
                    coordinate={{latitude: item.Latitude,
                      longitude: item.Longitude}}
                    title={item.Name}
                    >
                  </Marker>
              </MapView>
            </View>
            <View style={{alignItems: 'center', marginTop: 10, marginBottom: 10}}>
              <TouchableNativeFeedback
                onPress={this.handleGetDirections}>
                <View style={styles.button}>
                  <Text style={styles.buttontext}>  Get Directions  </Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </ScrollView>
        </View>
        )
      )
    }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  carousal: {
  backgroundColor: 'white'
    //width: Dimensions.get("window").width,
  },
  map : {
    left: 0,
    right: 0,
    position: 'absolute'
  },
  item: {
    alignItems: 'center',
  },
  content: {
    color: '#5a5c5e', 
    fontWeight: '300', 
    flexWrap: 'wrap',
    fontSize: 14
  },
  button: {
    padding: 10,
    backgroundColor: '#0e8c0c',
    width: '90%',
    alignItems: 'center',
    height: 50,
    borderRadius: 3,
  },
  buttontext: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  image: {
    width: Dimensions.get("window").width, 
    height: 230,
  },
  image2: {
    width: 45, 
    height: 45,
  },
  edit: {
		backgroundColor: 'white',
    borderRadius: 10,
		alignItems: 'center',
    borderColor: 'white',
		//marginBottom: '5%',
		//marginRight: 5,
  },
  book: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 15,
    padding: '2%'
  },
  title: {
    marginLeft: 10,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#1C8ADB',
  },
  desc: {
    fontWeight: '400',
    color: '#5a5c5e',
    fontSize: 20,
  },
  address: {
    fontWeight: '600',
    color: '#1C8ADB',
    marginLeft: 5,
    marginTop: 10
  },
})