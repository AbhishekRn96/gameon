import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import Icon5 from 'react-native-vector-icons/MaterialIcons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Entypo';
import config from "../extra/config";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SplashScreen from 'react-native-splash-screen';
import moment from 'moment';
import {FlatList,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        InteractionManager, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button,
        Dimensions,
        ScrollView, 
        Keyboard,
        StatusBar,
        BackHandler} from 'react-native';

export default class Tournaments extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data : [],
          failed : false,
          refreshing : false,
        }
        this.getTournaments = this.getTournaments.bind(this);
        this.SortArray = this.SortArray.bind(this);
    }

    componentWillMount() {
      SplashScreen.hide();
      this.getTournaments();
    }

    componentDidUpdate() {
      console.log("updated in Tournaments");
    }

    async getTournaments() {
      this.setState({data : []});
      try {
				let response = await fetch(`${config.userurl}getTournaments`);
				let responseJson = await response.json();
				if(responseJson.length > 0) {
          responseJson.map((item, key) => {
            item.DateString = moment(new Date(item.Date)).format('MMMM Do YYYY');
            let date1 = new Date();
            let date2 = new Date(item.Date);
            date1.setHours(date1.getHours()+Number(item.End));
            date2.setHours(Number(item.Hours), Number(item.Minutes));
            date1 = new Date(date1);
            date2 = new Date(date2);
            if(item.TournamentStatus === "Open" && date1 >= date2) {
              item.TournamentStatus = "Closed";
            }
          })
					this.setState({
            data: responseJson,
            refreshing : false,
					})
				} else if(responseJson.length == 0) {
					this.setState({failed : true, refreshing : false})
				} 
			} catch(err) {
          console.log("tour ,", err);
          this.setState({refreshing : false});
			}
    }

		SortArray(value) {
      let arr = this.state.data;
      this.setState({data : []});
      if(value === "by Date") {
        arr.sort((a, b) => (a.Date > b.Date) ? 1 : ((b.Date > a.Date) ? -1 : 0));
        this.setState({data : arr});
      } else if(value === "by Price") {
        arr.sort((a, b) => (a.Fee > b.Fee) ? 1 : ((b.Fee > a.Fee) ? -1 : 0));
        this.setState({data : arr});
      }
    }
    
    handleRefresh() {
      this.setState({refreshing : true}, () => this.getTournaments());
    }

    render() {
			const {navigate} = this.props.navigation;
      return(
          <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
              <Image 
                source={require('../images/NewGO.png')}
                style={{height: 31, width: 100, marginLeft: '5%'}}>
              </Image>
              <View style={{marginRight: '5%', marginTop:'4%'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  {/* <TouchableNativeFeedback
                    onPress={() => this.setState({choices : true})}>
                    <Icon5 name='sort' color={'#1C8ADB'} size={24}></Icon5>
                  </TouchableNativeFeedback> */}
                  <Dropdown data={[{value : 'by Date'}, {value : 'by Price'}]}
                    containerStyle={{ width: 40, height: 40 }}
                    pickerStyle={{
                      width: '30%',
                      left: null,
                      right: 0,
                      marginRight: 10,
                      marginTop: 25
                    }}
                    itemTextStyle={{fontSize: 18}}
                    shadeOpacity = {0}
                    dropdownPosition={-2}
                    onChangeText = {(value) => this.SortArray(value)}
                    renderBase={() => <Icon5 name='sort' color={'#1C8ADB'} size={26}></Icon5>}
                  />
                </View>
              </View>
            </View>
              {this.state.data.length > 0 ? (
                <FlatList
                  data={this.state.data}
                  showsVerticalScrollIndicator={false}
                  renderItem={({item}) =>
                  <TouchableNativeFeedback
                    disabled={item.TournamentStatus === "Closed" ? true : false}
                    onPress={() => navigate('ViewTour', {item: item})}>
                    <View style={styles.container}>
                      <View>
                        <View style={{alignItems: 'center'}}>
                          <View style={styles.shadow}>
                            <Image
                              source={{uri: item.Poster}}
                              style={{height: hp('27%'), borderRadius: 3}}>
                            </Image>
                          </View>
                        </View>
                        <View style={{marginLeft: '6%', padding: 5}}>
                          <View style={{marginTop: 7, width: '90%'}}>
                            <View>
                              <Text style={styles.CafeName}>{item.Name}</Text>
                            </View>
                          </View>
                          <View style={{marginTop: 7, width: '90%'}}>
                            <View>
                              <Text style={[styles.CafeName, {color: 'green'}]}>{'\u20B9'}{item.Fee}</Text>
                            </View>
                          </View>
                          <View style={{flexDirection: 'row', marginTop: '3%', justifyContent: 'space-between', width: '90%'}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon3 name='calendar-o' color={'#28B463'} size={20}></Icon3>
                              <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>{item.DateString}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                              <Icon3 name='clock-o' color={'#799e9e'} size={20}></Icon3>
                              <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>{item.Hours}:{item.Minutes}</Text>
                            </View>
                          </View>
                          <View style={{flexDirection: 'row', marginTop: '2%', justifyContent: 'space-between', width: '90%'}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name='medal' color={'#FFC300'} size={20}></Icon>
                              <Text style={{color: '#575859', marginLeft: 5, fontSize: 16}}>Winning Prize : <Text style={[styles.CafeName, {color: 'green'}]}>{'\u20B9'}{item.WinAmount}</Text></Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                              {item.TournamentStatus === "Closed" ? (
                                <Text style={{color: 'red', marginLeft: 5, fontSize: 16, fontWeight: '500'}}>Closed</Text>
                              ) : (
                                <Text style={{color: 'green', marginLeft: 5, fontSize: 16, fontWeight: '500'}}>Open</Text>
                              )}
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableNativeFeedback>
                  }
                  keyExtractor={item => item._id}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh.bind(this)}
                  style={{width : '96.5%', alignSelf: 'center'}}>
                </FlatList>) :
                (
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{width : '96.5%', alignSelf: 'center'}}>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '85%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '85%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '85%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '85%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '85%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                  <View style={styles.container}>
                    <View>
                      <View style={styles.skeletonImage}></View>
                      <View style={{marginLeft: 10, padding: 7}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                          <View style={{width: '90%', height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
                        </View>
                        <View style={{width: '50%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                        <View style={{width: '45%', height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
                      </View>
                    </View>
                  </View>
                </View>
              </ScrollView>
						)}
          </View>
      )
    }
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
		marginTop: 7,
		marginBottom : '7%',
	},
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
	CafeName: {
		fontSize: 18,
    color: '#3a3b3d',
    fontWeight: '500'
	},
	bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
	headerStyle: {
		backgroundColor: 'white',
	},
	headerStyleOptions: {
		height: '10.5%',
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	animatedView: {
		width : '80%',
		backgroundColor: "#595959",
		elevation: 2,
		position: "absolute",
		bottom: 0,
		padding: 10,
		opacity : 0.9,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row",
		borderRadius: 100,
	},
	exitTitleText: {
		textAlign: "center",
		color: "#ffffff",
		marginRight: 10,
	},
	skeletonImage : {
		height: hp('27%'),
		width: '90%', 
		borderRadius: 5, 
		backgroundColor: '#dee4e8',
		alignSelf: 'center'
	}
})