import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/Entypo';
import Icon5 from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Modal from "react-native-modal";
import config from "../extra/config";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import SplashScreen from 'react-native-splash-screen';
import { Dropdown } from 'react-native-material-dropdown';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {TextInput,
				AsyncStorage,
				View, 
				ActivityIndicator, 
				TouchableNativeFeedback, 
				Text, 
				Image, 
				ScrollView, 
				Keyboard,
				FlatList,
				BackHandler,
				Animated,
				Dimensions,
				Alert} from 'react-native';

let {height} = Dimensions.get('window');

export default class Home extends Component {
    constructor (props) {
        super(props);
        this.state = {
						data: [],
						refreshing: false,
						backClickCount: 0,
						show : false,
						location : null,
						//address : null,
						ok : false,
						choices : false,
						failed : false,
				}
				this.listLoad = this.listLoad.bind(this);
				this.springValue = new Animated.Value(100);
				this.SortArray = this.SortArray.bind(this);
				this.Cancel = this.Cancel.bind(this);
				this.setLocation = this.setLocation.bind(this);
				this.listLoadWithLocation = this.listLoadWithLocation.bind(this);
				this.geolocation = this.geolocation.bind(this);
    }

    componentDidMount() {
		// 	LocationServicesDialogBox.checkLocationServicesIsEnabled({
		// 		message: "GameOn requires your location to show you the nearest cafes to provide you the best experience<br/><br/>Use GPS for location<br/><br/><a href='#'>Learn more</a>",
		// 		ok: "YES",
		// 		cancel: "NO",
		// 		enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
		// 		showDialog: true, // false => Opens the Location access page directly
		// 		openLocationServices: true, // false => Directly catch method is called if location services are turned off
		// 		preventOutSideTouch: true, //true => To prevent the location services popup from closing when it is clicked outside
		// 		preventBackClick: true, //true => To prevent the location services popup from closing when it is clicked back button
		// 		providerListener: false, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
		// }).then(function(success) {
		// 		// success => {alreadyEnabled: true, enabled: true, status: "enabled"} 
		// 				navigator.geolocation.getCurrentPosition((position) => {
		// 						this.setLocation(position);
		// 				}, (error) => this.Cancel(error), { enableHighAccuracy: true, timeout: 40000, maximumAge: 1000 });
		// 		}.bind(this)
		// ).catch((error) => {
		// 		console.log(error.message);
		// 		this.Cancel(error);
		// });
		SplashScreen.hide();
		this.listLoad();
		}

		setLocation(position) {
			this.setState({location : position})
			this.listLoadWithLocation();
		}

		geolocation() {
			LocationServicesDialogBox.checkLocationServicesIsEnabled({
				message: "GameOn requires your location to show you the nearest cafes to provide you the best experience<br/><br/>Use GPS for location<br/><br/><a href='#'>Learn more</a>",
				ok: "YES",
				cancel: "NO",
				enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
				showDialog: true, // false => Opens the Location access page directly
				openLocationServices: true, // false => Directly catch method is called if location services are turned off
				preventOutSideTouch: true, //true => To prevent the location services popup from closing when it is clicked outside
				preventBackClick: true, //true => To prevent the location services popup from closing when it is clicked back button
				providerListener: false, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
			}).then(function(success) {
					// success => {alreadyEnabled: true, enabled: true, status: "enabled"} 
							navigator.geolocation.getCurrentPosition((position) => {
									this.setLocation(position);
							}, (error) => this.Cancel(error), { enableHighAccuracy: true, timeout: 40000, maximumAge: 1000 });
					}.bind(this)
			).catch((error) => {
					console.log(error.message);
					this.Cancel(error);
			});
		}

		Cancel(error) {
			console.log(error)
			this.listLoad();
		}

    async listLoad() {
      try {
				let response = await fetch(`${config.userurl}getCafes`);
				let responseJson = await response.json();
				if(responseJson.length > 0) {
					this.setState({
						data: responseJson,
					})
				} else if(responseJson.length == 0) {
					this.setState({failed : true})
				}
			} catch(err) {
					console.log(err);
			}
		}

		distance(lat1, lon1, lat2, lon2) {
			var p = 0.017453292519943295;    // Math.PI / 180
			var c = Math.cos;
			var a = 0.5 - c((lat2 - lat1) * p)/2 + 
							c(lat1 * p) * c(lat2 * p) * 
							(1 - c((lon2 - lon1) * p))/2;
		
			return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
		}

		async listLoadWithLocation() {
      try {
				let response = await fetch(`${config.userurl}getCafes`);
				let responseJson = await response.json();
				console.log(responseJson);
				if(responseJson.length > 0) {
					let arr = responseJson;
					for (let i=0; i<arr.length; i++) {
						kms = this.distance(this.state.location.coords.latitude, this.state.location.coords.longitude, arr[i].Latitude, arr[i].Longitude);
						arr[i].Distance = Math.round( kms * 10 ) / 10
					}
				console.log(arr);
				arr.sort((a,b) => {
					return Number(a.Distance) - Number(b.Distance)
				})
				this.setState({data : arr})
				} else if(responseJson.length == 0) {
					this.setState({failed : true})
				}
				//console.log(this.state.data);
			} catch(err) {
					console.log("error", err); 
			}
		}

		SortArray(value) {
			if(value == 'Rating') {
				let arr = this.state.data;
				this.setState({data : []})
				arr.sort((a,b) => {
					return Number(a.Rating) - Number(b.Rating)
				})
				arr.reverse();
				this.setState({data : arr, choices: false})
			}
			else if(value == 'Shuffle') {
				let arr = this.state.data;
				this.setState({data : []})
				var currentIndex = arr.length, temporaryValue, randomIndex;
				while (0 !== currentIndex) {
					randomIndex = Math.floor(Math.random() * currentIndex);
					currentIndex -= 1;
					temporaryValue = arr[currentIndex];
					arr[currentIndex] = arr[randomIndex];
					arr[randomIndex] = temporaryValue;
				}
				this.setState({data : arr, choices: false})
			}
			else if(value == 'Distance') {
				if(this.state.location) {
					let arr = this.state.data;
					this.setState({data : []})
					arr.sort((a,b) => {
						return Number(a.Distance) - Number(b.Distance)
					})
					this.setState({data : arr, choices: false})
				}
				else{
					this.setState({data : [], show : true})
					this.geolocation();
				}
			}
		}

    render() {
			const {navigate} = this.props.navigation;
        return(
				<View style={{flex: 1, backgroundColor: 'white', flexDirection: 'column'}}>
				<View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
					<View style={{marginLeft: '4%', flexDirection: 'row'}}>
						<View>
							<Image 
								source={require('../assets/location.png')}
								style={{height: 31, width: 21}}>
							</Image>
						</View>
						<View style={{marginLeft: '8%'}}>
							<Text style={{fontSize: 18, fontWeight: '400', color: '#1C8ADB'}}>Bangalore</Text>
						</View>
					</View>
					<View style={{marginRight: '5%', marginTop:'2%'}}>
						<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
							{/* <TouchableNativeFeedback
								onPress={() => this.setState({choices : true})}>
								<Icon5 name='sort' color={'#1C8ADB'} size={24}></Icon5>
							</TouchableNativeFeedback> */}
							<Dropdown data={[{value : 'Distance'}, {value : 'Rating'}]}
								containerStyle={{ width: 40, height: 40 }}
								pickerStyle={{
									width: '30%',
									left: null,
									right: 0,
									marginRight: 10,
									marginTop: 25
								}}
								itemTextStyle={{fontSize: 18}}
								shadeOpacity = {0}
								dropdownPosition={-2}
								onChangeText = {(value) => this.SortArray(value)}
								renderBase={() => <Icon5 name='sort' color={'#1C8ADB'} size={26}></Icon5>}
							/>
						</View>
					</View>
				</View>
					{this.state.failed ? (
						<View style={{alignItems: 'center', marginTop: '50%'}}>
							<Text style={{padding: 10, color: 'black'}}>There are no Cafes to show</Text>
						</View>
					) : this.state.data.length > 0 ? (
						<FlatList
							data={this.state.data}
							showsVerticalScrollIndicator={false}
							renderItem={({item}) =>
							<TouchableNativeFeedback
							 onPress={() => navigate('Cafe', {item: item, location : this.state.location})}>
								<View style={styles.container}>
									<View>
										<View style={{alignItems: 'center'}}>
											<View style={styles.shadow}>
												<Image
													source={{uri: item.Main}}
													style={{height: hp('27%'), borderRadius: 3}}>
												</Image>
											</View>
										</View>
										<View style={{marginLeft: '6%', padding: 5}}>
											<View>
												<Text style={styles.CafeName}>{item.Name}</Text>
											</View>
											<View style={{flexDirection: 'row', marginTop: 7}}> 
												<Icon3 name='clock-o' color={'#799e9e'} size={19}></Icon3>
												<Text style={{color: '#575859', marginLeft: 5}}>{item.OpenTime%1 == 0.5 ? (item.OpenTime-0.5+':30') : (item.OpenTime+':00')} - {item.CloseTime%1 == 0.5 ? (item.CloseTime-0.5+':30') : (item.CloseTime+':00')} - </Text>
												<Text>
													{item.OpenTime <= Number(moment(new Date()).format('HH')) && Number(moment(new Date()).format('HH')) <= item.CloseTime ? (
														Number(moment(new Date()).format('HH')) > (item.CloseTime-2) ? (
															<Text style={{color: '#a3a31a'}}>Closes Soon</Text>
														) : (
															<Text style={{color: 'green'}}>Open now</Text>
														)
													) : (
														<Text style={{color: 'red'}}>Closed</Text>
													)}
												</Text>
											</View>
											<View style={{flexDirection: 'row', marginTop: 7, justifyContent: 'space-between'}}>
												<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
													<View style={{flexDirection: 'row'}}>
														<Icon4 name='location-pin' color={'#f21f1f'} size={19}></Icon4>
														<Text style={{color: '#575859', marginLeft: 5}}>{item.Location}</Text>
													</View>
													{item.Distance ? (
														<View>
															<Text style={{color: '#575859', marginRight: 5}}>{item.Distance} kms away</Text>
														</View>
													) : (
														<View></View>
													)}
												</View>
												<View style={{marginRight: '2%'}}>
													{item.Rating == 5 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 5 && item.Rating >= 4.5 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 4.5 && item.Rating >= 4 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 4 && item.Rating >= 3.5 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 3.5 && item.Rating >= 3 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 3 && item.Rating >= 2.5 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 2.5 && item.Rating >= 2 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star-half' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 2 && item.Rating >= 1.5 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : item.Rating < 1.5 && item.Rating >= 1 ? (
															<View style={{flexDirection: 'row'}}>
																<Icon3 name='star' color={'#1C8ADB'} size={13}></Icon3>
															</View>
													) : null}
												</View>
											</View>
										</View>
									</View>
								</View>
							</TouchableNativeFeedback>
							}
							keyExtractor={item => item._id}
							refreshing={this.state.refreshing}
							style={{width : '96.5%', alignSelf: 'center'}}>
						</FlatList>) :
						(
					<ScrollView showsVerticalScrollIndicator={false}>
						<View style={{width : '96.5%', alignSelf: 'center'}}>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
								<View style={styles.container}>
									<View>
										<View style={styles.skeletonImage}></View>
										<View style={{marginLeft: 10, padding: 7}}>
											<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
												<View style={{width: 250, height: 20, backgroundColor: '#dee4e8', borderRadius: 100}}></View>
											</View>
											<View style={{width: 150, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
											<View style={{width: 125, height: 20, backgroundColor: '#dee4e8', marginTop: 7, borderRadius: 100}}></View>
										</View>
									</View>
								</View>
							</View>
					</ScrollView>
						)}
				</View>
        )
    }
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
		marginTop: 7,
		marginBottom : 5,
	},
	shadow: { 
		width: '90%',
		backgroundColor: '#ddd',
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 12 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
		elevation: 15,
	},
	CafeName: {
		fontSize: 18,
		color: '#3a3b3d',
	},
	bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
	headerStyle: {
		backgroundColor: 'white',
	},
	headerStyleOptions: {
		height: '10.5%',
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	animatedView: {
		width : '80%',
		backgroundColor: "#595959",
		elevation: 2,
		position: "absolute",
		bottom: 0,
		padding: 10,
		opacity : 0.9,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row",
		borderRadius: 100,
	},
	exitTitleText: {
		textAlign: "center",
		color: "#ffffff",
		marginRight: 10,
	},
	skeletonImage : {
		height: hp('27%'),
		width: '90%', 
		borderRadius: 5, 
		backgroundColor: '#dee4e8',
		alignSelf: 'center'
	}
})

//export default connect(mapStatetoProps)(Home)
