import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";
import renderIf from '../extra/renderIf';
import config from "../extra/config";
import RazorpayCheckout from 'react-native-razorpay';
import {configure, localNotif, scheduleNotifDay, scheduleNotifHour} from './Notifications';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {TextInput,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        TouchableNativeFeedback,
        Text,
        Image,
        Dimensions,
        ScrollView,
        BackHandler} from 'react-native';

export default class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Username : null,
      Email : null,
      DP : null,
      selectedpc: 1,
      totalCost : null,
      credit : 0,
      show: false,
      show2: false,
      game: '',
      UserId : null,
      usedCredit : 0,
      SystemCost : 0,
      disable : false,
    }
    this.book = this.book.bind(this);
    this.increase = this.increase.bind(this);
    this.decrease = this.decrease.bind(this);
    this.RazorPay = this.RazorPay.bind(this);
  }

  async componentDidMount() {
    let item = this.props.navigation.getParam('data');
    let tc = (this.state.selectedpc) * item.totalCost;
    await AsyncStorage.multiGet(['userid', 'dp', 'email', 'user', 'gcredits']).then((data) => {
      this.setState({
        Username : data[0][1],
        DP : data[1][1],
        Email : data[2][1],
        totalCost : item.totalCost,
        UserId: data[3][1],
        SystemCost : tc,
        credit : Number(data[4][1]),
      })
      if(Number(data[4][1]) == 0) {
        this.setState({
          totalCost : tc,
          usedCredit : 0,
        })
      } else if(Number(data[4][1]) > tc) {
        this.setState({
          totalCost : 0,
          usedCredit : tc,
        })
      } else if(Number(data[4][1]) == tc) {
        this.setState({
          totalCost : 0,
          usedCredit : tc,
        })
      } else if(Number(data[4][1]) < tc) {
        this.setState({
          totalCost : tc - Number(data[4][1]),
          usedCredit : Number(data[4][1]),
        })
      }
    })
  }

  increase() {
    let data = this.props.navigation.getParam('data');
    if(this.state.selectedpc + 1 <= data.available){
      let tc = (this.state.selectedpc + 1) * data.totalCost;
      this.setState({SystemCost : tc});
      if(this.state.credit == 0) {
        this.setState({
          selectedpc : this.state.selectedpc + 1,
          totalCost : tc,
          usedCredit : 0,
        })
      } else if(this.state.credit < tc) {
        this.setState({
          selectedpc : this.state.selectedpc + 1,
          totalCost : tc - this.state.credit,
          usedCredit : this.state.credit,
        })
      } else if(this.state.credit > tc) {
        this.setState({
          selectedpc : this.state.selectedpc + 1,
          totalCost : 0,
          usedCredit : tc, 
        })
      } else if(this.state.credit == tc) {
        this.setState({
          selectedpc : this.state.selectedpc + 1,
          totalCost : 0,
          usedCredit : tc,
        })
      }
    }
  }

  decrease() {
    let data = this.props.navigation.getParam('data');
    if(this.state.selectedpc - 1 > 0){
      let tc = (this.state.selectedpc - 1) * data.totalCost;
      this.setState({SystemCost : tc});
      if(this.state.credit == 0) {
        this.setState({
          selectedpc : this.state.selectedpc - 1,
          totalCost : tc,
          usedCredit : 0,
        })
      } else if(this.state.credit < tc) {
        this.setState({
          selectedpc : this.state.selectedpc - 1,
          totalCost : tc - this.state.credit,
          usedCredit : this.state.credit,
        })
      } else if(this.state.credit > tc) {
        this.setState({
          selectedpc : this.state.selectedpc - 1,
          totalCost : 0,
          usedCredit : tc, 
        })
      } else if(this.state.credit == tc) {
        this.setState({
          selectedpc : this.state.selectedpc - 1,
          totalCost : 0,
          usedCredit : tc, 
        })
      }
    }
  }

  RazorPay() {
    this.setState({disable : true})
    let item = this.props.navigation.getParam('item');
    let notif = this.props.navigation.getParam('data');
    var options = {
      description: item.Name,
      image: 'https://s3.ap-south-1.amazonaws.com/gameon-cafe-images/icon.png',
      currency: 'INR',
      key: 'rzp_test_t5wTJXwdktpc56',
      amount: this.state.totalCost == 0 ? (this.state.SystemCost*0.1)*100 : (this.state.totalCost + this.state.SystemCost*0.1)*100,
      name: 'GameOn',
      prefill: {
        email: this.state.Email,
      },
      // external: {
      //   wallets: ['paytm']
      // },
      theme: {color: '#1C8ADB'}
    }
    RazorpayCheckout.open(options).then((data) => {
      // handle success
      data.status = "paid";
      this.book(data);
    }).catch((error) => {
      this.setState({disable : false});
      // handle failure
      //alert(`Error: ${error.code} | ${error.description}`);
    });
  }

  async book(payment) {
    this.setState({
      show2 : true,
    })
    let data = this.props.navigation.getParam('data');
    let item = this.props.navigation.getParam('item');
    let choice = this.props.navigation.getParam('choice');
    if(choice == "PC") {
      try {
        let response = await fetch(`${config.bookingurl}bookPC`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Date : data.dateValue,
              DateTime : data.date.getFullYear()+'-'+(data.date.getMonth()+1 == 13 ? '1' : (data.date.getMonth()+1))+'-'+data.date.getDate(),
              User : {UserId : this.state.UserId, Username : this.state.Username, Email : this.state.Email, DP : this.state.DP},
              Cafe : {CafeId : item.CafeId, Cafename : item.Name, CafePic : item.Main},
              TotalSystems : this.state.selectedpc,
              Game : this.state.game,
              AvailablePCs : data.available - this.state.selectedpc,
              StartTime : data.start,
              EndTime : data.end,
              TotalHours : data.totalHrs,
              TotalCost : this.state.totalCost == 0 ? (this.state.SystemCost*0.1) : (this.state.totalCost + this.state.SystemCost*0.1),
              GCreditsUsed : this.state.usedCredit,
              PaymentInfo : payment
            }),
          })
          let responseJson = await response.json();
          if(responseJson[0].BookingId){
            this.setState({
              show2 : false,
              show : true,
            })
            let credit = this.state.credit - this.state.usedCredit;
            await AsyncStorage.setItem('gcredits', credit+'');
            console.log("here",data);
            this.props.navigation.navigate('BookConfirm', {data : responseJson[0], raw : data, item : item, userDate: responseJson[1]});
          }
      } catch (err) { 
        console.log(err.message);
      }
    } else {
      try {
        let response = await fetch(`${config.bookingurl}bookConsole`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Date : data.dateValue,
              DateTime : data.date.getFullYear()+'-'+(data.date.getMonth()+1 == 13 ? '1' : (data.date.getMonth()+1))+'-'+data.date.getDate(),
              User : {UserId : this.state.UserId, Username : this.state.Username, Email : this.state.Email, DP : this.state.DP},
              Cafe : {CafeId : item.CafeId, Cafename : item.Name, CafePic : item.Main},
              TotalSystems : this.state.selectedpc,
              Game : this.state.game,
              AvailableConsoles : data.available - this.state.selectedpc,
              StartTime : data.start,
              EndTime : data.end,
              TotalHours : data.totalHrs,
              TotalCost : this.state.totalCost == 0 ? (this.state.SystemCost*0.1) : (this.state.totalCost + this.state.SystemCost*0.1),
              GCreditsUsed : this.state.usedCredit,
              PaymentInfo : payment
            }),
          })
          let responseJson = await response.json();
          if(responseJson[0].BookingId){
            this.setState({
              show2 : false,
              show : true,
            })
            let credit = this.state.credit - this.state.usedCredit;
            await AsyncStorage.setItem('gcredits', credit+'');
            this.props.navigation.navigate('BookConfirm', {data : responseJson[0], raw : data, item : item, userDate: responseJson[1]});
          }
      } catch (err) { 
        console.log(err.message);
      }
    }
  }

  render() {
    const {navigate} = this.props.navigation;
    const { goBack } = this.props.navigation;
    const data = this.props.navigation.getParam('data');
    const item = this.props.navigation.getParam('item');
    let choice = this.props.navigation.getParam('choice');
    return(
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <TouchableNativeFeedback
								onPress={() => goBack()}>
                <View style={{paddingLeft: 8, paddingRight: 5, paddingTop: 5}}>
                  <Icon name='md-arrow-back' color={'black'} size={28}></Icon>
                </View>
              </TouchableNativeFeedback>
            </View>
            <View style={{marginLeft: '3%', marginTop: '2%'}}>
              <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Booking Details</Text>
            </View>
          </View>
          <View style={{marginRight: '4%'}}>
          </View>
        </View>
        <View style={{alignItems: 'center'}}> 
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.show2}
            backdropOpacity={0.8}
            backdropColor='white'
            style={{alignItems: 'center'}}>
              <ActivityIndicator size={50} color="#1C8ADB" style={{marginTop: '20%'}} />
          </Modal>
        </View>
        <View style={{width : '97%',  alignSelf: 'center',}}>
          <View style={{marginTop: 5, padding: 7}}>
            <Text style={styles.book2}>Select a game:</Text>
            <ScrollView style={styles.carousal} horizontal={true} showsHorizontalScrollIndicator={false}>
              {item.Games.map((data, key) => (
                this.state.game == data.value ? (
                  <View style={[styles.item, {marginLeft: 10, borderRadius: 5, borderWidth: 3, borderColor: 'green'}]} key={key}>
                    <Image
                      source={{uri : data.url}}
                      style={styles.image2}>
                    </Image>
                  </View>
                ) : (
                  <TouchableNativeFeedback
                    onPress={() => this.setState({game : data.value})}>
                    <View style={[styles.item, {marginLeft: 10, borderRadius: 5, borderWidth: 3, borderColor: 'white'}]} key={key}>
                      <Image
                        source={{uri : data.url}}
                        style={styles.image2}>
                      </Image>
                    </View>
                  </TouchableNativeFeedback>
                )
              ))}
            </ScrollView>
          </View>
          <View style={{backgroundColor: 'white', borderRadius: 3, padding: 7, borderColor: '#bec4ce'}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Date</Text>
              <Text style={styles.book2}>{data.dateValue}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Timings</Text>
              <Text style={styles.book2}>{data.start%1 == 0.5 ? (data.start-0.5+':30') : (data.start+':00')} - {data.end%1 == 0.5 ? (data.end-0.5+':30') : (data.end+':00')}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Duration</Text>
              <Text style={styles.book2}>{data.totalHrs%1 == 0.5 ? (data.totalHrs-0.5+' hrs 30 mins') : (data.totalHrs+' hrs')}</Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 15}}>
            <Text style={styles.book2}>{choice == "PC" ? "Available PCs" : "Available Consoles"}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View>
                <Icon style={{marginTop: 5, paddingLeft: 10, paddingRight: 10}} name='md-arrow-dropleft' color={'white'} size={22}></Icon>
              </View>
              <View>
                <Text style={styles.number}>{data.available}</Text>
              </View>
              <View>
                <Icon style={{marginTop: 5, paddingLeft: 10, paddingRight: 10}} name='md-arrow-dropright' color={'white'} size={22}></Icon>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 10}}>
            <Text style={styles.book2}>{choice == "PC" ? "No of PC's" : "No of Console's"}</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableNativeFeedback
                disabled={this.state.disable}
                onPress={this.decrease}>
                <Icon style={{marginTop: 5, paddingLeft: 10, paddingRight: 10}} name='md-arrow-dropleft' color={'#1C8ADB'} size={22}></Icon>
              </TouchableNativeFeedback>
              <View>
                <Text style={styles.number}>{this.state.selectedpc}</Text>
              </View>
              <TouchableNativeFeedback
                disabled={this.state.disable}
                onPress={this.increase}>
                <Icon style={{marginTop: 5, paddingLeft: 10, paddingRight: 10}} name='md-arrow-dropright' color={'#1C8ADB'} size={22}></Icon>
              </TouchableNativeFeedback>
            </View>
          </View>
          <View style={{width: '97%', alignSelf: 'center'}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Systems Cost</Text>
              <Text style={[styles.book2, {color: '#1C8ADB', fontWeight: '500'}]}>&#8377;{this.state.SystemCost}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Convienience</Text>
              <Text style={[styles.book2, {color: '#1C8ADB', fontWeight: '500'}]}>&#8377;{this.state.SystemCost*0.1}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={[styles.book2, {color: '#1C8ADB', fontSize: 18, fontWeight: '500'}]}>Available GCredits</Text>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Image
                    source={require('../images/GCoins.png')}
                    style={{width: 28, height: 23}}>
                  </Image>
                </View>
                <View>
                  <Text style={{color: '#1C8ADB', fontSize: 18, fontWeight: '500'}}>{this.state.credit == 0 || this.state.credit == undefined ? "0" : this.state.credit}</Text>
                </View>
              </View>
            </View>
            <View style={{borderBottomWidth: 2, borderBottomColor: 'grey', marginTop: 10}}></View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
              <Text style={styles.book2}>Total Cost</Text>
              <Text style={[styles.book2, {color: '#1C8ADB', fontWeight: '500'}]}>&#8377;{this.state.totalCost == 0 ? this.state.SystemCost*0.1 : this.state.totalCost + this.state.SystemCost*0.1}</Text>
            </View>
          </View>
            {/* <View style={{alignItems: 'center'}}>
              <TouchableNativeFeedback
                onPress={this.book}>
                <View style={styles.booking}>
                  <Text style={styles.book}>Book</Text>
                </View>
              </TouchableNativeFeedback>
            </View> */}
        </View>
        <View style={{alignItems : 'center', bottom: 0, position: 'absolute', width: '100%'}}>
          {this.state.disable ? (
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
              <View style={{ alignItems: 'center'}}>
                <ActivityIndicator size={22} color="white" style={{padding: 15,}} />
              </View>
            </LinearGradient>
          ) : (
            <TouchableNativeFeedback
              disabled={this.state.disable}
              onPress={this.RazorPay}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
                <View style={{ alignItems: 'center'}}>
                  <Text style={styles.book}>  Book  </Text>
                </View>
              </LinearGradient>
            </TouchableNativeFeedback>
          )}
        </View> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: 'white',
  },
  image: {
    height: hp('35%'), 
    width: '100%', 
  },
  item: {
    alignItems: 'center',
  },
  carousal: {
  backgroundColor: 'white',
  marginTop: 10
    //width: Dimensions.get("window").width,
  },
  image2: {
    width: 45, 
    height: 45,
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(198,198,198,0.3)'
  },
  textInImage: {
    color: 'white', 
    marginTop: '44%',
    fontSize: 25,
    fontWeight: '500',
    flexWrap: 'wrap',
    marginLeft: '2.5%'
  },
  edit: {
		backgroundColor: '#1C8ADB',
		padding: 10,
		width: '30%',
		height: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 100, 
    // borderBottomWidth: 2,
    // borderRightWidth: 2,
    // borderColor: '#c6c2c2',
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // elevation: 2,
  },
  label: {
    fontSize: 20,
    color: '#1C8ADB',
    fontWeight: '500'
  },
  font: {
    fontSize: 18,
    color: 'grey',
    fontWeight: '400',
    padding: 6,
    paddingBottom: 6,
    paddingTop: 6,
    paddingLeft: 6, 
  },
  booking: {
		backgroundColor: '#1C8ADB',
		height: 60,
    alignItems: 'center',
  },
  scaleButtons: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: '#858687'
  },
  scaleButtonsSelected: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: 'white'
  },
  editDate: { 
    backgroundColor: 'white',
		padding: 10,
		width: '60%',
		height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderWidth: 2,
    borderColor: '#1C8ADB',
  },
  picker: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#1C8ADB',
		padding: 10,
		width: '70%',
		height: 50,
    alignItems: 'center',
  },
  pickerContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  pcContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%',
  },
  ok: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    padding: 10,
  },
  number: {
    color: '#1C8ADB',
    fontWeight: '500',
    fontSize: 18,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
  },
  book1: {
    color: '#1C8ADB',
    fontWeight: '400',
    fontSize: 18,
  },
  book2: {
    color: 'grey',
    fontWeight: '400',
    fontSize: 18,
  },
  bookDate: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 10
  },
  bookDay: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 23,
  },
  book3: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 30,
  },
})