import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import {TextInput,
        AsyncStorage, 
        View, 
        ActivityIndicator, 
        InteractionManager, 
        TouchableNativeFeedback, 
        KeyboardAvoidingView, 
        Text, 
        Image, 
        Button,
        Dimensions,
        ScrollView, 
        Keyboard,
        StatusBar,
        BackHandler} from 'react-native';

export default class DashboardTemp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
      return(
          <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
              <Image 
                source={require('../images/NewGO.png')}
                style={{height: 31, width: 100, marginLeft: '5%'}}>
              </Image>
            </View>
            <View style={{alignItems: 'center', marginTop: '50%'}}>
              <Text style={{fontSize: 20, color: 'grey', fontWeight: '500'}}>Coming Soon...</Text>
            </View>
          </View>
      )
    }
}

const styles = StyleSheet.create({
  headerStyle: {
		backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})