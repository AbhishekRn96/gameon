import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import renderIf from '../extra/renderIf';
import Modal from "react-native-modal";
import config from "../extra/config";
import {TextInput, 
		AsyncStorage, 
		View, 
		TouchableOpacity, 
		ActivityIndicator, 
		TouchableNativeFeedback, 
		KeyboardAvoidingView, 
		Text, 
		Image, 
		Button, 
		ScrollView, 
		Keyboard} from 'react-native';

export default class SignUp2 extends Component {
    constructor(props) {
        super(props);
          this.state = {
            user : '',
            pass : '',
						email : '',
						number : '',
						showpassword : true,
						usernameerror: '',
						contacterror: '',
						hint: true,
						show: false,
						};
						this.Signup = this.Signup.bind(this);
						this.matchContact = this.matchContact.bind(this);
						//this.validateEmail = this.validateEmail.bind(this);
        }
		
		componentDidMount() {
			this.setState({showpassword : true})
		}

		async matchContact () {
			this.setState({
				hint : false,
				contacterror: ''
			})
			Keyboard.dismiss();
			try {
				let response = await fetch(`${config.userurl}number`, {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						number: this.state.number,
					}),
			})
				let responseJson = await response.json();
				if(responseJson.message == 'success'){
					this.Signup();
				}
				else {
					this.setState({
						hint : true,
						contacterror: responseJson.message,
						show: true,
					})
					console.log(this.state.contacterror);
				}
			} catch (err) {
				console.log(err);
			}
		}

		async Signup() {
			try {
				let response = await fetch(`${config.userurl}register`, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
    			'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					user: this.state.user,
					pass: this.state.pass,
					email: this.props.navigation.getParam('email').toLowerCase(),
					number: Number(this.state.number),
					dp : 'https://thegameonn.herokuapp.com/dp/avatar.png'
				}),
			})
				let responseJson = await response.json();
				if(responseJson.userid == this.state.user){
					await AsyncStorage.multiSet([
						['userid', responseJson.userid],
                        ['email', responseJson.emailid],
                        ['dp', responseJson.dp]
						])
					this.props.navigation.navigate('App');
				}
		} catch (err) { 
			console.log(err.message);
		}
	}
		componentWillUnmount() {
			this.setState({showpassword : true})
			Keyboard.dismiss();
		}

		changepasswordVis() {
			this.setState({showpassword : !this.state.showpassword})
		}

    render() {
			const {navigate} = this.props.navigation;
			const { goBack } = this.props.navigation;
		return(
			<View style={{flex: 1, flexDirection: 'column'}}>
				<View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
					<View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
						<View>
							<TouchableNativeFeedback>
								<View style={{padding: 10, paddingTop: 15}}>
									<Icon name='md-arrow-back' color={'black'} size={28} onPress={() => goBack()}></Icon>
								</View>
							</TouchableNativeFeedback>
						</View>
						<View style={{padding: 10}}>
							<Text style={{fontSize: 20, fontWeight: '400', color: 'black'}}>Signup 2</Text>
						</View>
					</View>
					<View></View>
				</View>
			<View style={{alignItems: 'center'}}> 
				<Modal
					animationIn = 'fadeIn'
					animationOut = 'fadeOut'
					isVisible={this.state.show}
					onBackdropPress={() => this.setState({show : false})}
					onBackButtonPress={() => this.setState({show : false})}
					style={{alignItems: 'center'}}>
					<View style={{width: '75%', alignItems: 'center', backgroundColor: 'white',  borderTopRightRadius: 10, borderTopLeftRadius: 10}}>
						<Text allowFontScaling={false} style={{padding: 10, fontSize: 15}}>{this.state.contacterror}</Text>
					</View>
					<TouchableNativeFeedback
						onPress={() => this.setState({show : false})}>
						<View style={{width: '75%', alignItems: 'center', backgroundColor: 'white',  borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
							<Text allowFontScaling={false} style={{padding: 10, fontSize: 15}}>OK</Text>
						</View>
					</TouchableNativeFeedback>
				</Modal>
			</View>
      <View style={styles.container}>
				<View style={{alignItems: 'center', marginTop: '3%',}}>
					<Text style={styles.signup}>Sign Up</Text>
				</View>
				<View style={{marginTop: '3%'}}>
					<View style={{alignItems: 'center'}}>
						<Text style={styles.textcontainer}>USERNAME</Text>
					</View>
					<KeyboardAvoidingView behavior='position'>
						<View style={styles.inputcontainer}>
							<TextInput 
								style={styles.inputusername}
								underlineColorAndroid='transparent'
								//placeholder= "Username"
								onChangeText = {(text) => this.setState({user : text})}
								value = {this.state.user}
							>
							</TextInput>
						</View>
						<View style={{alignItems: 'center', marginBottom: 19}}>
							{renderIf(!this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) && this.state.user)(
								<Text style={styles.textcontainerusername}>
									//Should have minimum 6 and maximum 15 characters and should start with an alphabet and can have digits and special characters like -, ., _
								</Text>
							)}
						</View>
					</KeyboardAvoidingView>
					<View style={{alignItems: 'center',}}>
						<Text style={styles.textcontainer}>PASSWORD</Text>
					</View>
					<KeyboardAvoidingView behavior='position'>
						<View style={{alignItems: 'center'}}>
							<View style = { styles.textBoxBtnHolder }>
								<TextInput underlineColorAndroid = "transparent" onChangeText = {(text) => this.setState({pass : text})} secureTextEntry = { this.state.showpassword } style = { styles.textBox }/>
								<TouchableOpacity activeOpacity = { 1 } style = { styles.visibilityBtn } onPress = { this.changepasswordVis.bind(this) }>
									<Image source = { ( this.state.showpassword ) ? require('../images/hide.png') : require('../images/view.png') } style = { styles.btnImage } />
								</TouchableOpacity>
								</View>
							</View>
							<View style={{alignItems: 'center', marginBottom: 19}}>
								{renderIf(!this.state.pass.match(/^[A-Za-z][a-zA-Z0-9_.-@*&$!#%]{5,15}$/) && this.state.pass)(
									<Text style={styles.textcontainerusername}>
										//Minimum 6 characters and should start with an alphabet. Should contain atleast one digit and one special character  
									</Text>
								)}
							</View>
					</KeyboardAvoidingView>
				</View>
					<View>
						<View style={{alignItems: 'center'}}>
					<Text style={styles.textcontainer}>CONTACT NUMBER</Text>
				</View>
				<KeyboardAvoidingView behavior='position'>
					<View style={styles.inputcontainer}>
						<TextInput 
							keyboardType = 'numeric'
							style={styles.input}
							underlineColorAndroid='transparent'
							//placeholder= "Email"
							onChangeText = {(text) => this.setState({number : text})}
							value = {this.state.number}
						>
						</TextInput>
					</View>
					<View style={{alignItems: 'center', marginBottom: 19}}>
						{renderIf(!this.state.number.match(/^[1-9][0-9]{9}$/) && this.state.number)(
							<Text style={styles.textcontainerusername}>
								//Contact number should be of 10 digits
							</Text>
						)}
						</View>
					</KeyboardAvoidingView>
					<View style={{alignItems: 'center', marginTop: 5, marginBottom: 10}}>
					<Text style={{ justifyContent: 'center', width: '88%',color: '#525452',paddingLeft: 10, flexWrap: 'wrap'}}>
						By tapping on <Text style={{fontWeight :'bold', color : '#1C8ADB'}}>SIGN UP</Text>, you agree to GameOn's <Text style={styles.textcontainer}>User Agreement, Privacy Policy</Text> and 
						<Text style={styles.textcontainer}> Cookie Policy</Text>.
					</Text>
				</View>
				<View style={{alignItems: 'center', marginBottom: 19}}>
				{renderIf(this.state.contacterror)(
						<Text style={styles.emailerror}>
							{this.state.contacterror}
						</Text>
				)}
				</View>
				{this.state.hint ? (
					<View style={styles.buttonContainer}>
						<TouchableOpacity
						disabled= {this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) && this.state.pass.match(/^[A-Za-z][a-zA-Z0-9_.-@*&$!#%]{5,15}$/) && this.state.number.match(/^[1-9][0-9]{9}$/) ? false : true}
						style = {this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) && this.state.pass.match(/^[A-Za-z][a-zA-Z0-9_.-@*&$!#%]{5,15}$/) && this.state.number.match(/^[1-9][0-9]{9}$/) ? styles.buttonContinue : styles.buttonContinueDisabled}
						onPress={this.matchContact}
						>
						<Text style={this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) && this.state.pass.match(/^[A-Za-z][a-zA-Z0-9_.-@*&$!#%]{5,15}$/) && this.state.number.match(/^[1-9][0-9]{9}$/) ? styles.text : styles.textDisabled}>SIGN UP</Text>
						</TouchableOpacity>
					</View>
				) : (
					<ActivityIndicator allowFontScaling={false} size={50} color="#1C8ADB" />
				)}
				</View>
      </View>
		</View>
        );
    }
}

const styles = StyleSheet.create({
container: {
		flex: 11,
		backgroundColor: 'white',
},
headerStyle: {
	backgroundColor: 'white',
},
forgot: {
		color: '#525452',
		fontWeight: 'bold',
},
textBoxBtnHolder:
{
		position: 'relative',
		alignSelf: 'stretch',
		justifyContent: 'center',
		width: '92%',
		paddingTop: 1,
		paddingLeft: 30,
		marginTop: 3,
		//paddingRight: 10,
		marginBottom: 3,
},

textBox:
{
		fontSize: 18,
		alignSelf: 'stretch',
		height: 50,
		paddingRight: 50,
		paddingLeft: 8,
		borderBottomWidth: 1,
		paddingVertical: 0,
		borderColor: '#aeb1b7',
},

visibilityBtn:
{
		position: 'absolute',
		right: 3,
		height: 40,
		width: 35,
		padding: 5,
},
btnImage:
{
		resizeMode: 'contain',
		height: '100%',
		width: '100%'
},
textDisabled: {
		color: '#1C8ADB',
		fontWeight: 'bold',
		fontSize: 16,
},
text: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 16,
},
button: {
		padding: 10,
		borderWidth: 2,
	borderColor: '#0d3f93',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 45,
},
back: {
		padding: 10,
		borderRadius: 100,
		backgroundColor: 'white',
		//marginTop: 5,
		width: '14%',
		alignItems: 'center',
		height: 45,
		borderWidth: 2,
},
buttonContinue: {
		padding: 10,
		borderWidth: 2,
		borderColor: '#1C8ADB',
		backgroundColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
},
buttonContinueDisabled: {
		padding: 10,
		borderWidth: 2,
	borderColor: '#1C8ADB',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 50,
		opacity: 0.7,
},
buttonContainer: {
		padding: 10,
		alignItems:'center',
},
password: {
		marginTop: 5,
		marginBottom: 25,
		width: '80%',
		padding: 10,
		height: 44,
		borderColor: 'grey',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderLeftWidth: 1,
		borderStyle: 'solid',
},
passwordcontainer: {
	paddingTop: 1,
	paddingBottom: 1  ,
	paddingLeft: 10,
	paddingRight: 10,
	flexDirection: 'row',
	alignItems: 'center',
},
toolbar: {
	height: 50,
	borderWidth: 2,
	borderColor: 'grey',
	alignItems: 'stretch',
	elevation: 1,
},
signup: {
	width: '88%',
	paddingLeft: 10,
	fontWeight: 'bold',
	color: '#0e4b77',
	fontSize: 28,
	paddingBottom: 12,
},
textcontainer: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#878a91',
},
textcontainerusername: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: '500',
		color: 'grey',
		fontStyle: 'italic',
},
emailerror: {
		width: '88%',
		fontSize: 12,
		paddingLeft: 10,
		fontWeight: '500',
		color: 'grey',
},
inputcontainer: {
		//paddingTop: 1,
		//paddingBottom: 1  ,
		paddingLeft: 10,
		paddingRight: 10,
		alignItems: 'center',
},
input: {
		marginTop: 5,
		marginBottom: 3,
		borderColor: '#aeb1b7',
		borderBottomWidth: 1,
		borderStyle: 'solid',
		width: '88%',
		padding: 10,
		height: 50,
		fontSize: 18,
},
inputusername: {
		marginTop: 5,
		marginBottom: 3,
		borderColor: '#aeb1b7',
		borderBottomWidth: 1,
		borderStyle: 'solid',
		width: '88%',
		padding: 10,
		height: 50,
		fontSize: 18,
		},
});