import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import {TextInput, View, TouchableOpacity, TouchableNativeFeedback, BackHandler, Text, StatusBar, Image, Button, ScrollView, Keyboard} from 'react-native';

export default class Forgot extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value : ''
		}
	}

	componentDidMount() {
	}

	render() {
		return(
			<View style={styles.container}>
				<View style={{alignItems: 'center',marginTop: '8%'}}>
					<Text style={styles.signup}>Restore your account</Text>
				</View>
				<View style={{alignItems: 'center'}}>
					<Text style={styles.textcontainer}>Enter your GameOn username or the email address linked to your account</Text>
				</View>
				<View style={styles.inputcontainer}>
					<TextInput
						style={styles.input}
						underlineColorAndroid='transparent'
						value = {this.state.value}
						onChangeText = {(text) => this.setState({value : text})}
					>
					</TextInput>
				</View>
				<View style={styles.buttonContainer}>
					<TouchableOpacity
						style={styles.button}
					>
						<Text style={styles.text}>Proceed</Text>
					</TouchableOpacity>
				</View>
				{/* <View style={styles.buttonContainer}>
          <Text style={styles.or}>OR</Text>
        </View> */}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	or: {
    fontWeight: 'bold',
    color: '#525452',
    fontSize: 16,
  },
	signup: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#525452',
		fontSize: 23,
		paddingBottom: 12,
	},
	text: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 16,
	},
	textcontainer: {
		width: '88%',
		paddingLeft: 10,
		fontWeight: 'bold',
		color: '#525452',
	},
	buttonContainer: {
		padding: 10,
		alignItems:'center',
	},
	button: {
		padding: 10,
		borderRadius: 5,
		backgroundColor: '#247500',
		//marginTop: 5,
		width: '88%',
		alignItems: 'center',
		height: 45,
	},
	inputcontainer: {
		marginTop: '4%',
		paddingLeft: 10,
		paddingRight: 10,
		alignItems: 'center',
	},
	input: {
		marginTop: 5,
		marginBottom: 10,
		borderColor: 'grey',
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 5,
		width: '88%',
		padding: 10,
		height: 44,
		},
})