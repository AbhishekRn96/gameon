import React, {Component} from 'react';
import { StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
//import {} from 'react-native-image-crop-picker'
import {TextInput,
				AsyncStorage,
				View,
				TouchableOpacity, 
				ActivityIndicator, 
				TouchableNativeFeedback, 
				KeyboardAvoidingView, 
				Text, 
				Image, 
				Button, 
				ScrollView, 
				Keyboard,
				FlatList,
				BackHandler} from 'react-native';

export default class Edit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user : '',
			image : null,
			visibility: false,
		}
		this.TakeAPicture = this.TakeAPicture.bind(this);
		this.SelectImageFromLibrary = this.SelectImageFromLibrary.bind(this);
		this.modalVisibility = this.modalVisibility.bind(this);
	}

	async componentDidMount() {
		await AsyncStorage.multiGet(['userid', 'dp']).then((data) => {
      this.setState({
        user : data[0][1],
        image : data[1][1],
      })
    })
	}

	componentWillUnmount() {
		this.setState({
			visibility : false,
		})
	}

	modalVisibility () {
		this.setState({
			visibility : true,
		})
	}

	async TakeAPicture () {
		await Permissions.askAsync(Permissions.CAMERA);
		await Permissions.askAsync(Permissions.CAMERA_ROLL);
		let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
			aspect: [1, 1],
			//base64: true,
		});
		
		console.log(result);

    if (!result.cancelled) {
      this.setState({ 
				image: result.uri,
				visibility: false });
		}else {
			this.setState({
				visibility : false,
			})
		}
	}

	async SelectImageFromLibrary () {
		let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
			aspect: [1, 1],
			//base64: true,
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ 
				image: result.uri,
				visibility: false });
		}else {
			this.setState({
				visibility : false,
			})
		}
  };

	render() {
		const {navigation} = this.props;
		const { goBack } = this.props.navigation;
		return (
			<View>
			<View style={[styles.headerStyle, {height: '10%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
				<View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
					<View>
						<TouchableNativeFeedback
							onPress={() => goBack()}>
							<View style={{padding: 10, paddingTop: 15}}>
								<Icon name='md-arrow-back' color={'black'} size={28}></Icon>
							</View>
						</TouchableNativeFeedback>
					</View>
					<View style={{padding: 10}}>
						<Text style={{fontSize: 20, fontWeight: '400', color: 'black'}}></Text>
					</View>
				</View>
				<View></View>
			</View>
				<Modal
					animationIn = 'fadeIn'
					animationOut = 'fadeOut'
					isVisible={this.state.visibility}
					//backdropOpacity = {1}
					//backdropColor='white'
					onBackdropPress={() => this.setState({visibility : false})}
					onBackButtonPress={() => this.setState({visibility : false})}
					style={{alignItems: 'center'}}>
					<View style={styles.modal}>
						<View style={styles.optionContainer}>
							<Icon3 name='camera-retro' color={'#1C8ADB'} size={25}></Icon3>
							<TouchableNativeFeedback
								onPress={this.TakeAPicture}
								>
								<Text style={styles.option}>Camera</Text>
							</TouchableNativeFeedback>
						</View>
						<View style={styles.optionContainer}>
							<Icon3 name='file-image-o' color={'#1C8ADB'} size={25}></Icon3>
							<TouchableNativeFeedback
								onPress={this.SelectImageFromLibrary}
								>
								<Text style={styles.option}>Galary</Text>
							</TouchableNativeFeedback>
						</View>
						<View style={styles.optionContainer}>
							<Icon name='delete' color={'#1C8ADB'} size={25}></Icon>
							<TouchableNativeFeedback
								onPress={() => this.setState({visibility : false})}
								>
								<Text style={styles.option}>
									Remove Picture
								</Text>
							</TouchableNativeFeedback>
						</View>
					</View>
				</Modal>
				<ScrollView>
					<View style={styles.container}>
						<View style={{alignItems: 'center', marginTop: 13}}>
							<View style={{flex: 1}}>
								<TouchableNativeFeedback
									onPress={this.modalVisibility}>
									<Image
										source={{uri: this.state.image}}
										style={styles.dp}>
									</Image>
								</TouchableNativeFeedback>
							</View>
							<View style={{width: '88%', alignItems: 'flex-start', marginTop: '5%'}}>
								<Text
								 style={styles.font}>Username</Text>
							</View>
							<View style={{flex: 1, width: '95%', alignItems: 'center'}}>
								<TextInput
									value={this.state.user}
									underlineColorAndroid = 'transparent'
									onChangeText = {(text) => this.setState({user : text})}
									style={styles.input}
									>
								</TextInput>
							</View>
							<View style={{width: '95%', alignItems: 'center', marginTop: '10%'}}>
								<TouchableOpacity
									disabled= {this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) ? false : true}
									 style={this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) ? styles.button : styles.buttonDisabled}
									 >
									<Text
									 style={this.state.user.match(/^[A-Za-z][a-zA-Z0-9_.-]{5,15}$/) ? styles.text : styles.textDisabled}
									 >
										SAVE
									</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	headerStyle: {
		backgroundColor: '#1C8ADB',
	},
	dp: {
		borderRadius: 100,
		borderWidth: 3,
		borderColor: '#1C8ADB',
		height: 125,
		width: 125,
	},
	container: {
		flex: 1,
		flexDirection: 'column',
	},
	input: {
    //marginTop: 7,
    borderColor: 'grey',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    width: '95%',
    padding: 8,
    fontSize: 18,
    height: 50,
	},
	font: {
		fontSize: 16.5,
		color: '#1C8ADB',
		fontWeight: 'bold',
	},
	buttonDisabled: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    //marginTop: 5,
    width: '95%',
    height: 50,
    alignItems: 'center',
    opacity: 0.7,
	},
	button: {
    padding: 10,
    borderWidth: 2,
    borderColor: '#1C8ADB',
    backgroundColor: '#1C8ADB',
    //marginTop: 5,
    width: '95%',
    alignItems: 'center',
    //opacity: 0.7,
    height: 50,
  },
	textDisabled: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 16,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
	},
	modal: {
		height: '30%',
		width: '80%',
		borderWidth: 1,
		borderRadius: 5,
		backgroundColor: 'white',
		alignItems: 'flex-start',
	},
	option: {
		fontSize: 18,
		padding: 12,
	},
	optionContainer: {
		marginLeft: '5%',
		flexDirection : 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
})