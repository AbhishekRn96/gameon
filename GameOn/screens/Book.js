import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import Icon2 from 'react-native-vector-icons/Ionicons';
//import Icon3 from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import config from "../extra/config";
import Modal from "react-native-modal";
import moment from 'moment';
import renderIf from '../extra/renderIf';
import {TextInput,
        AsyncStorage, 
        View, 
        TouchableOpacity, 
        ActivityIndicator, 
        TouchableNativeFeedback, 
        InteractionManager, 
        Text, 
        Image, 
        Button,
        FlatList,
        StatusBar,
        ScrollView, 
        BackHandler} from 'react-native';

export default class Book extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date : new Date(),
      dateValue : moment(new Date()).format('MMM Do YYYY'),
      minDate : null,
      maxDate : null,
      start: 'Start',
      end: 'End',
      timeArrayStart: [],
      timeArrayEnd: [],
      available : '',
      selectedpc : 1,
      totalHrs: null,
      totalCost: null,
      startColor: '#858687',
      endColor: '#858687',
      hideButton : true,
      booked : [],
      disabledDates : [],
      show: false,
      login: false,
      didFinishInitialAnimation: false,
    }
    this.handleDate = this.handleDate.bind(this);
    this.clearData = this.clearData.bind(this);
    this.booking = this.booking.bind(this);
    this.handleStart = this.handleStart.bind(this);
    this.handleEnd = this.handleEnd.bind(this);
    this.bookedDates = this.bookedDates.bind(this);
    this.ToBooking = this.ToBooking.bind(this);
  }

  componentDidMount() {
    var date = new Date();
    let dateValue = moment(date).format('MMM Do YYYY');
    let item = this.props.navigation.getParam('item');
    let arrStart = [];
    let arrEnd = [];
    arrEnd.push(parseInt(item.OpenTime)+0.5)
    for(let i = parseInt(item.OpenTime); i < parseInt(item.CloseTime)+1; i++){
      if(i!=parseInt(item.CloseTime)){
        arrStart.push(i);
        arrStart.push(i+0.5);
      }
      if(i!=parseInt(item.OpenTime)){
        if(i==parseInt(item.CloseTime)){
          arrEnd.push(i);
        }
        else{
          arrEnd.push(i);
          arrEnd.push(i+0.5);
        }
      }
    }
    this.setState({
      timeArrayStart : arrStart
    })
    this.setState({
      timeArrayEnd : arrEnd,
    })
    if(item.DisabledDates === undefined) {
      this.setState({
        disabledDates : [],
      })
    } else {
      this.setState({
        disabledDates : item.DisabledDates,
      })
    }
    this.bookedDates(dateValue);
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        didFinishInitialAnimation: true,
      });
    });
  }

  async bookedDates(dateValue) {
    let choice = this.props.navigation.getParam('choice');
    this.setState({booked : null})
    try {
      let item = this.props.navigation.getParam('item');
      let response = await fetch(`${config.bookingurl}bookedDates`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        date : dateValue,
        CafeId : item.CafeId,
        choice : choice
      }),
    })
    let responseJson = await response.json();
    if(responseJson != null) {
      let arr = [];
      for(let i=0;i<responseJson.length;i++) {
        for(let j=responseJson[i].StartTime;j<responseJson[i].EndTime;j++) {
          arr.push(j);
          if(j+0.5 != responseJson[i].EndTime)
            arr.push(j+0.5);
        }
      }
      this.setState({booked : arr})
    }
    } catch (err) { 
      console.log(err.message);
    }
  }

  async handleDate (date) {
    console.log(date);
    this.setState({
      date : date,
      dateValue : moment(date).format('MMM Do YYYY'),
      showDate : !this.state.showDate,
      available : null,
      startColor: '#858687',
      endColor: '#858687',
      start : 'Start',
      end : 'End',
    })
    let dateValue = moment(date).format('MMM Do YYYY')
    console.log(dateValue)
    this.bookedDates(dateValue);
  }



  handleStart (item) {
    if(item != this.state.start)
      this.setState({
        start : item,
        startColor : '#1C8ADB',
        endColor : '#858687',
        end : 'End',
        available : null
      })
  }

  async handleEnd (item) {
    if(item>=this.state.start+1 && item !=this.state.end){
      await this.setState({
        end : item,
        endColor : '#1C8ADB',
        available : null,
        show: true,
      })

      this.booking()
    }
  }

  async booking() {
    if(this.state.end > this.state.start){
      let item = this.props.navigation.getParam('item');
      let choice = this.props.navigation.getParam('choice');
      if(choice == "PC") {
        try {
          let response = await fetch(`${config.bookingurl}bookingPC`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            CafeId : item.CafeId,
            date : this.state.dateValue,
            start : this.state.start,
            end : this.state.end,
            available : item.PCs
          }),
        })
        let responseJson = await response.json();
        console.log(responseJson);
        this.setState({
            available : responseJson.available,
            totalCost : (item.CostPC * (this.state.end - this.state.start)) * 1,
            totalHrs : this.state.end - this.state.start,
            selectedpc: 1,
            show: false,
          })
        } catch (err) { 
          console.log(err.message);
        }
      } else {
        try {
          let response = await fetch(`${config.bookingurl}bookingConsole`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            CafeId : item.CafeId,
            date : this.state.dateValue,
            start : this.state.start,
            end : this.state.end,
            available : item.Consoles
          }),
        })
        let responseJson = await response.json();
        console.log(responseJson);
        this.setState({
            available : responseJson.available,
            totalCost : (item.CostConsole * (this.state.end - this.state.start)) * 1,
            totalHrs : this.state.end - this.state.start,
            selectedpc: 1,
            show: false,
          })
        } catch (err) { 
          console.log(err.message);
        }
      }
    }
  }

  async ToBooking() {
    let item = this.props.navigation.getParam('item');
    let choice = this.props.navigation.getParam('choice');
    AsyncStorage.getItem('userid').then(async (result) => {
      if(result) {
        this.props.navigation.navigate('Booking', {data : this.state, item : item, choice : choice});
      } else {
        this.setState({login : true});
        this.props.navigation.navigate('Login', {route: 'Account'});
      }
    })
  }

  clearData (itemValue, itemIndex) { 
    this.setState({
      start : itemValue,
      available : null,
    })
  }

  render() {
    const {navigate} = this.props.navigation;
    const { goBack } = this.props.navigation;
    const {navigation} = this.props;
    const item = navigation.getParam('item');
    let choice = navigation.getParam('choice');
    let dateArray = [];
    let lastDate = new Date(new Date().setDate(new Date().getDate()+20));
    for(let i=0; i<20; i++) {
      let dateVal = new Date(new Date().setDate(new Date().getDate()+i));
      let dateArr = dateVal.toString().split(' ');
      dateArray.push(
        <View key={i}>
          <View style={{marginLeft: 2, marginRight: 2}}>
            {new Date(this.state.date).getDate() == new Date(dateVal).getDate() && this.state.disabledDates.indexOf(moment(dateVal).format('MMM Do YYYY')) === -1 ? (
              <TouchableNativeFeedback
                onPress={() => this.handleDate(dateVal)}>
                <View elevation={5} style={styles.boxShadow}>
                  <View style={{padding: 10, alignItems: 'center'}}>
                    <Text style={{fontSize: 14, fontWeight: '500', color: '#1C8ADB'}}>{dateArr[0]}</Text>
                    <Text style={{fontSize: 17, fontWeight: '500', color: '#1C8ADB'}}>{dateArr[2]}</Text>
                  </View>
                </View>
              </TouchableNativeFeedback>
            ) : this.state.disabledDates.indexOf(moment(dateVal).format('MMM Do YYYY')) > -1 ? (
              <View style={{paddingTop: 10, paddingBottom: 10, paddingLeft: 13, paddingRight: 13, alignItems: 'center'}}>
                <Text style={{fontSize: 14, fontWeight: '500', color: 'grey'}}>{dateArr[0]}</Text>
                <Text style={{fontSize: 17, fontWeight: '500', color: 'grey'}}>{dateArr[2]}</Text>
              </View>
            ) : (
              <TouchableNativeFeedback
                onPress={() => this.handleDate(dateVal)}>
                <View style={{paddingTop: 10, paddingBottom: 10, paddingLeft: 13, paddingRight: 13, alignItems: 'center'}}>
                  <Text style={{fontSize: 14, fontWeight: '500', color: '#1C8ADB'}}>{dateArr[0]}</Text>
                  <Text style={{fontSize: 17, fontWeight: '500', color: '#1C8ADB'}}>{dateArr[2]}</Text>
                </View>
              </TouchableNativeFeedback>
            )}
          </View>
        </View>
      )
    }
    let compare = '';
    if(new Date().getMinutes()<30)
      compare = Number(new Date().getHours());
    else
      compare = Number(new Date().getHours()+'.5');
    //const time = Number(moment(new Date()).format('HH'));
    //const date = moment(new Date()).format('MMM Do YYYY');
    var startTime = [];
    for(let i=0;i<this.state.timeArrayStart.length;i++) {
      startTime.push(
        <View key={i}>
          {this.state.timeArrayStart[i] <= compare && new Date(this.state.date).getDate() == new Date().getDate() ? (
            <TouchableNativeFeedback
              disabled={true}>
              <View style={{alignItems: 'center', padding: 10}}>
                <Text style={styles.scaleButtonsTimeDisabled}>{this.state.timeArrayStart[i]%1 == 0.5 ? (this.state.timeArrayStart[i]-0.5+':30') : (this.state.timeArrayStart[i]+':00')}</Text>
              </View>
            </TouchableNativeFeedback>
          ) : (
            <View>
              {this.state.booked != null && this.state.booked.includes(this.state.timeArrayStart[i]) ? (
                <TouchableNativeFeedback
                  disabled={true}>
                  <View style={{alignItems: 'center', padding: 10}}>
                    <Text style={styles.scaleButtonsDisabled}>{this.state.timeArrayStart[i]%1 == 0.5 ? (this.state.timeArrayStart[i]-0.5+':30') : (this.state.timeArrayStart[i]+':00')}</Text>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.handleStart(this.state.timeArrayStart[i])}>
                    {this.state.start == this.state.timeArrayStart[i] ? (
                      <View style={{alignItems: 'center', padding: 10, backgroundColor: '#1C8ADB', borderRadius: 100}}>
                        <Text style={styles.scaleButtonsSelected}>{this.state.timeArrayStart[i]%1 == 0.5 ? (this.state.timeArrayStart[i]-0.5+':30') : (this.state.timeArrayStart[i]+':00')}</Text>
                      </View>
                    ) : (
                      <View style={{alignItems: 'center', padding: 10}}>
                        <Text style={styles.scaleButtons}>{this.state.timeArrayStart[i]%1 == 0.5 ? (this.state.timeArrayStart[i]-0.5+':30') : (this.state.timeArrayStart[i]+':00')}</Text>
                      </View>
                    )}
                </TouchableNativeFeedback>
              )}
            </View>
            )}
        </View>
      )
    }
    var endTime = [];
    for(let i=0;i<this.state.timeArrayEnd.length;i++) {
      endTime.push(
        <View key={i}>
          {this.state.timeArrayEnd[i] <= compare && new Date(this.state.date).getDate() == new Date().getDate() ? (
            <TouchableNativeFeedback
              disabled={true}>
              <View style={{alignItems: 'center', padding: 10}}>
                <Text style={styles.scaleButtonsTimeDisabled}>{this.state.timeArrayEnd[i]%1 == 0.5 ? (this.state.timeArrayEnd[i]-0.5+':30') : (this.state.timeArrayEnd[i]+':00')}</Text>
              </View>
            </TouchableNativeFeedback>
          ) : (
            <View>
              {this.state.booked != null && this.state.booked.includes(this.state.timeArrayEnd[i]) ? (
                <TouchableNativeFeedback
                  disabled={true}>
                  <View style={{alignItems: 'center', padding: 10}}>
                    <Text style={styles.scaleButtonsDisabled}>{this.state.timeArrayEnd[i]%1 == 0.5 ? (this.state.timeArrayEnd[i]-0.5+':30') : (this.state.timeArrayEnd[i]+':00')}</Text>
                  </View>
                </TouchableNativeFeedback>
              ) : (
                <TouchableNativeFeedback
                  onPress={() => this.handleEnd(this.state.timeArrayEnd[i])}>
                  {this.state.end == this.state.timeArrayEnd[i] ? (
                    <View style={{alignItems: 'center', padding: 10, backgroundColor: '#1C8ADB', borderRadius: 100}}>
                      <Text style={styles.scaleButtonsSelected}>{this.state.timeArrayEnd[i]%1 == 0.5 ? (this.state.timeArrayEnd[i]-0.5+':30') : (this.state.timeArrayEnd[i]+':00')}</Text>
                    </View>
                  ) : (
                    <View style={{alignItems: 'center', padding: 10}}>
                      <Text style={styles.scaleButtons}>{this.state.timeArrayEnd[i]%1 == 0.5 ? (this.state.timeArrayEnd[i]-0.5+':30') : (this.state.timeArrayEnd[i]+':00')}</Text>
                    </View>
                )}
              </TouchableNativeFeedback>
              )}
            </View>
          )}
        </View>
      )
    }
    return (
      this.state.didFinishInitialAnimation == false ? (
        <View style={{backgroundColor: 'white', flex: 1}}>
          <View style={styles.container}>
            <ActivityIndicator />
            <StatusBar
              backgroundColor="white"
              barStyle="dark-content"
            />
          </View>
        </View>
      ) : (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={[styles.headerStyle, {height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}]}>
          <View style={{marginLeft: '3%', flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <TouchableNativeFeedback
								onPress={() => goBack()}>
                <View style={{paddingLeft: 8, paddingRight: 5, paddingTop: 5}}>
                  <Icon2 name='md-arrow-back' color={'black'} size={28}></Icon2>
                </View>
              </TouchableNativeFeedback>
            </View>
            <View style={{marginLeft: '3%', marginTop: '2%'}}>
              <Text style={{fontSize: 18, fontWeight: '400', color: 'black'}}>Select Date and Time</Text>
            </View>
          </View>
          <View style={{marginRight: '4%'}}>
          </View>
        </View>
        <View style={{alignItems: 'center'}}> 
          <Modal
            animationIn = 'fadeIn'
            animationOut = 'fadeOut'
            isVisible={this.state.show}
            backdropOpacity={0.8}
            backdropColor='white'
            style={{alignItems: 'center'}}>
              <ActivityIndicator size={50} color="#1C8ADB" style={{marginTop: '20%'}} />
          </Modal>
        </View>
        <View style={{width: '96%', marginLeft: '2%'}}>
          {this.state.date.getMonth()+1 == lastDate.getMonth()+1 ? (
            <Text style={{padding: 10, fontSize: 20, fontWeight: '500', color: '#1C8ADB'}}>{lastDate.toString().split(' ')[1]+' '+lastDate.toString().split(' ')[3].split('0')[1]}</Text>
          ) : (
            <Text style={{padding: 10, fontSize: 20, fontWeight: '500', color: '#1C8ADB'}}>{this.state.date.toString().split(' ')[1]+' '+this.state.date.toString().split(' ')[3].split('0')[1]+' / '+lastDate.toString().split(' ')[1]+' '+lastDate.toString().split(' ')[3].split('0')[1]}</Text>
          )}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {dateArray}
          </ScrollView>
        </View>
        <View style={{alignItems: 'center',}}>
          <View style={{marginTop: 10, width: '90%'}}>
            <Text style={{color: 'grey'}}>
              Interval of Gaming session should be a minimum of <Text style={{color: '#1C8ADB'}}>1 hour</Text>. Scroll the scales to select time slot
            </Text>
          </View>
        </View>
        {this.state.booked ? (
          <View style={{flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, height: '37%'}}>
            <View>
              <View style={{alignItems: 'center'}}>
                <View style={{borderBottomWidth: 2, borderColor: this.state.startColor}}>
                  {this.state.start == 'Start' ? (
                    <Text style={{padding: 10, fontSize: 20, color: this.state.startColor}}>From</Text>
                  ) : (
                    <Text style={{padding: 10, fontSize: 20, color: this.state.startColor}}>{this.state.start % 1 == 0.5 ? (this.state.start-0.5+':30'):(this.state.start+':00')}</Text>
                  )}
                </View>
              </View>
              <View style={{marginTop: 15, height: '80%'}}>
                <ScrollView showsVerticalScrollIndicator={false}>{startTime}</ScrollView>
              </View>
            </View>
            <View>
              <View style={{alignItems: 'center'}}>
                <View style={{borderBottomWidth: 2, borderColor: this.state.endColor}}>
                  {this.state.end == 'End' ? (
                    <Text style={{padding: 10, fontSize: 20, color: this.state.endColor}}>To</Text>
                  ) : (
                    <Text style={{padding: 10, fontSize: 20, color: this.state.endColor}}>{this.state.end % 1 == 0.5 ? (this.state.end-0.5+':30'):(this.state.end+':00')}</Text>
                  )}
                </View>
              </View>
              <View style={{marginTop: 15, height: '80%'}}>
                <ScrollView showsVerticalScrollIndicator={false}>{endTime}</ScrollView>
              </View>
            </View>
          </View>
        ) : (
          <ActivityIndicator size={50} color="#1C8ADB" style={{marginTop: '40%'}} />
        )}
        {renderIf(this.state.available)(
          <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.book1}>Duration</Text>
              <Text style={styles.book2}>{this.state.totalHrs%1 == 0.5 ? (this.state.totalHrs-0.5+' hrs 30 mins') : (this.state.totalHrs+' hrs')}</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Text style={styles.book1}>{choice == "PC" ? "Available PCs" : "Available Consoles"}</Text>
              <Text style={styles.book2}>{this.state.available}</Text>
            </View>
          </View>
        )}
        {renderIf(this.state.available)(
          <View style={{alignItems : 'center', bottom: 0, position: 'absolute', width: '100%'}}>
            <TouchableNativeFeedback
              onPress={this.ToBooking}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={styles.linearGradient}>
                <View style={{ alignItems: 'center'}}>
                  <Text style={styles.book}>  Next  </Text>
                </View>
              </LinearGradient>
            </TouchableNativeFeedback>
          </View>
        )}
      </View>
    )
    )}
}

const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  edit: {
		backgroundColor: '#1C8ADB',
		padding: 10,
		width: '30%',
		height: 50,
    alignItems: 'center',
    flexDirection: 'row',
  },
  linearGradient: {
    width: '90%',
    marginBottom: '3%',
    borderRadius: 100,
  },
  label: {
    fontSize: 20,
    color: '#1C8ADB',
    fontWeight: '500'
  },
  booking: {
		backgroundColor: '#1C8ADB',
		padding: 10,
		width: '40%',
		height: 60,
    alignItems: 'center',
  },
  scaleButtons: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: '#1c7f16'
  },
  scaleButtonsDisabled: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: '#cc1818'
  },
  scaleButtonsTimeDisabled: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: 'grey'
  },
  scaleButtonsSelected: {
    fontSize: 16, 
    fontWeight: 'bold', 
    color: 'white',
  },
  shadow : {
    borderBottomWidth: 2,
    borderRightWidth: 2,
    borderColor: '#c6c2c2',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
  },
  editDate: { 
    backgroundColor: 'white',
		padding: 10,
		width: '60%',
		height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderWidth: 2,
    borderColor: '#1C8ADB',
  },
  picker: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: '#1C8ADB',
		padding: 10,
		width: '70%',
		height: 50,
    alignItems: 'center',
  },
  pickerContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '40%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  pcContainer : {
    borderWidth: 2,
    borderColor: '#1C8ADB',
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  boxShadow : {
    backgroundColor: 'white', 
    borderBottomWidth: 2,
    borderRightWidth: 2,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderRadius: 10,
    borderColor: '#c6c2c2',
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
  },
  number: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 25,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 5,
  },
  book: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    padding: '4%'
  },
  book1: {
    color: '#1C8ADB',
    fontWeight: '400',
    fontSize: 18,
  },
  book2: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 25,
  },
  bookDate: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 10
  },
  bookDay: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 23,
  },
  book3: {
    color: '#1C8ADB',
    fontWeight: 'bold',
    fontSize: 30,
  },
})