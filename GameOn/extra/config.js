const mode = "prod"

const config = {
  userurl : mode == "dev" ? "http://192.168.0.107:3020/" : "https://gameonusercontrol.herokuapp.com/",
  bookingurl : mode == "dev" ? "https://thegameonn.herokuapp.com/" : "https://gameonbookingcontrol.herokuapp.com/"
}

export default config;