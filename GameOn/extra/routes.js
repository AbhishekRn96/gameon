import React from 'react';
import {createStackNavigator, createMaterialTopTabNavigator, createAppContainer, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import LogIn from '../screens/Login';
import SignUp from '../screens/Signup';
import {ActivityIndicator, AsyncStorage, View, StyleSheet, StatusBar} from 'react-native';
import Forgot from '../screens/Forget';
import Verify from '../screens/Verify';
import DashboardTemp from '../screens/DashboardTemp';
import Home from '../screens/Home';
import Details from '../screens/Details';
import Cafe from '../screens/Cafe';
import Filter from '../screens/Filter';
import Edit from '../screens/Edit';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import BookConfirm from '../screens/BookConfirm';
import Settings from '../screens/Settings';
import SignUp2 from '../screens/SignUp2';
import Book from '../screens/Book';
import Booking from '../screens/Booking';
import Account from '../screens/Account';
import Upcoming from '../screens/UpcomingB';
import Previous from '../screens/PreviousB';
import Offers from '../screens/Offers';
import SysChoice from '../screens/SysChoice';
import RateCafe from '../screens/RateCafe';
import BookedDetails from '../screens/BookedDetails';
import BookingCancelled from '../screens/BookingCancelled';
import PassLogin from '../screens/PassLogin';
import Tournaments from '../screens/Tournaments';
import ViewTour from '../screens/ViewTour';
import TBooking from '../screens/TBooking';
import Withdraw from '../screens/Withdraw';

class AuthLoadingScreen extends React.Component {
  constructor() {
		super();
    this.bootstrapAsync();
	}
	
  bootstrapAsync = async () => {
		const userToken = await AsyncStorage.getItem('userid');
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');

    //this.props.navigation.navigate('App');
  };

  render() {
    return (
      <View style={styles.container}>
				<StatusBar
					backgroundColor="white"
					barStyle="dark-content"
				/>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Tabs = createMaterialBottomTabNavigator({
	Tournaments : {screen: Tournaments,
		navigationOptions:{
			tabBarLabel: 'Tournaments',
			tabBarIcon: ({tintColor}) => (
				<Icon2 name='sword-cross' color={tintColor} size={24}></Icon2>
			)
		}},
	Cafes : {screen: DashboardTemp,
		navigationOptions:{
			tabBarLabel: 'Cafes',
			tabBarIcon: ({tintColor}) => (
				<Icon name='game-controller' color={tintColor} size={24}></Icon>
			)
		}},
	Account : {screen: Account,
		navigationOptions:{
			tabBarLabel: 'Account',
			tabBarIcon: ({tintColor}) => (
				<Icon2 name='account' color={tintColor} size={24}></Icon2>
			)
		}},
}, {
	activeTintColor : '#1C8ADB',
	lazy : false,
	shifting: true,
	barStyle : {
		backgroundColor : 'white',
		padding: 5,
	}
})

const Tabs2 = createMaterialBottomTabNavigator({
	Tournaments : {screen: Tournaments,
		navigationOptions:{
			tabBarLabel: 'Tournaments',
			tabBarIcon: ({tintColor}) => (
				<Icon2 name='sword-cross' color={tintColor} size={24}></Icon2>
			)
		}},
	Cafes : {screen: DashboardTemp,
		navigationOptions:{
			tabBarLabel: 'Cafes',
			tabBarIcon: ({tintColor}) => (
				<Icon name='game-controller' color={tintColor} size={24}></Icon>
			)
		}},
	Account : {screen: Account,
		navigationOptions:{
			tabBarLabel: 'Account',
			tabBarIcon: ({tintColor}) => (
				<Icon2 name='account' color={tintColor} size={24}></Icon2>
			)
		}},
}, {
	activeTintColor : '#1C8ADB',
	lazy : false,
	shifting: true,
	barStyle : {
		backgroundColor : 'white',
		padding: 5,
	}
})

const AuthStack = createStackNavigator({
	Login : {
		screen : LogIn,
		navigationOptions: {
				header : null,
		}},
	PassLogin : {
		screen : PassLogin,
		navigationOptions: {
				header : null,
		}},
	SysChoice : {
		screen : SysChoice,
		navigationOptions: {
			header : null,
	}},
	Signup : {
			screen : SignUp,
			navigationOptions: {
					header : null,
			}},
	Signup2 : {
		screen : SignUp2,
		navigationOptions: {
				header : null,
		}},	
	Forgot : {
			screen : Forgot,
			navigationOptions: {
					header : null,
			}},
	Verify : {
			screen : Verify,
			navigationOptions: {
					header : null,
			}},
	Withdraw : {
			screen : Withdraw,
			navigationOptions: {
					header : null,
			}},
	BookConfirm : { screen : BookConfirm,
		navigationOptions: {
			header: null,
		}},
	Cafe : { screen : Cafe,
			navigationOptions: {
					header : null,
			}},
	ViewTour : { screen : ViewTour,
			navigationOptions: {
					header : null,
			}},
	Dashboard : {
		screen : Tabs2,
		navigationOptions: {
				header : null,
		}},
	Filter : { screen : Filter,
		navigationOptions: {
			header : null,
	}},
	Settings : { screen : Settings,
		navigationOptions: {
			header : null,
	}},
	Edit : { screen : Edit,
		navigationOptions: {
			header : null,
	}},
	Book : { screen : Book,
		navigationOptions: {
			header: null,
		}},
	TBooking : { screen : TBooking,
		navigationOptions: {
			header: null,
		}},
	Booking : { screen : Booking,
		navigationOptions: {
			header: null,
		}},
	Previous : { screen : Previous,
		navigationOptions: {
			header: null,
		}},
	Upcoming : { screen : Upcoming,
		navigationOptions: {
			header: null,
		}},
	Details : { screen : Details,
		navigationOptions: {
			header: null,
		}},
	Offers : { screen : Offers,
		navigationOptions: {
			header: null,
		}},
	},{
		initialRouteName: 'Dashboard'
	})

const AppStack = createStackNavigator({
Login : {
	screen : LogIn,
	navigationOptions: {
			header : null,
	}},
PassLogin : {
	screen : PassLogin,
	navigationOptions: {
			header : null,
	}},
SysChoice : {
	screen : SysChoice,
	navigationOptions: {
		header : null,
}},
Rate : {
	screen : RateCafe,
	navigationOptions: {
		header : null,
	}},
BookingCancelled : {
	screen : BookingCancelled,
	navigationOptions: {
		header : null,
	}},
Signup : {
		screen : SignUp,
		navigationOptions: {
				header : null,
		}},
Signup2 : {
	screen : SignUp2,
	navigationOptions: {
			header : null,
	}},	
Forgot : {
		screen : Forgot,
		navigationOptions: {
				header : null,
		}},
Verify : {
		screen : Verify,
		navigationOptions: {
				header : null,
		}},
Withdraw : {
		screen : Withdraw,
		navigationOptions: {
				header : null,
		}},
BookConfirm : { screen : BookConfirm,
	navigationOptions: {
		header: null,
	}},
BookedDetails : { screen : BookedDetails,
	navigationOptions: {
		header: null,
	}},
Cafe : { screen : Cafe,
		navigationOptions: {
				header : null,
		}},
ViewTour : { screen : ViewTour,
		navigationOptions: {
				header : null,
		}},
Dashboard : {
	screen : Tabs,
	navigationOptions: {
			header : null,
	}},
Filter : { screen : Filter,
	navigationOptions: {
		header : null,
}},
Settings : { screen : Settings,
	navigationOptions: {
		header : null,
}},
Edit : { screen : Edit,
	navigationOptions: {
		header : null,
}},
Book : { screen : Book,
	navigationOptions: {
		header: null,
	}},
TBooking : { screen : TBooking,
	navigationOptions: {
		header: null,
	}},
Booking : { screen : Booking,
	navigationOptions: {
		header: null,
	}},
Previous : { screen : Previous,
	navigationOptions: {
		header: null,
	}},
Upcoming : { screen : Upcoming,
	navigationOptions: {
		header: null,
	}},
Details : { screen : Details,
	navigationOptions: {
		header: null,
	}},
Offers : { screen : Offers,
	navigationOptions: {
		header: null,
	}},
},{
	initialRouteName: 'Dashboard'
})

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading', 
  }
));


//AppRegistry.registerComponent('UI', () => Routes);