import React, {Component} from 'react';
import Rootstack from './extra/routes';
import {AsyncStorage, StatusBar, ActivityIndicator, Text, TextInput, View, StyleSheet, NetInfo, Image, TouchableNativeFeedback} from 'react-native';
import renderIf from './extra/renderIf';
import SplashScreen from 'react-native-splash-screen';
import LinearGradient from 'react-native-linear-gradient';
import config from "./extra/config";
import FlashMessage from "react-native-flash-message";
import {configure, localNotif, scheduleNotif} from './screens/Notifications';
// import {createStore} from 'redux';
// import {Provider} from 'react-redux';

// const initialState = {
//     Cafelist : '',
//     //userData : '',
// }

// const reducer = (state = initialState) => {

// }

// const store = createStore(reducer);

export default class App extends Component {
	constructor (props) {
		super(props);
		this.state ={
				connection : "",
				message : false,
				login: "",
		}
		this.checkConnection = this.checkConnection.bind(this);
		this.fixTextCutOff();
		this.getUser = this.getUser.bind(this);
	}

	async componentWillMount() {
		await NetInfo.isConnected.fetch().then(isConnected => {
			if(isConnected) {
				this.setState({connection : "yes"});
			} else {
				this.setState({connection : "no"});
				SplashScreen.hide();
			}
		});
		AsyncStorage.getItem('user').then((result) => {
			this.getUser(result);
		})
		AsyncStorage.getItem('contact').then((phone) => {
			if(phone) {
				configure(phone);
				return;
			}
			return;
		})
	}

	getUser(id) {
		fetch(`${config.userurl}getUser`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        UserId : id,
      }),
    })
    .then(response =>  response.json())
    .then(async (resData) => {
      if(resData.message === "failed") {
        console.log("failed");
      } else {
        await AsyncStorage.multiSet([
          ['userid', resData.Username],
          ['email', resData.Email],
          ['dp', resData.DP],
          ['contact', resData.Contact],
          ['gcredits', resData.GCredits+''],
          ['user', resData.UserId]
        ])
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
	}

	fixTextCutOff() {
		const oldRenderText = Text.render;
		Text.render = function render(...args) {
			const origin = oldRenderText.call(this, ...args);
			return React.cloneElement(origin, {
				style: [styles.defaultFontFamily, origin.props.style],
			});
		};

		const oldRenderTextInput = TextInput.render;
		TextInput.render = function render(...args) {
			const origin = oldRenderTextInput.call(this, ...args);
			return React.cloneElement(origin, {
				style: [styles.defaultFontFamily, origin.props.style],
			});
		};
	}

	async checkConnection() {
		await this.setState({message : false});
		await NetInfo.isConnected.fetch().then(isConnected => {
			if(isConnected) {
				this.setState({connection : "yes"});
			} else {
				this.setState({connection : "no", message : true});
				SplashScreen.hide();
			}
		});
	}

	render() {
		return (
			<View style={{flex: 1, backgroundColor: 'white'}}>
				<StatusBar
					backgroundColor="white"
					barStyle="dark-content"
				/>
				{this.state.connection == "yes" ? (
						<Rootstack loginState={this.state.login}/>
				) : this.state.connection == "no" ? (
						<View>
							<Image 
									source={require('./assets/NoNet.png')}
									style={{height: 435, width: 370, marginTop: 45}}>
							</Image>
							<View style={{marginTop: 35}}>
								<TouchableNativeFeedback
									onPress={this.checkConnection}>
									<LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={{width : '38%', alignSelf : 'center', borderRadius: 50}}>
										<View style={{ alignItems: 'center'}}>
											<Text style={{ fontSize : 18, fontWeight : '500', color : 'white', padding: 15 }}>Try Again</Text>
										</View>
									</LinearGradient>
								</TouchableNativeFeedback>
							</View>
							{renderIf(this.state.message)(
								<View style={{marginTop: 20, alignItems: 'center'}}>
									<Text style={{ fontSize : 15, fontWeight : '500', color : '#244a90' }}>No connection detected</Text>
								</View>
							)}
						</View>
				) : (
						<View style={styles.container}>
								<ActivityIndicator />
								<StatusBar barStyle="default" />
						</View>
				)}
				{/* <Image 
					source={require('./assets/NoNet.png')}
					style={{height: 435, width: 370, marginTop: 45}}>
				</Image>
				<View style={{marginTop: 35}}>
					<TouchableNativeFeedback
						onPress={this.checkConnection}>
						<LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1C8ADB', '#075c9b']} style={{width : '38%', alignSelf : 'center', borderRadius: 50}}>
              <View style={{ alignItems: 'center'}}>
                <Text style={{ fontSize : 18, fontWeight : '500', color : 'white', padding: 15 }}>Try Again</Text>
              </View>
            </LinearGradient>
					</TouchableNativeFeedback>
				</View> */}
				<FlashMessage position="top" />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	}, 
	defaultFontFamily: {
		fontFamily: 'OpenSans',
	},
});